#!/usr/bin/env bash

# initial install
sudo apt-get install -y software-properties-common
sudo add-apt-repository ppa:nginx/stable
sudo add-apt-repository ppa:ondrej/php
sudo curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -

# install software
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install vim nginx build-essential python-software-properties git nodejs -y

# install php 7
sudo apt-get install php7.2-cli php7.2-fpm php7.2-curl php7.2-gd php7.2-mysql php7.2-mbstring php7.2-dom zip unzip -y

# MySQL setup for development purposes ONLY
echo -e "\n--- Install MySQL specific packages and settings ---\n"
debconf-set-selections <<< "mysql-server mysql-server/root_password password root1234"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root1234"
debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password root1234"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password root1234"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password root1234"
debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
sudo apt-get -y install mysql-server phpmyadmin

sudo mysql -uroot -proot1234 -e "CREATE DATABASE letmein"
sudo mysql -uroot -proot1234 -e "grant all privileges on letmein.* to 'root'@'localhost' identified by 'root1234'"

# boot
sudo systemctl enable nginx.service
sudo systemctl enable mysql.service

# install composer
sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/bin/composer

# yii2 asset
sudo -u vagrant composer global require "fxp/composer-asset-plugin:~1.1.1"

# virtual host
sudo cp /opt/host/app.conf /etc/nginx/sites-available/letmein.com.conf
sudo ln -s /etc/nginx/sites-available/letmein.com.conf /etc/nginx/sites-enabled/letmein.com.conf

# reboot server
sudo systemctl restart nginx
sudo systemctl restart mysql
