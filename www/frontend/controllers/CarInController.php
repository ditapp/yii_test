<?php

namespace frontend\controllers;

use Yii;
use common\models\CarIn;
use frontend\models\CarInSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * CarInController implements the CRUD actions for CarIn model.
 */
class CarInController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CarIn models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarInSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarIn model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CarIn model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CarIn();
        $model->stamp_to_out = '0';
        $model->parking_within_time = '1';

        if ($model->load(Yii::$app->request->post())) {

            $names = explode(" ", $model->full_name);
            $model->first_name = $names[0];
            $model->last_name = $names[1];

            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CarIn model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CarIn model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CarIn model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarIn the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarIn::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionStamp()
    {
        $type = Yii::$app->request->post('type', null);
        $id = Yii::$app->request->post('id', null);

        if (isset($id)) {
            $model = $this->findModel($id);




            if ($model->stamp_to_out == 0 && $type == 'NO') {
                $model->stamp_to_out = 1;
                $model->save();
            } elseif ($model->stamp_to_out == 1 && $type == 'YES') {
                $model->stamp_to_out = 0;
                $model->save();
            }



            $searchModel = new CarInSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            //   $url = Url::current();

            //$this->render('registerSuccess', Yii::$app->user->getFlash('register.success'));
            // return $this->refresh();

             echo Yii::$app->getRequest()->getQueryParam('sort');
            echo Yii::$app->request->baseUrl; 
            $kwai = 'ss';
            $this->debug_to_console($kwai);
            
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {

            $searchModel = new CarInSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    function debug_to_console($data) {
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

    echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
}
}
