<?php

namespace frontend\controllers;

use Yii;
use common\models\Owner;
use common\models\Qrcode;
use common\models\Device;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\helpers\thsms;
use common\helpers\qrcode_gen;


/**
 * OwnerController implements the CRUD actions for Owner model.
 */
class OwnerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Owner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Owner::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Owner model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

    }

    /**
     * Creates a new Owner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Owner();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Owner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Owner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

//-----------------Function-----------------//

    public function actionCheckLineid() {

      $session = Yii::$app->session;

    	$owner_info = Owner::findOne(["line_id" => $_POST['line_id']]);

    	if($owner_info) {

        $session->set('line_id', $_POST['line_id']);
    		return json_encode(["success" => true]);
    	}
    	else {

    		return json_encode(["success" => false]);
    	}
    }

    public function actionCheckid() {

    	$session = Yii::$app->session;

      $model = new Owner();

      if($session->get('identity_id') !== null) {

        return $this->render('checkotp', [
            'model' => $model,
        ]);
      }
      else {

        return $this->render('checkid', [
            'model' => $model,
        ]);
      }

    }

//     public function actionCheckOwner() {

//       $session = Yii::$app->session;

//       $owner_info = Owner::findOne(["identity_id" => $_POST['identity_id']]);
//       $response = [];

//       if($owner_info) {

//         $session->set('identity_id', $_POST['identity_id']);
//         $session->set('line_id', $_POST['line_id']);

//         $response = [
//             "success" => true
//         ];
//       }
//       else {
//         $response = [
//             "success" => false
//         ];
//       }

//       echo json_encode($response);

//     }

    public function actionManage(){

      $session = Yii::$app->session;

//       $model = new Owner();

      if($session->get('line_id') !== null) {

        $owner_info = Owner::findOne(["line_id" => $session->get('line_id')]);
        // $owner_info = Owner::findOne(["line_id" => 'U2f5b67e1d00abb2d42cab8fa512d5dfa']);
        if($owner_info) {

          $owner_info->name = $owner_info->first_name." ".$owner_info->last_name;
          $owner_info->floor = "49B";

          return $this->render('manage', [
              'model' => $owner_info,
          ]);
        }
        else {

          return $this->redirect(['checkid']);
        }

      }
      else {

        return $this->redirect(['checkid']);
      }
    }

//-----------------OTP Function-----------------//

    public function actionCheckotp() {

      $model = new Owner();

      return $this->render('checkotp', [
          'model' => $model,
      ]);
    }

    public function actionGetotp()
    {
        $session = Yii::$app->session;
        $sms = new thsms();

        $owner_info = Owner::findOne(["identity_id" => $_POST['identity_id']]);

        if($owner_info){

            if($owner_info->phone) {

                $session->set('identity_id', $_POST['identity_id']);
                $session->set('line_id', $_POST['line_id']);

                $otp_code = mt_rand(100000, 999999);

                //Session
                $session->set('password_otp',$otp_code);
                $session->set('expire_otp', date('Y-m-d H:i:s', strtotime('+60 second')));

                $phone = $owner_info->phone;
                $message = 'รหัส OTP คือ '.$otp_code.' สำหรับยืนยันตัวตนใน LINE@';
                $response = $sms->send($phone, $message);
                Yii::warning($otp_code, "OTP Code");

                if($response) {

                    $success = true;
                    $msg = 'รหัส OTP ส่งไปยังหมายเลขโทรศัพท์ '.$phone.' จะหมดอายุภายใน 60 วินาที';
                }
                else {

                    $success = false;
                    $msg = 'ไม่สามารถส่งรหัส OTP ไปยังหมายเลข '.$phone.' ได้';
                    $session->remove('identity_id');
                }
            }
            else {

                $success = false;
                $msg = 'คุณไม่มีหมายเลขโทรศัพท์อยู่ในระบบ กรุณาติดต่อนิติฯ เพื่อเพิ่มหมายเลขโทรศัพท์ของคุณ';
            }
        }
        else {
            $success = false;
            $msg = ' ไม่มีรหัสประจำตัวนี้ในระบบ';
        }

        echo json_encode([
            'success'   => $success,
            'content'   => $msg,
            // 'content' => "OTP คือ ".$otp_code
        ]);

    }

    function actionConfirmotp(){

        $session = Yii::$app->session;

        $diff = strtotime(date('Y-m-d H:i:s')) - strtotime($session->get('expire_otp'));

        if($session->get('password_otp') == $_POST['otp']){
            if($diff>0){
                $success = false;
                $msg = 'รหัส OTP ที่ได้รับหมดอายุแล้ว กรุณาทำการยืนยันตัวตนใหม่อีกครั้ง';
                $session->remove('identity_id');
            } else {

              if($session->get('identity_id')) {

                $owner_info = Owner::findOne(["identity_id" => $session->get('identity_id')]);

                if($owner_info) {
                  $owner_info->line_id = $session->get('line_id');
                  $owner_info->save();
                }
              }

                $success = true;
                $msg = 'รหัส OTP ได้รับการยืนยันแล้ว';
                $session->remove('identity_id');
            }
        } else {
          if($diff>0){
            $success = false;
            $msg = 'รหัส OTP ที่ได้รับหมดอายุแล้ว กรุณาทำการยืนยันตัวตนใหม่อีกครั้ง';
            $session->remove('identity_id');
          }
          else {
            $success = false;
            $msg = 'รหัส OTP ไม่ถูกต้อง';
          }
        }
        echo json_encode([
            'success'   =>$success,
            'content'   => $msg,
        ]);
    }

//-----------------Create QR Code Function-----------------//

    public function actionCreateQr() {

        date_default_timezone_set("Asia/Bangkok");

        $owner = new Owner();
        $qrcode_model = new Qrcode();

        if($_POST['qrcode_time']) {

            $owner_info = Owner::findOne(["line_id" => $_POST['line_id']]);
            $response = [];

            if($owner_info) {

                $qrcode_model->coowner_id = $owner_info->id;
                $qrcode_model->channel = 'Line@';
                $qrcode_model->created_tm = date("Y-m-d H:i:s");
                $qrcode_model->visit_time = date("Y-m-d H:i:s");
                $qrcode_model->options = 1;
                $qrcode_model->identity_id = $owner_info->identity_id;
                $qrcode_model->mobile = $owner_info->phone;
                $qrcode_model->effective_time = 1;
                $qrcode_model->first_name = $owner_info->first_name;
                $qrcode_model->last_name = $owner_info->last_name;
                $qrcode_model->room_number = $owner_info->room_no;

                $time_val = "";

                if($_POST['qrcode_time'] == 1) {
                    $time_val = "30 นาที";
                    $time_expire = 30;
                }
                else if($_POST['qrcode_time'] == 2) {
                    $time_val = "1 ชั่วโมง";
                    $time_expire = 60;
                }
                else if($_POST['qrcode_time'] == 3) {
                    $time_val = "2 ชั่วโมง";
                    $time_expire = 120;
                }
                else if($_POST['qrcode_time'] == 4) {
                    $time_val = "4 ชั่วโมง";
                    $time_expire = 240;
                }
                else if($_POST['qrcode_time'] == 5) {
                    $time_val = "6 ชั่วโมง";
                    $time_expire = 360;
                }
                else if($_POST['qrcode_time'] == 6) {
                    $time_val = "12 ชั่วโมง";
                    $time_expire = 720;
                }
                else if($_POST['qrcode_time'] == 7) {
                    $time_val = "1 วัน";
                    $time_expire = 1440;
                }
                else if($_POST['qrcode_time'] == 8) {
                    $time_val = "2 วัน";
                    $time_expire = 2880;
                }

                $post_url = Yii::$app->params['LiftQrCode'].Yii::$app->params['openToken'];

                $current_time = round(microtime(true) * 1000);

                // $sdkKey = array('9B3510DD8CE9AF678206D4B352F1A1D39E2058ABFFD7D9AA7E192122DA3D88CA4953574A61BE8407B10A863641AE55F49960');
                $sdkKey = array();

                // $device_val = Device::find()->where(['group' => ["V1"]])->all();
                // $device_val = Device::find()->where(['group' => ["1", "2", "02", "24", "25", "38"]])->all();
                $device_val = Device::find()->where(['group' => ["A", "LA", "B", "LB"]])->all();


                foreach ($device_val as $device_info) {
                  array_push($sdkKey, $device_info->sdk_key);
                }

                Yii::warning($sdkKey, "SDK KEY");

                $floor_arr = [1,2,3,4,5,6,7];

                // $floor_arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38];

                $post_data = array(
                                'lingLingId' =>  Yii::$app->params['linglingId'],
                                'sdkKeys' =>  $sdkKey,
                                'startTime' => $current_time,
                                'endTime' => $time_expire,
                                'effecNumber' => 1,
                                'autoFloorId' => 0,
                                'baseFloorId' => 1,
                                'userType' => 0,
                                'floorIds' => $floor_arr,
                                'strKey' =>  Yii::$app->params['strKey']
                            );

                $qrcode_info = $owner->manageDevice($post_url, $post_data);
                Yii::warning($qrcode_info->qrcodeKey, "qrkey");

                $qrkey = $qrcode_info->qrcodeKey;

                $qr_gen = new qrcode_gen();
                $qr_gen->renderQrcode($qrkey, null,'LINE@', $current_time);
                // $imageUrl = Yii::$app->params['apigenqr'].$qrkey;
                // Yii::warning($imageUrl, "IMAGE URL");
                // $imageUrl = Yii::$app->params['genqr_url'].'&w='.$qrkey;
                $imageUrl = Yii::$app->params['qrcode_url'].date("Y-m-d")."/LINE@_".$current_time.'.png';

                Yii::warning($imageUrl, "IMAGE URL");

                $message = "QR Code ของคุณสามารถใช้งานได้  ".$time_val."ค่ะ";

                $send_result = $owner->replyMessage($_POST['line_id'], $message, $imageUrl);

                $qrcode_model->qrcodekey = $qrkey;
                $qrcode_model->save();

                $response = [
                    "success" => $send_result
                ];

            }
            else {

                $response = [
                    "success" => false,
                    "content" => "ท่านไม่ได้รับอนุญาตในการทำงานส่วนนี้"
                ];
            }
        }
        else {

            $response = [
                "success" => false,
                "content" => "กรุณาเลือกเวลาสำหรับการใช้งาน QRCode"
            ];
        }

    	return json_encode($response);

    }


//===============================================================================================

    public function actionLogout() {

      $session = Yii::$app->session;

      if($_POST['identity_id']) {

        $owner_info = Owner::findOne(["identity_id" => $_POST['identity_id']]);

        if($owner_info) {

          $owner_info->line_id = null;
          $owner_info->save();

          $success = true;
          $msg = 'ออกจากระบบสำเร็จแล้ว';
          $session->set('line_id', null);
        }
        else {
          $success = false;
          $msg = 'ไม่สามารถทำการออกจากระบบได้';
        }
      }
      else {
        $success = false;
        $msg = 'ไม่สามารถทำการออกจากระบบได้';
      }

      echo json_encode([
          'success'   =>$success,
          'content'   => $msg,
      ]);
    }
//==============================================================================================

    public function actionSendsms(){
      return $this->render('sms');
    }
//==============================================================================================
    // public function actionLogoutline(){
    //
    // }
//==============================================================================================
    /**
     * Finds the Owner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Owner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Owner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
