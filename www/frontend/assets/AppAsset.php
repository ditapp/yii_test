<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\helpers\Url;
use Yii;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
      'js/filter.js',
      'js/check_lineid.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    public function init()
    {
    	// $ctr = Yii::$app->controller->id;
    	// $act = Yii::$app->controller->action->id;
      // 
    	// if($ctr == "owner") {
    	// 	if($act == 'checkid'){
    	// 		array_push($this->js, 'js/check_lineid.js');
    	// 	}
    	// 	else {
    	// 		array_push($this->js, 'js/manage_info.js');
    	// 	}
    	// }
    	
    	regisJsVars([
    			'URL_CHECKOWNER' => Url::to(['owner/check-owner']),
    			'URL_CHECKLINEID' => Url::to(['owner/check-lineid']),
    			'URL_CREATEQR' => Url::to(['owner/create-qr']),
          'URL_GETOPT' => Url::to(['owner/getotp']),
          'URL_CONFIRMOPT' => Url::to(['owner/confirmotp']),
          'URL_LOGOUT' => Url::to(['owner/logout']),
          // 'URL_CHECKINFO' => Url::to(['owner/checkinfo-owner'])
    	]);
    	
    	// return parent::init();
    }
}
