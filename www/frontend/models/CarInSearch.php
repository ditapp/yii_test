<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CarIn;

/**
 * CarInSearch represents the model behind the search form of `common\models\CarIn`.
 */
class CarInSearch extends CarIn
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'stamp_to_out'], 'integer'],
            [['first_name', 'last_name', 'license_plate', 'address', 'created_tm', 'mobile_phone', 'visit_to', 'identity_id', 'car_in_image1', 'car_in_image2', 'card_image', 'relationship', 'parking_within_time', 'full_name'], 'safe'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarIn::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_tm' => $this->created_tm,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'license_plate', $this->license_plate])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'mobile_phone', $this->mobile_phone])
            ->andFilterWhere(['like', 'visit_to', $this->visit_to])
            ->andFilterWhere(['like', 'identity_id', $this->identity_id])
            ->andFilterWhere(['like', 'car_in_image1', $this->car_in_image1])
            ->andFilterWhere(['like', 'car_in_image2', $this->car_in_image2])
            ->andFilterWhere(['like', 'card_image', $this->card_image])
            ->andFilterWhere(['like', 'relationship', $this->relationship])
            ->andFilterWhere(['like', 'parking_within_time', $this->parking_within_time])
            ->andFilterWhere(['like', 'stamp_to_out', $this->stamp_to_out]);

        return $dataProvider;
    }

    public function splitName()
    {
       $name =  explode(" ",$this->full_name);
       return $name;
    }

}
