$(function() {

	liff.init(function (data) {

        // window.alert(window.location.search);
        $.ajax({
    	    url: URL_CHECKLINEID,
    	    type: 'post',
    	    dataType: 'json',
    	    data: {line_id: data.context.userId},
    	    success: function(data) {
    	    	if(data.success) {
    	    		document.getElementById("form-validate").style.display = "none";
    	    		document.getElementById("form-requestqr").style.display = "block";
    	    	}
				else {
					document.getElementById("form-validate").style.display = "block";
					document.getElementById("form-requestqr").style.display = "none";
				}
    	    }
    	});

        $("#owner-line_id").val(data.context.userId);

    });

	 	document.getElementById('closewindowbutton').addEventListener('click', function () {
         liff.closeWindow();
    });

});

function checkCreateQR(){
//	var j$ = jQuery.noConflict();
	$.ajax({
	    url: URL_CREATEQR,
	    type: 'post',
	    dataType: 'json',
	    data: {line_id: $("#owner-line_id").val(), qrcode_time: $('#owner-qrcode_time').val()},
	    success: function(data) {
	        if(data.success){
//	        	window.alert(data.success);
	        	liff.closeWindow();
	        } else {
	        	console.log(data);
	        	window.alert(data.content);
	        }
	       return false;
	    }
	});
}

//if เก่า 
/*
 	if(data.success){
	         liff.sendMessages([
 				{
 					"type": "image",
 				    "originalContentUrl": data.image,
 				    "previewImageUrl": data.image
 				},
 	            {
 	              type: 'text',
 	              text: "QR Code ของคุณสามารถใช้งานได้ "+data.time_mes+"ค่ะ"
 	            }
 	          ]).then(function () {
 	             liff.closeWindow();
 	          }).catch(function (error) {
 	              window.alert("Error sending message: " + error);
 	          });
        	  
	        } else {
	        	console.log(data);
	        	window.alert(data.content);
	        }
 */

//function checkLicenceID(){
//var j$ = jQuery.noConflict();
//j$.ajax({
//    url: URL_CHECKOWNER,
//    type: 'post',
//    dataType: 'json',
//    data: {identity_id : $("#owner-identity_id").val(), line_id: $("#owner-line_id").val()},
//    success: function(data) {
//        if(data.success){
//			location.reload();
//        }
//		else {
//        	$('#check_fail').modal('show');
//        }
//       return false;
//    }
//});
//}
