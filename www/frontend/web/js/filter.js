//Get OTP Password 
function getOtp() {
    //console.log($('#form_otp').yiiActiveForm('validate', true));
    $.ajax({
        url: URL_GETOPT,
        type: 'post',
        dataType: 'json',
        data: {identity_id : $("#owner-identity_id").val(), line_id: $("#owner-line_id").val()},
        success: function(data) {
            if(data.success){
            	location.reload();
                alert(data.content);
            } else {
                alert(data.content);
            }
        }
    });

}

//Confirm OTP Password 
function confirmOtp() {
    $.ajax({
        url: URL_CONFIRMOPT,
        type: 'post',
        dataType: 'json',
        data: {otp : $("#owner-otp").val()},
        success: function(data) {
            if(data.success){
                alert(data.content);
                liff.closeWindow();
            } else {
                alert(data.content);
            }
        }
    });

}

//Logout LINE@
function logout() {
  $.ajax({
      url: URL_LOGOUT,
      type: 'post',
      dataType: 'json',
      data: {identity_id : $("#owner-identity_id").val()},
      success: function(data) {
          if(data.success){
              alert(data.content);
              liff.closeWindow();
          } else {
              alert(data.content);
          }
      }
  });
}
