<?php

use yii\web\View;
use yii\helper\Html;

function num($val)
{
	return number_format($val);
}

function return_format($val)
{
	if(!$val)
		return '-';

	$val = number_format($val);
	return "{$val} บาท";
}

function tel_number($value)
{
	$temp = [];
	$rows = explode(',', $value);
	foreach($rows as $tel)
	{
		$tel = trim($tel);
		$temp[] = "<a href=\"tel:{$tel}\">{$tel}</a>";
	}

	return implode(', ', $temp);
}

function regisJs($script, $position = View::POS_END)
{
	$script = preg_replace('/<[^>]*>/', '', $script);
	Yii::$app->view->registerJs($script, $position);
}

function regisJsVars($params)
{
	$tmp = '';
	foreach($params as $key => $val)
		$tmp .= "var {$key}='{$val}';";

	regisJs($tmp, $position = View::POS_BEGIN);
}

function script_start()
{
    ob_start();
}

function script_end()
{
    $content = ob_get_clean();
    regisJs($content);
}
