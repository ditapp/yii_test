<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
  	'modules' => [
  			'gii' => [
  					'class' => 'yii\gii\Module',
  					'allowedIPs' => ['*'] // adjust this to your needs
  			],
  	],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'class' => 'yii\web\Session',
            'cookieParams' => ['lifetime' => 60*60*60*12],
            'timeout' => 60*60*60*12, //session expire 1 month
            'useCookies' => true,
            // 'name' => 'advanced-frontend',
            // 'timeout' => 60*60*60*12, //1 month
        ],
        'log' => [
      		'traceLevel' => YII_DEBUG ? 3 : 0,
      		'targets' => [
      			[
      				'class' => 'yii\log\FileTarget',
      				'levels' => ['error', 'warning'],
      				'logFile' => '@app/runtime/logs/error.log',
      			],
      			[
      				'class' => 'yii\log\FileTarget',
      				'levels' => ['info'],
      				'logFile' => '@app/runtime/logs/info.log',
      			],
      		],
      	],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
