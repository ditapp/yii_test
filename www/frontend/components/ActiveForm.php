<?php

namespace frontend\components;

use yii\bootstrap\Html;

class ActiveForm extends \yii\bootstrap\ActiveForm
{
    public $fieldClass = 'frontend\components\ActiveField';
    public $fieldConfig = [
        'template' => "{input}\n{hint}\n{error}"
    ];

    public function label($model, $attribute)
    {
        if($model->isAttributeRequired($attribute))
            $options = ['class' => 'required'];

        return Html::activeLabel($model, $attribute, @$options);
    }
}
