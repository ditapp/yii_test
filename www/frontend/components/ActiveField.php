<?php

namespace frontend\components;

class ActiveField extends \yii\bootstrap\ActiveField
{
    public function dropDownList($items, $options = [])
    {
        $attribute = Html::getAttributeName($this->attribute);
        $options['data-val'] = $this->model->$attribute;

        return parent::dropDownList($items, $options);
    }
}
