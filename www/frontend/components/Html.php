<?php

namespace frontend\components;

use yii\bootstrap\BaseHtml;

class Html extends BaseHtml
{
    private static function addClass($options)
    {
        if(!isset($options['class']))
            $options['class'] = '';

        if(strpos($options['class'], 'form-control') === false)
            $options['class'] = $options['class'] . ' form-control';
        else
            $options['class'] = 'form-control';

        return $options;
    }

    public static function textInput($name, $value = null, $options = [])
    {
        $options = self::addClass($options);
        return parent::textInput($name, $value, $options);
    }

    public static function dropDownList($name, $selection = null, $items = [], $options = [])
    {
        $options = self::addClass($options);
        return parent::dropDownList($name, $selection, $items, $options);
    }
}

?>