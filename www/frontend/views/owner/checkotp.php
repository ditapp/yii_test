<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Owner */

// $this->title = 'กรุณายืนยันตัวตน';
// $this->params['breadcrumbs'][] = ['label' => 'Owners', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="owner-check-otp">

    <?= $this->render('_formotp', [
        'model' => $model,
    ]) ?>

</div>
