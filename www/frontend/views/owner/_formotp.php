<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use frontend\components\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Owner */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="owner-formotp" id="form-otp" style="display: block">
<h1><center><?= Html::encode("กรอก OTP ที่ได้รับจากทาง SMS") ?></center></h1>

    <?php $form = ActiveForm::begin(); ?>
  
    <div class="form-rows">
        <div class="row row-centered">
            <div class="col-md-3 col-centered">
                <?= $form->field($model, 'otp')->textInput(['maxlength' => 6,'placeholder'=>'รหัส OTP']) ?>
            </div>
        </div>
    </div>
    <div class="form-group" style="text-align:center">
        <?= Html::Button('ตกลง', ['id'=>'btn-check', 'onclick'=>'confirmOtp()','class' => 'btn btn-success']) ?>
    </div>
    
  </div>
