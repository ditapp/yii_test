<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use frontend\components\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Owner */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="owner-formcheck" id="form-validate" style="display: none">
<h1><center><?= Html::encode("กรุณายืนยันตัวตน") ?></center></h1><br>

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-rows">
        <div class="row row-centered">
            <div class="col-md-2 col-centered">
                <?= $form->label($model, 'identity_id')." :" ?>
            </div>
            <div class="col-md-3 col-centered">
                <?= $form->field($model, 'identity_id')->textInput(['maxlength' => 13,'placeholder'=>'เลขบัตรประชาชน 13 หลัก']) ?>
                <?= Html::activeHiddenInput($model, 'line_id') ?>
            </div>
        </div>
    </div>
    <br>
    <div class="form-group" style="text-align:center">
        <?= Html::Button('ตรวจสอบ', ['id'=>'btn-check', 'onclick'=>'getOtp()','class' => 'btn btn-success']) ?>
    </div>

    <div class="container">

         <!-- Modal1 -->
         <div class="modal fade" id="check_success" role="dialog">
           <div class="modal-dialog">

             <!-- Modal content-->
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title">ยืนยันตัวเองสำเร็จ</h4>
               </div>
               <div class="modal-body">
                 <p>คุณสามารถทำการขอ QR Code ได้ด้วยการกด "ขอ QR Code" ที่หน้าเมนู</p>
               </div>
               <div class="modal-footer">
                 <button id="closewindowbutton" type="button" class="btn btn-default" data-dismiss="modal">ตกลง</button>
               </div>
             </div>

           </div>
         </div>

         <!-- Modal2 -->
         <div class="modal fade" id="check_fail" role="dialog">
           <div class="modal-dialog">

             <!-- Modal content-->
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title">ไม่มีรหัสประจำตัวนี้ในระบบ</h4>
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">ตกลง</button>
               </div>
             </div>

           </div>
         </div>

       </div>

  </div>


<div class="owner-formcheck" id="form-requestqr" style="display: none">
<h1><center><?= Html::encode("กรุณาเลือกระยะเวลา") ?></center></h1>

    <?php $form = ActiveForm::begin(); ?>


	  <div class="row" align="center">
	  	<table>
	  		<tr>
	  			<td style="margin: 10px; padding: 10px;">
	  				<?= $form->field($model, 'qrcode_time')->dropDownList(['' => 'กรุณาเลือก']+[ '1' => '30 นาที' , '2' => '1 ชั่วโมง' , '3' => '2 ชั่วโมง', '4' => '4 ชั่วโมง', '5' => '6 ชั่วโมง', '6' => '12 ชั่วโมง', '7' => '1 วัน', '8' => '2 วัน']) ?>
	  				<?= Html::activeHiddenInput($model, 'line_id') ?>
	  			</td>

	  		</tr>
	  	</table>

      </div>


    <div class="form-group" style="text-align:center">
        <?= Html::Button('ตกลง', ['onClick'=>'checkCreateQR()' ,'class' => 'btn btn-success']) ?>
    </div>
  </div>
