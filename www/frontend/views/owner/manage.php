<?php

use yii\helpers\Html;
use frontend\components\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="form-customer form-customer-info">
    <center><h4>SETTING</h4></center><br>
    <?php $form = ActiveForm::begin(); ?>
    <div class="form-rows" style="padding-left:40px">
        <div class="row">
            <div class="col-sm-2 col-centered">
                <?= $form->label($model, 'name')." :" ?>
            </div>
            <div class="col-sm-2 col-centered">
                <?= $form->field($model, 'name')->textInput(['readonly'=>true]) ?>
                <?= Html::activeHiddenInput($model, 'identity_id') ?>
            </div>
        </div>
        <div class="row" style="padding-left:32px">
            <div class="col-sm-2 col-centered">
                <?= $form->label($model, 'room_no')." :" ?>
            </div>
            <div class="col-sm-2 col-centered">
                <?= $form->field($model, 'room_no')->textInput(['readonly'=>true])  ?>
            </div>
        </div>
        <div class="row" style="padding-left:37px">
            <div class="col-sm-2 col-centered">
                <?= $form->label($model, 'floor')." :" ?>
            </div>
            <div class="col-sm-2 col-centered">
                <?= $form->field($model, 'floor')->textInput(['readonly'=>true])  ?>
            </div>
        </div>

        </div>
    </div>
    <br>
    <div class="form-group" style="text-align:center">
        <?= Html::Button('LOGOUT', ['id'=>'btn-check', 'onclick'=>'logout()','class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
