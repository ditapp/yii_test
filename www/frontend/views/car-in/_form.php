<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CarIn */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-in-form">


    <?php $form = ActiveForm::begin(); ?>
    
     <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]+['placeholder' => 'ชือ - นามสกุล'])->label(false) ?>

   
    <?= $form->field($model, 'relationship')->dropDownList(['' => 'ความสัมพันธ์', '1' => 'พ่อ - แม่', '2' => 'พี่-น้อง', '3' => 'ลูก' , '4' => 'เพื่อน' , '5' => 'ญาติ'])->label(false) ?>

    <?= $form->field($model, 'visit_to')->textInput(['maxlength' => true]+['placeholder' => 'เลขที่ห้องที่มาติดต่อ'])->label(false) ?>

    <?= $form->field($model, 'license_plate')->textInput(['maxlength' => true]+['placeholder' => 'เลขทะเบียนรถ'])->label(false) ?>

    <?= $form->field($model, 'parking_within_time')->radioList(['1' => '1 ชั่วโมง', '3' => '3 ชั่วโมง', '5' => '5 ชั่วโมง'])->label(false) ?>

    
    <div class="form-group">
        <?= Html::submitButton('ตกลง', ['class' => 'btn btn-success']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
