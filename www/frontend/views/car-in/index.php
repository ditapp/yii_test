<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CarInSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="car-in-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <p>
        <?= Html::a('ลงทะเบียนสำหรับจอดรถ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        //'filterModel' => $searchModel,

        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            'license_plate',
            'created_tm',
            //'stamp_to_out',


            //'address',

            //'created_by',
            //'mobile_phone',
            //'visit_to',
            //'identity_id',
            //'car_in_image1',
            //'car_in_image2',
            //'card_image',
            //'relationship',
            //'parking_within_time',
            //'stamp_to_out,
            //'full_name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{stampButton}',  
                'buttons' => [
                    'stampButton' => function ($url, $model, $key) {
                        if ($model->stamp_to_out == '0') {
                            return Html::a('NO', ['stamp'], [
                                'class' => 'btn btn-danger', 'data-method' => 'POST',
                                'data-params' => [
                                    'type' => 'NO',
                                    'id' => $model->id,
                                ]
                            ]);
                        } elseif ($model->stamp_to_out == '1') {
                            return Html::a('YES', ['stamp'], [
                                'class' => 'btn btn-success', 'data-method' => 'POST',
                                'data-params' => [
                                    'type' => 'YES',
                                    'id' => $model->id,
                                ]
                            ]);
                        }
                    }
                ],
                'header' => 'แสตมป์'
            ]
        ],
    ]); ?>
</div>