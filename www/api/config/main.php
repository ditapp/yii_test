<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
  'id' => 'app-api',
  'basePath' => dirname(__DIR__),
  'controllerNamespace' => 'api\controllers',
  'bootstrap' => ['log'],
	'language' => 'th',
	'timeZone' => 'Asia/Bangkok',
  'modules' => [
    'v2' => [
        'basePath' => '@app/modules/v2',
        'class' => 'api\modules\v2\Module'
    ]
  ],
  'components' => [
    'user' => [
    	'identityClass' => 'common\models\User',
    	'enableSession' => false,
    ],
    'log' => [
    	'traceLevel' => YII_DEBUG ? 3 : 0,
    	'targets' => [
    		[
    			'class' => 'yii\log\FileTarget',
    			'levels' => ['error', 'warning'],
    			'logFile' => '@app/runtime/logs/error.log',
    		],
    		[
    			'class' => 'yii\log\FileTarget',
    			'levels' => ['info'],
    			'logFile' => '@app/runtime/logs/info.log',
    		],
    	],
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'request' => [
      'enableCookieValidation' => false,
      'parsers' => [
          'application/json' => 'yii\web\JsonParser',
      ],
    ],
    'response' => [
    	'format' => yii\web\Response::FORMAT_JSON,
    	'charset' => 'UTF-8',
    ],
    'urlManager' => [
        'class' => 'yii\web\UrlManager',
        // Disable r= routes
        'enablePrettyUrl' => true,
        // Disable index.php
        'showScriptName' => false,
          'rules' => [
            '<controller:\w+>/<id:\d+>' => '<controller>/view',
            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
          ],
      ],
  ],
  'params' => $params,
];
