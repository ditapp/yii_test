<?php

namespace api\controllers;

use common\models\Owner;
use common\models\Device;
use Yii;

class DeviceController extends \yii\web\Controller
{

    public function beforeAction($action)
  	{
  		$this->enableCsrfValidation = false;
  		return parent::beforeAction($action);
  	}

    public function actionAddDevice() {

      $owner_model = new Owner();
      $device_model = new Device();

      if($_POST) {

        if($_POST['version'] == 1) {

          $post_url = Yii::$app->params['addDevice'].Yii::$app->params['openToken'];

          $post_data = [
            'deviceName' => $_POST['deviceName'],
            'deviceCode' => $_POST['deviceCode']
          ];

          $result = $owner_model->manageDevice($post_url, $post_data);
        }
        else {

          $post_url = Yii::$app->params['addDevice'].Yii::$app->params['openTokenV2'];

          $post_data = [
            'deviceName' => $_POST['deviceName'],
            'deviceCode' => $_POST['deviceCode'],
            'strKey' => Yii::$app->params['strKeyV2']
          ];

          $result = $device_model->manageDevice($post_url, $post_data, $_POST['version']);
        }

        $device_query = Device::findOne(['serial_number' => $_POST['deviceCode']]);

        if($device_query) {

          $device_query->device_id = (string)$result->deviceId;
          $device_query->save(false);

          return array('result' => true, 'device_id' => $result->deviceId);

        }
        else {
          return array('result' => $result);
          // return array('result' => true, 'device_id' => $result->deviceId);
        }
      }
    }

    public function actionGeneratesdkKey() {

      $owner_model = new Owner();

      if($_POST) {

        $data_arr = explode(',', $_POST['deviceIds']);

        $post_url = Yii::$app->params['accessApiUrl']."cgi-bin/key/makeSdkKey/".Yii::$app->params['openToken'];

        $post_data = [
          'deviceIds' => $data_arr
        ];

        $result = $owner_model->manageDevice($post_url, $post_data);

        return array('result' => true, 'device_id' => $result);
      }
    }

    public function actionCheckStatusdevice() {

      $owner_model = new Owner();
      $post_url = Yii::$app->params['accessApiUrl']."cgi-bin/device/queryDeviceList/".Yii::$app->params['openToken'];

      $result = $owner_model->manageDevice($post_url);

      return array('result' => true, 'data' => $result);
    }

    public function actionDeleteDevice() {

      $owner_model = new Owner();
      $device_model = new Device();

      if($_POST) {

        if($_POST['version'] == 1) {
          $post_url = Yii::$app->params['accessApiUrl']."cgi-bin/device/delDevice/".Yii::$app->params['openToken'];
        }
        else {
          $post_url = Yii::$app->params['accessApiUrl']."cgi-bin/device/delDevice/".Yii::$app->params['openTokenV2'];
        }

        $post_data = [
          'deviceId' => $_POST['deviceId']
        ];

        $result = $device_model->manageDevice($post_url, $post_data, $_POST['version']);
        // $result = $owner_model->manageDevice($post_url, $post_data);

        return array('result' => true, 'device_id' => $result);
      }
    }

    public function openDoor($device_id) {

      $device_model = new Device();
      $owner_model = new Owner();

      $post_url = Yii::$app->params['OpenDoor'].Yii::$app->params['openToken'];
      $sdk_key = $device_model->getSdkKeyByDeviceid($device_id);

      $post_data = [
        'sdkKey' => $sdk_key
      ];

      $result = $owner_model->manageDevice($post_url, $post_data);

      Yii::warning($result, "OPEN DOOR LOG");

      if($result == "Operation successfully") {
        return true;
      }
      else {
        return false;
      }

  }
}

?>
