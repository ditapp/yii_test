<?php

namespace api\controllers;

use common\models\Qrcode;
use common\models\Owner;
use common\models\Qrcodelog;
use common\models\Address;
use common\models\Device;
use common\models\IotLog;
use common\models\Project;
use Yii;

class OwnerController extends \yii\web\Controller
{

	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	public function actionReceivelog() {

		date_default_timezone_set("Asia/Bangkok");

	    $qrcode_model = new Qrcode();
	    $qrlog_model = new Qrcodelog();
			$device_model = new Device();
			$owner_model = new Owner();
			$project_model = new Project();
			$iot_model = new IotLog();

	    $arr_val = $_POST;
			Yii::warning($_POST, "POST");
	    $jsonObj = json_decode($arr_val['noticeMsg']);

			Yii::warning($jsonObj->noticeName, "NOTI NAME");

	    if($jsonObj->noticeName == "visitorQrcodeOpen") {

	        $open_time = $jsonObj->msg->openTime / 1000;
	        $open_time_format = date('Y-m-d H:i:s', $open_time);
	        $qrkey = $jsonObj->msg->qrcodeKey;
	        $device_info = $device_model->getInfoByDeviceId($jsonObj->msg->deviceId);

	        $qr_info = $qrcode_model->getOwnerByQrkey($qrkey);

					$device_id = $device_info != [] ? $device_info->id : 0;
					$device_name = $device_info != [] ? $device_info->name : "";

	        $qrlog_model->qrcodekey = $qrkey;
	        $qrlog_model->device_id = strval($device_id);
	        $qrlog_model->opentime = $open_time_format;
	        $qrlog_model->created_tm = date('Y-m-d H:i:s');

	        if($qr_info) {

							$qrlog_model->project_code = $qr_info->project_code;
	            $qrlog_model->env = "Production";

							$project_info = $project_model->find()->where(['project_code' => $qr_info->project_code])->one();
		          $iot_info = $iot_model->find()->where(['project_id' => $project_info->id])->one();

							if($iot_info) {

								$data_log = [
									'loginfo' => [
					          'locations' => $iot_info->locations,
					          'registries' => $iot_info->registries,
					          'devices' => $iot_info->devices,
					          'project' => $iot_info->project,
					          'key' => $iot_info->key_file
					        ],
					        'used' => [
										'qrcode_key' => $qrkey,
					          'device' => $device_name,
					          'opened_time' => round($open_time)
					        ]
					      ];

					      $owner_model->sendLog($data_log);

							}

	            $this->sendScanNotification($qr_info, $open_time, $device_id);
	        }
	        else {

							$this->sendScanNotificationDev();
	            $qrlog_model->env = "Development";
							$qrlog_model->project_code = "2891";
	        }

	        Yii::warning($arr_val, "log_callback");
	        Yii::warning($qrkey, "QR Key");

	        $qrlog_model->save();
	    }

	    return ["status" => "success"];

	}

	private function sendScanNotification($qr_info, $open_time, $device_id) {

	    $owner_model = new Owner();
			$address_model = new Address();

        if($qr_info->channel == "Line@") {

            $owner_info = Owner::findOne(['id' => $qr_info->coowner_id]);

						 if($owner_info) {

							 $open_time_format = date('d/m/Y H:i', $open_time);
	             $message = "Qr code ที่คุณสร้างถูกใช้ไปแล้วในเวลา ".$open_time_format;
	             $line_id = $owner_info->line_id;

	             $owner_model->replyMessage($line_id, $message);
						 }
        }
				else if($qr_info->channel == "Home Service") {

					if($device_id == 5 || $device_id == 6 || $device_id == 2 || $device_id == 3 || $device_id == 4) {

						if($device_id == 5 || $device_id == 6) {

							$message_th = "QR ที่คุณสร้างสำหรับ ".$qr_info->first_name." ".$qr_info->last_name." ได้ถูกสแกนที่ล็อบบี้แล้ว";
							$message_en = "Your Visitor QR for ".$qr_info->first_name." ".$qr_info->last_name." has been scanned at the lobby";
					  }
					  else if($device_id == 2 || $device_id == 3 || $device_id == 4) {

							$message_th = "QR ที่คุณสร้างสำหรับ ".$qr_info->first_name." ".$qr_info->last_name." ได้ถูกสแกนที่ลิฟต์แล้ว";
							$message_en = "Your Visitor QR for ".$qr_info->first_name." ".$qr_info->last_name." has been scanned at the elevator";
					  }

					  $post_url = Yii::$app->params['noti_url']."/notification/unit/".$qr_info->room_number."/user/".$qr_info->uuid;
					  $post_data = [
						  "message" =>
						  [
							  "th" => $message_th,
							  "en" => $message_en
							  ]
						  ];

					  $owner_model->sendNotification($post_url, $post_data);
					}
				}
		}

		private function sendScanNotificationDev() {

		    $owner_model = new Owner();

				$post_url = Yii::$app->params['noti_url']."/notification/unit/2891-099999/user/RZOy3VQCC0cjOeR6a7kcyehL8I63";
				$post_data = ["message" => ["th" => "QR ที่คุณสร้างสำหรับ (ชื่อนามสกุล) ได้ถูกสแกนที่ล็อบบี้/ลิฟต์แล้ว", "en" => "Your Visitor QR for (ชื่อนามสกุล) has been scanned at the lobby/elevator"]];

				$owner_model->sendNotification($post_url, $post_data);

		}

}
