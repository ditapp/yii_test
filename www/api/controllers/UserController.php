<?php
namespace api\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\filters\auth\HttpBearerAuth;

use common\models\User;
use common\models\Owner;
use common\models\Qrcode;
use common\models\Address;
use common\models\Project;
use common\models\Device;
use common\models\IotLog;
use \LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use \LINE\LINEBot\MessageBuilder\ImageMessageBuilder;
use \LINE\LINEBot\MessageBuilder\MultiMessageBuilder;
use \LINE\LINEBot\HTTPClient\CurlHTTPClient;
use \LINE\LINEBot;
use common\helpers\thsms;
use yii\web\UploadedFile;

class UserController extends ActiveController
{
    public $modelClass = 'common\models\User';

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
    	if ($action->id == 'callback') {
    		$this->enableCsrfValidation = false;
    	}

    	return parent::beforeAction($action);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['genqr'],
        ];
        return $behaviors;

    }

    public function actionLogin(){

      $message_error = "";
      $message_error = $this->Validation($_POST, "login");
      if($message_error != "") {
        Yii::$app->response->statusCode = 400;
        return ["status" => 0, "message" => $message_error];
      }

      $user = User::findByUsername($_POST['username']);
      if(!empty($user)){
          if($user->validatePassword($_POST['password'])){

//                 $user->generateAuthKey();
//                 $user->generatePasswordResetToken();
//                 $user->save(false);

              Yii::$app->response->statusCode = 200;
              $response = [
                  'status' => 'success',
                  'message' => 'Login Complete!',
                  'data' => [
                      'token' => "Bearer ".$user->auth_key,
                  ]
              ];
        }
        else{

          Yii::$app->response->statusCode = 400;
          $response = [
            'status' => 'error',
            'message' => 'Username or Password is wrong!'
          ];
        }
      }
      else{

        Yii::$app->response->statusCode = 400;
        $response = [
          'status' => 'error',
          'message' => 'Username or Password is wrong!'
        ];
      }

      return $response;
    }

//-------------------//Genarate QR Code function//-------------------//

    public function actionGenqr() {

      date_default_timezone_set("Asia/Bangkok");

      $sms = new thsms();
      $owner = new Owner();
      $qrcode_model = new Qrcode();
      $address_model = new Address();
      $device_model = new Device();
      $project_model = new Project();
      $iot_model = new IotLog();

      $message_error = "";
      $message_error = $this->Validation($_POST, "genqr");
      if($message_error != "") {
        Yii::$app->response->statusCode = 400;
        return ["status" => 0, "message" => $message_error];
      }

      $current_time = date("Y-m-d H:i:s");

      Yii::$app->response->statusCode = 200;

      //variable of home service application
      $options = !empty($_POST['option']) ? $_POST['option'] : 1;
      $visit_time = !empty($_POST['visit_time']) ? $_POST['visit_time'] : date("Y-m-d H:i:s");
      $language = !empty($_POST['language']) ? $_POST['language'] : "th";
      $identity_type = !empty($_POST['identity_type']) ? $_POST['identity_type'] : 1;

      $first_name = preg_replace('/\s+/', '', $_POST['first_name']);
      $last_name = preg_replace('/\s+/', '', $_POST['last_name']);

      $device_info = $device_model->getSdkKeyGroup($options, $_POST['room_number'], $_POST['channel']);

      Yii::warning($device_info, "LOG DEVICE");

      if(empty($device_info)){
        Yii::$app->response->statusCode = 400;
        return ["status" => false, "title" => "LET ME IN", "message" => 'กรุณาตรวจสอบหมายเลขห้องที่มาติดต่อ'];
      }

       //save value to qrcode table
       if($identity_type == 1) {
           $qrcode_model->identity_id = $_POST['identity_id'];
       }
       else {
           $qrcode_model->passport_no = $_POST['identity_id'];
       }
       $qrcode_model->channel = $_POST['channel'];
       $qrcode_model->mobile = $_POST['mobile'];
       $qrcode_model->room_number = $_POST['room_number'];

       $qrcode_model->options = $options;
       $qrcode_model->visit_time = $visit_time;
       $qrcode_model->created_tm = $current_time;
       $qrcode_model->identity_type = $identity_type;
       $qrcode_model->first_name = $first_name;
       $qrcode_model->last_name = $last_name;
       $qrcode_model->language = $language;

       $uuid = !empty($_POST['uuid']) ? $_POST['uuid'] : "";
       $project_code = $_POST['channel'] == "Kiosk" ? $_POST['project_cod'] : $_POST['project_code'];

       $qrcode_model->uuid = $uuid;
       $qrcode_model->project_code = $project_code;

       if($project_code != "" && $project_code != NULL) {
         $project_info = $project_model->find()->where(['project_code' => $project_code])->one();
         $iot_info = $iot_model->find()->where(['project_id' => $project_info->id])->one();
       }

       if($_POST['channel'] == "Kiosk") {

          $visitor_img = UploadedFile::getInstanceByName('visitor_img');
          $camera_img = UploadedFile::getInstanceByName('camera_img');

          if(!empty($visitor_img)) {

            $filename_visitor = 'visitor_'.date('Y_m_d_H_i').'_'.mt_rand(100000, 999999).'.'.$visitor_img->extension;
            $qrcode_model->visitor_img_url = Yii::$app->params['url_visitor_img'].$filename_visitor;
            $visitor_img->saveAs(Yii::$app->params['visitor_img_path'].$filename_visitor);
          }

          if(!empty($camera_img)) {

            $filename_camera = 'camera_'.date('Y_m_d_H_i').'_'.mt_rand(100000, 999999).'.'.$camera_img->extension;
            $qrcode_model->camera_img_url = Yii::$app->params['url_camera_img'].$filename_camera;
            $camera_img->saveAs(Yii::$app->params['camera_img_path'].$filename_camera);
          }

           $effective_time = 1;
           $time_expire = 1440;
           $device_info['floor'] = [1];

           $start_time = strtotime($current_time) * 1000;

           $qrcode_info = $owner->getQrCodeKey($time_expire, $effective_time, $start_time, $device_info);

           Yii::warning($qrcode_info->qrcodeKey, "qrkey_kiosk");

           $imageUrl = Yii::$app->params['genqr_url'].'&w='.$qrcode_info->qrcodeKey;

           Yii::warning($imageUrl, "image qr code");

           $shortlink_info = $qrcode_model->createShortlink(urlencode($imageUrl));

           Yii::warning($shortlink_info, "short link info");

           $phone = $_POST['mobile'];
           $message_sms = $language == "th" ? 'เปิดลิงค์ เพื่อเปิด QR Code สำหรับใช้ในการแสกนเข้า Lobby: '.$shortlink_info->shortLink : 'Open this URL and use the QR code to gain access when enter the lobby. '.$shortlink_info->shortLink;
           $sms_result = $sms->send($phone, $message_sms);

           $unit_code = $address_model->getUnitcodeByRoom($_POST['room_number']);

           if($unit_code != "") {

             $post_url = Yii::$app->params['noti_url']."/notification/unit/".$unit_code;
             $post_data = [
               "message" => [
                  "th" => "คุณ".$first_name." ".$last_name." ได้ลงทะเบียนขอติดต่อคุณที่ล็อบบี้ มีข้อสงสัยโปรดแจ้งนิติบุคคลที่ 02-116-2805-07",
                  "en" => "Your visitor: ".$first_name." ".$last_name." has registered at the lobby. If you are not familiar with this guest, please contact Juristic Person at Tel: 02 116 2805-7"
                ]
             ];

            $owner->sendNotification($post_url, $post_data);
           }

           if($sms_result) {

               $title = $language == "th" ? "เสร็จสิ้น" : "Success";
               $message = $language == "th" ? 'ระบบกำลังส่ง QR Code ให้ท่านทาง SMS เบอร์ '.$phone : 'QR Code will be sent to you by SMS to '.$phone;

               $response = [
                   'status' => true,
                   'title' => $title,
                   'message' => $message
               ];

           }
           else {

               $title = "LET ME IN";
               $message = $language == "th" ? 'ขออภัย Application ยังไม่พร้อมใช้งานในขณะนี้' : "Sorry, this application is not available right now.";

               Yii::$app->response->statusCode = 400;
               $response = [
                   'status' => false,
                   'title' => $title,
                   'message' => $message
               ];
           }

        }
        else {

          $effective_time = 1;
          $time_expire = 1439;

          $start_time = strtotime(date('Y-m-d', strtotime($visit_time))) * 1000;

          $qrcode_info = $owner->getQrCodeKey($time_expire, $effective_time, $start_time, $device_info);
          Yii::warning($qrcode_info->qrcodeKey, "qrkey_home");

          Yii::$app->response->statusCode = 200;
          $response = [
              'status' => 'success',
              'message' => 'Generated Complete!',
              'qrcode_key' => $qrcode_info->qrcodeKey
          ];

        }

      $qrcode_model->effective_time = $effective_time;
      $qrcode_model->qrcodekey = $qrcode_info->qrcodeKey;

      $qrcode_model->save();

      if($options == 1) {
        $option_message = "Lobby";
      }
      else {
        $option_message = "Lobby&Lift";
      }

      if($iot_info) {

        $data_log = [
          'loginfo' => [
            'locations' => $iot_info->locations,
            'registries' => $iot_info->registries,
            'devices' => $iot_info->devices,
            'project' => $iot_info->project,
            'key' => $iot_info->key_file
          ],
          'requested' => [
            'channel' => $_POST['channel'],
            'visitor_name' => $first_name." ".$last_name,
            'identity_id' => $_POST['identity_id'],
            'room_no' => $_POST['room_number'],
            'visit_time' => strtotime($visit_time),
            'mobile' => $_POST['mobile'],
            'option' => $option_message,
            'qrcode_key' => $qrcode_info->qrcodeKey,
            'uuid' => $uuid,
            'project' => $project_info->name,
            'create_time' => strtotime($current_time),
          ]
        ];

        $owner->sendLog($data_log);

      }

      return $response;
    }

    public function actionHashpass() {

      if(!empty($_POST['password'])) return ["password" => Yii::$app->security->generatePasswordHash($_POST['password'])];
      else return ["message" => "password is empty!"];

    }

    private function Validation($post, $action) {

      if($action == "login"){

        if(empty($post['username']) || empty($post['password'])) return "Username or Password is empty!";
      }
      else if ($action == "genqr") {

        if(empty($post['channel'])) return "Missing 'channel' parameter!";

        $language = !empty($post['language']) ? $post['language'] : "th";

        if($language == "th") {

          if(($post['channel']) == 'Kiosk') {

            if(empty($post['identity_type'])) return "กรุณาเลือกระหว่างบัตรประชาชนกับพาสปอร์ต";

            if($post['identity_type'] == 1) {

              if(empty($post['identity_id'])) return "กรุณากรอกเลขประจำตัวประชาชน";
              if(strlen($post['identity_id']) != 13) return "หมายเลขประจำตัวประชาชนต้องมี 13 หลัก";
            }
            else {

              if(empty($post['identity_id'])) return "กรุณากรอกหมายเลขพาสปอร์ต";
            }

          }
          else {

            if(empty($post['identity_id'])) return "กรุณากรอกเลขประจำตัวประชาชนหรือหมายเลขพาสปอร์ต";
          }

          if(strlen($post['mobile']) != 10) return "หมายเลขโทรศัพท์ต้องมี 10 หลัก";
          if(empty($post['first_name'])) return "กรุณากรอกชื่อ";
          if(empty($post['last_name'])) return "กรุณากรอกนามสกุล";
          if(empty($post['room_number'])) return "กรุณากรอกหมายเลขห้องที่มาติดต่อ";

        }
        else {

          if(($post['channel']) == 'Kiosk') {

            if(empty($post['identity_type'])) return "Please select ID card or Passport.";

            if($post['identity_type'] == 1) {

              if(empty($post['identity_id'])) return "Please fill in your ID card number.";
              if(strlen($post['identity_id']) != 13) return "ID card number have to be 13 digit.";
            }
            else {

              if(empty($post['identity_id'])) return "Please fill in your Passport number.";
            }
          }
          else {

            if(empty($post['identity_id'])) return "Please fill in your ID number or Passport number.";
          }

          if(strlen($post['mobile']) != 10) return "Mobile phone numner have to be 10 digit.";
          if(empty($post['first_name'])) return "Please fill in your first name.";
          if(empty($post['last_name'])) return "Please fill in your last name.";
          if(empty($post['room_number'])) return "Please fill in room number.";

        }

      }
        return "";
    }

//-------------------//Web hook function for LINE Bot//-------------------//

    public function actionWebhook(){

    	if(!empty($_POST)) {

    		$username = !empty($_POST['username']) ? $_POST['username'] : "";
//     		$test = Yii::$app->request->post(); //$test['username']
    		$response = [
    			"username" => $username
    		];

    		return $response;
    	}
    	else {

        include '../../common/helpers/phpqrcode/qrlib.php';

      	$channel_token = 'P34EbmE4+HNIu3B400u73gY8us9RfUbzZSgIKCUv4ZIW4rmWpa1yXa6W9jPEI+RwMRMI1ojD4ZnZdQPnAFGf08C5CC55+KWb7ai2IIwV7GZVZ/BwUxYmxTC1WVLMkiDIyxr3ryebfoE0qDlGCfcC3gdB04t89/1O/w1cDnyilFU=';

      	$httpClient = new CurlHTTPClient($channel_token);
      	$bot = new LINEBot($httpClient, ['channelSecret' => 'ea81ac85ccac8c157ae37935e2ab25eb']);

      	$json_string = file_get_contents('php://input');
      	$jsonObj = json_decode($json_string);

      	$message_id = $jsonObj->events[0]->message->id;
      	$message_type = $jsonObj->events[0]->message->type;

      	$testUrl = "https://qr-official.line.me/M/E6cdhXVlSz.png";
      	$imageUrl = "https://api.letmein.asia/gen_qrcode.php?w=".$testUrl;

      	$replyToken = $jsonObj->events[0]->replyToken;

      	$QRCode = ['QRCode', 'qrcode', 'qr', 'QRcode'];

      	if(in_array($jsonObj->events[0]->message->text, $QRCode)) {

      		$multipleMessageBuilder = new MultiMessageBuilder();

      		$multipleMessageBuilder->add(new ImageMessageBuilder($imageUrl, $imageUrl))
      		->add(new TextMessageBuilder("QR Code ของคุณสามารถใช้งานได้ 2 วันค่ะ"));

      		$response = $bot->replyMessage($replyToken, $multipleMessageBuilder);
      	}
      	else {

  //     		$textMessageBuilder = new TextMessageBuilder($jsonObj->events[0]->message->text);
  //     		$response = $bot->replyMessage($replyToken, $textMessageBuilder);
      	}

      	if ($response->isSucceeded()) {
      		echo 'Succeeded!';
      		return;
      	}
     }

    }

}
?>
