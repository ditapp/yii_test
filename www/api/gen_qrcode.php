<?php

	include '../common/helpers/phpqrcode/qrlib.php';

	$get = str_replace("gen_qrcode.php?w=", "", $_GET['w']);

	$logo_path = 'https://www.letmein.asia/imgs/letmein.png';
	$filepath = 'img/qrcode/'.$get.'.png';

	QRcode::png($get,$filepath,QR_ECLEVEL_H, 4);

	$QR = imagecreatefrompng($filepath);

	// START TO DRAW THE IMAGE ON THE QR CODE
	$logo = imagecreatefromstring(file_get_contents($logo_path));

	$QR_width = imagesx($QR);
	$QR_height = imagesy($QR);

	$logo_width = imagesx($logo);
	$logo_height = imagesy($logo);

	// Scale logo to fit in the QR Code
	$logo_qr_width = $QR_width/3;
	$scale = $logo_width/$logo_qr_width;
	$logo_qr_height = $logo_height/$scale;

	imagecopyresampled($QR, $logo, $QR_width/3, $QR_height/3, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);

	imagepng($QR,$filepath);
	// END OF DRAW

	/**
	 * As this example is a plain PHP example, return
	 * an image response.
	 *
	 * Note: you can save the image if you want.
	 */
	// header('Content-type: image/png');
	// imagepng($QR);
	// imagedestroy($QR);


?>
