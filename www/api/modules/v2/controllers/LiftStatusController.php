<?php

namespace api\modules\v2\controllers;

use Yii;
use common\models\LiftStatus;
use common\models\Queue;
use common\models\Project;
use yii\filters\auth\HttpBearerAuth;

class LiftStatusController extends \yii\web\Controller
{

  public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

  public function behaviors()
	{
			$behaviors = parent::behaviors();
			$behaviors['authenticator'] = [
					'class' => HttpBearerAuth::className(),
					'only' => ['getliftstatus'],
			];
			return $behaviors;

	}

  public function actionGetliftstatus() {

    $lift_model = new LiftStatus();
    $queue_model = new Queue();
    $project_model = new Project();


    if(isset($_GET['pid'])) {

      $project_code = $_GET['pid'];

      $project_info = $project_model->find()->where(['project_code' => $project_code])->one();
      $project_id = $project_info->id;

      $lift_info = $lift_model->find()->where(['project_id' => $project_id])->all();
      $queue_info = $queue_model->find()->where(['project_id' => $project_id])->all();

      $elevator = [];
      $reservation = [];

      for($i=0; $i<count($lift_info); $i++) {

        $lift_json = new \stdClass();
        $lift_json->unit = $lift_info[$i]->id;
        $lift_json->status = $lift_info[$i]->lift_status;
        $lift_json->cardId = $lift_info[$i]->card_no;
        $lift_json->timeRemain = $lift_info[$i]->arriving_time;
        $lift_json->message = $lift_info[$i]->vehicle_status;

        array_push($elevator, $lift_json);
      }

      for($j=0; $j<12; $j++) {

        $queue_json = new \stdClass();
        $queue_json->no = $queue_info[$j]->queue_no;
        $queue_json->cardId = $queue_info[$j]->card_no;
        $queue_json->timeRemain = $queue_info[$j]->estimate_time;

        array_push($reservation, $queue_json);

      }

      return ["elevator" => $elevator, "reservation" => $reservation];

    }
  }

}
