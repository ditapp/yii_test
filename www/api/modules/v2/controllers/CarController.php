<?php

namespace api\modules\v2\controllers;

use Yii;
use common\models\CarIn;
use common\models\CarOut;
use common\models\User;
use yii\web\UploadedFile;
use yii\filters\auth\HttpBearerAuth;
use common\helpers\socketio\Broadcast;
use common\models\Project;

class CarController extends \yii\web\Controller {

    public function beforeAction($action)
  	{
  		$this->enableCsrfValidation = false;
  		return parent::beforeAction($action);
  	}

  	public function behaviors()
  	{
  			$behaviors = parent::behaviors();
  			$behaviors['authenticator'] = [
  					'class' => HttpBearerAuth::className(),
  					'only' => ['come-in', 'go-out', 'capture-image'],
  			];
  			return $behaviors;

  	}

    //function สำหรับให้ app เรียกตอนรถออก
    public function actionComeIn() {

       date_default_timezone_set("Asia/Bangkok");
       $carIn_model = new CarIn();
       $user_model = new User();

       if($_POST) {

         // Yii::warning($user_model->getUserIdByToken(), "USER ID");

         $current_time = date("Y-m-d H:i:s");

         $message_error = "";
         $message_error = $this->Validation($_POST, "comein");
         Yii::$app->response->statusCode = 400;
         if($message_error != "") return ["status" => 0, "message" => $message_error];

         $carIn_model->first_name = $_POST['first_name'];
         $carIn_model->last_name = $_POST['last_name'];
         $carIn_model->license_plate = $_POST['license_plate'];
         $carIn_model->address = $_POST['address'];
         $carIn_model->created_tm = $current_time;
         $carIn_model->created_by = $user_model->getUserIdByToken();
         $carIn_model->mobile_phone = $_POST['mobile_phone'];
         $carIn_model->visit_to = $_POST['visit_to'];
         $carIn_model->identity_id = $_POST['identity_id'];
         $carIn_model->car_in_image1 = $_POST['car_in_image1'];
         $carIn_model->car_in_image2 = $_POST['car_in_image2'];

         $card_image = UploadedFile::getInstanceByName('card_image');

         if(!empty($card_image)) {

           $filename_card = 'cardid_'.date('Y-m-d_H-i').'_'.time().'.'.$card_image->extension;

           $card_image->saveAs(Yii::$app->params['card_img_path'].$filename_card);

           $carIn_model->card_image = Yii::$app->params['url_card_img'].$filename_card;
         }

         if($carIn_model->save(false)) {

           Yii::$app->response->statusCode = 200;
           return ["status" => 1, "message" => "บันทึกข้อมูลเรียบร้อยแล้ว"];
         }
         else {

           Yii::$app->response->statusCode = 400;
           return ["status" => 0, "message" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง"];
         }

       }
    }

    //function สำหรับให้ app เรียกตอนรถออก
    public function actionGoOut() {

       date_default_timezone_set("Asia/Bangkok");
       $carIn_model = new CarIn();
       $user_model = new User();
       $carOut_model = new CarOut();

       if($_POST) {

         $current_time = date("Y-m-d H:i:s");

         $message_error = "";
         $message_error = $this->Validation($_POST, "goout");
         Yii::$app->response->statusCode = 400;
         if($message_error != "") return ["status" => 0, "message" => $message_error];

         $car_info = $carOut_model->find()->where(['license_plate' => $_POST['license_plate']])->orderBy(['parking_time' => SORT_DESC])->one();

         // Yii::warning($car_info->get_money, "CarinOf_GetMoney");

         $car_info->get_money = $_POST['get_money'];
         $car_info->change = $_POST['change'];
         $car_info->car_out_image1 = $_POST['car_out_image1'];
         $car_info->car_out_image2 = $_POST['car_out_image2'];

         if($car_info->update(false)) {

           Yii::$app->response->statusCode = 200;
           return ["status" => 1, "message" => "บันทึกข้อมูลเรียบร้อยแล้ว"];
         }
         else {

           Yii::$app->response->statusCode = 400;
           return ["status" => 0, "message" => "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง"];
         }

       }
    }

    public function actionCaptureImage() {

      if($_POST) {

        date_default_timezone_set("Asia/Bangkok");

        $username = Yii::$app->params['cap_user_dev'];
        $password = Yii::$app->params['cap_pass_dev'];
        $url = Yii::$app->params['cap_url_dev'];

        if($_POST['type'] == 'in') {

          $img_path = Yii::$app->params['carin_img_path'];
          $filename_image = 'carin_'.date('Y-m-d_H-i').'_'.time().'.jpg';
          $url_img = Yii::$app->params['url_carin_img'];
        }
        else {

          $img_path = Yii::$app->params['carout_img_path'];
          $filename_image = 'carout_'.date('Y-m-d_H-i').'_'.time().'.jpg';
          $url_img = Yii::$app->params['url_carout_img'];
        }

        $command = "ffmpeg -rtsp_transport tcp -i rtsp://".$username.":".$password."@".$url." -f image2 ".$img_path.$filename_image." >/dev/null 2>&1";
        exec($command, $output);

        $url_image = $url_img.$filename_image;

        if($_POST['type'] == 'in') {
          $data = ['status' => true, 'carin_image' => $url_image, 'userId' => 1];
        }
        else {
          $data = ['status' => true, 'carout_image' => $url_image, 'userId' => 1];
        }

        Broadcast::emit('carimage', $data);

        Yii::$app->response->statusCode = 200;
        return $data;

      }
      else {

        $data = ['status' => fail, 'message' => 'Missing Parameter!!'];

        Yii::$app->response->statusCode = 200;
        return $data;
      }

    }

    //function สำหรับดึงข้อมูลรถขาออก ให้ app ส่งทะเบียนรถหลังจากที่ทำ ocr มา
    public function getCarinfo($license_plate, $token_bearer) {

      date_default_timezone_set("Asia/Bangkok");
      $carIn_model = new CarIn();
      $user_model = new User();
      $carOut_model = new CarOut();
      $project_model = new Project();

      $car_info = $carIn_model->find()->where(['license_plate' => $license_plate])->orderBy(['created_tm' => SORT_DESC])->one();

      $current_time = date("Y-m-d H:i:s");
      $parking_start = $car_info->created_tm;

      $parking_time_hour =  $parking_time_minute = round((strtotime($current_time) - strtotime($parking_start)) / (60*60));

      if (!empty($token_bearer)) {
          if (preg_match('/Bearer\s(\S+)/', $token_bearer, $matches)) {
              $token = $matches[1];
          }
      }

      if(empty($token)) return ["message" => "Token Empty!"];

      $user_info = $user_model->findIdentityByAccessToken($token);

      $project_info = $project_model->find()->where(['id' => $user_info->property_project_id])->one();
      $parking_fee = $project_info->parking_fee;

      $parking_net = (int)$parking_time_hour*(int)$parking_fee;

      $carOut_model->parking_time = $parking_start;
      $carOut_model->created_by = $user_info->id;
      $carOut_model->license_plate = $license_plate;
      $carOut_model->parking_time_cal = $parking_time_hour;
      $carOut_model->net_parking_fee = $parking_net;

      if($carOut_model->save(false)) {

        $data = [
          'carin_image1' => $car_info->car_in_image1,
          'carin_image2' => $car_info->car_in_image2,
          'parking_time' => $parking_time_hour,
          'net_parking_fee' => $parking_net,
          'address' => $car_info->address,
          'name' => $car_info->first_name." ".$car_info->last_name,
          'identity_id' => $car_info->identity_id,
          'mobile' => $car_info->mobile_phone,
          'visit_to' => $car_info->visit_to
        ];

        return $data;
      }

    }

    private function Validation($post, $action) {

      if($action == "comein") {
        if(empty($post['first_name'])) return "กรุณากรอกชื่อ";
        if(empty($post['last_name'])) return "กรุณากรอกนามสกุล";
        if(empty($post['license_plate'])) return "กรุณาใส่หมายเลขทะเบียนรถ";
        if(strlen($post['mobile_phone']) != 10) return "หมายเลขโทรศัพท์ต้องมี 10 หลัก";
        if(empty($post['visit_to'])) return "กรุณาใส่หมายเลขห้องที่ต้องการติดต่อ";
        if(empty(UploadedFile::getInstanceByName('card_image'))) return "ไม่สามารถระบุรูปบัตรประจำตัว หรือ รูปพาสปอร์ตได้";
        if(empty($post['identity_id'])) return "กรุณากรอกหมายเลขประจำตัวประชาชน หรือ หมายเลขพาสปอร์ต";

      }
      else if($action == "goout") {
        if(empty($post['get_money'])) return "กรุณากรอกค่าที่จอดรถ";
        if(empty($post['license_plate'])) return "กรุณาใส่หมายเลขทะเบียนรถ";
      }

        return "";
    }

}
?>
