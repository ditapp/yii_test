<?php
namespace api\modules\v2\controllers;

use Yii;
use common\models\User;
use common\models\Owner;
use common\models\Qrcode;
use common\models\Address;
use common\models\Settings;
use common\helpers\thsms;
use common\models\Project;
use common\models\Device;
use yii\filters\auth\HttpBearerAuth;
use yii\web\UploadedFile;

class KioskController extends \yii\web\Controller
{

  public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

  public function behaviors()
	{
			$behaviors = parent::behaviors();
			$behaviors['authenticator'] = [
					'class' => HttpBearerAuth::className(),
					'only' => ['genqr'],
			];
			return $behaviors;

	}

  public function actionLogin() {

    date_default_timezone_set("Asia/Bangkok");

    $user_model = new User();
    $address_model = new Address();

    if($_POST) {

      $message_error = "";
      $message_error = $this->Validation($_POST, "login");
      if($message_error != "") return ["status" => 0, "message" => $message_error];

      $user = $user_model->findByUsername($_POST['username']);

      if(!empty($user)) {

        if($user->validatePassword($_POST['password'])){

            $address_info = $address_model->find()->where(['project_id' => $user->property_project_id])->one();
            $address_no = explode('/', $address_info->room_number);

            Yii::$app->response->statusCode = 200;
            $response = [
              'status' => 1,
              'message' => 'เข้าสู่ระบบสำเร็จ',
              'data' => [
                  'token' => "Bearer ".$user->auth_key,
                  'user' => $user->first_name,
                  'address_no' => $address_no[0]
              ]
            ];
        }
        else {

          Yii::$app->response->statusCode = 400;
          $response = [
            'status' => 0,
            'message' => 'รหัสผ่านของคุณไม่ถูกต้อง'
          ];
        }
      }
      else {

        Yii::$app->response->statusCode = 400;
        $response = [
          'status' => 0,
          'message' => 'ไม่พบชื่อผู้ใช้งานในระบบ'
        ];
      }

      return $response;
    }
  }

  public function actionGenqr() {

    date_default_timezone_set("Asia/Bangkok");

    $owner_model = new Owner();
    $qrcode_model = new Qrcode();
    $sms = new thsms();
    $address_model = new Address();
    $settings_model = new Settings();
    $project_model = new Project();
    $device_model = new Device();

    $current_time = date("Y-m-d H:i:s");

    $project_id = Yii::$app->user->identity->property_project_id;

    $message_error = "";
    $message_error = $this->Validation($_POST, "genqr");
    if($message_error != "") {
      Yii::$app->response->statusCode = 400;
      return ["status" => false, "title" => "LET ME IN", "message" => $message_error];
    }

    $language = !empty($_POST['language']) ? $_POST['language'] : "th";

    $first_name = preg_replace('/\s+/', '', $_POST['first_name']);
    $last_name = preg_replace('/\s+/', '', $_POST['last_name']);

    $effective_time = $settings_model->getValuebyKey('effective_time', $project_id);
    $option = $settings_model->getValuebyKey('option', $project_id);
    $time_expire = $settings_model->getValuebyKey('time_expire', $project_id);
    $floor = $settings_model->getValuebyKey('floor', $project_id);

    $device_info = $device_model->getSdkKeyGroup($option, $_POST['room_number'], 'Kiosk');

    if(empty($device_info)){
      Yii::$app->response->statusCode = 400;
      return ["status" => false, "title" => "LET ME IN", "message" => 'กรุณาตรวจสอบหมายเลขห้องที่มาติดต่อ'];
    }
    $device_info['floor'] = [(int)$floor];

    $project_info = $project_model->getProjectinfoById($project_id);

    //save value to qrcode table
    if($_POST['identity_type'] == 1) {
        $qrcode_model->identity_id = $_POST['identity_id'];
    }
    else {
        $qrcode_model->passport_no = $_POST['identity_id'];
    }
    $qrcode_model->channel = 'Kiosk';
    $qrcode_model->mobile = $_POST['mobile'];
    $qrcode_model->options = $option;
    $qrcode_model->visit_time = $current_time;
    $qrcode_model->created_tm = $current_time;
    $qrcode_model->identity_type = $_POST['identity_type'];
    $qrcode_model->room_number = $_POST['room_number'];
    $qrcode_model->first_name = $first_name;
    $qrcode_model->last_name = $last_name;
    $qrcode_model->language = $language;
    $qrcode_model->effective_time = $effective_time;
    $qrcode_model->project_code = $project_info->project_code;
    $qrcode_model->address = $_POST['address'] != "" ? $_POST['address'] : "";

    if(!empty(UploadedFile::getInstanceByName('visitor_img'))) {

      $visitor_img = UploadedFile::getInstanceByName('visitor_img');
      $filename_visitor = 'visitor_'.date('Y_m_d_H_i').'_'.mt_rand(100000, 999999).'.'.$visitor_img->extension;
      $qrcode_model->visitor_img_url = Yii::$app->params['url_visitor_img'].$filename_visitor;
      $visitor_img->saveAs(Yii::$app->params['visitor_img_path'].$filename_visitor);

    }

    $start_time = strtotime($current_time) * 1000;

    $qrcode_info = $owner_model->getQrCodeKey($time_expire, $effective_time, $start_time, $device_info);

    Yii::warning($project_id, "Project");

    $imageUrl = Yii::$app->params['genqr_url'].'&w='.$qrcode_info->qrcodeKey."&pid=".$project_id;
    $shortlink_info = $qrcode_model->createShortlink(urlencode($imageUrl));

    // Yii::warning($shortlink_info, "short link info");

    $phone = $_POST['mobile'];
    $message_sms = $language == "th" ? $settings_model->getValuebyKey('sms_th', $project_id)." ".$shortlink_info->shortLink : $settings_model->getValuebyKey('sms_en', $project_id).$shortlink_info->shortLink;
    $sms_result = $sms->send($phone, $message_sms);

    $unit_code = $address_model->getUnitcodeByRoom($_POST['room_number']);

    Yii::warning($unit_code, "Unit Code");

    if($unit_code != "") {

      $post_url = Yii::$app->params['noti_url']."/notification/unit/".$unit_code;
      $post_data = [
        "message" => [
           "th" => "คุณ".$first_name." ".$last_name." ได้ลงทะเบียนขอติดต่อคุณที่ล็อบบี้ มีข้อสงสัยโปรดแจ้งนิติบุคคลที่ 02-116-2805-07",
           "en" => "Your visitor: ".$first_name." ".$last_name." has registered at the lobby. If you are not familiar with this guest, please contact Juristic Person at Tel: 02 116 2805-7"
         ]
      ];
      $owner_model->sendNotification($post_url, $post_data);
    }

    if($sms_result) {

        $title = $language == "th" ? "เสร็จสิ้น" : "Success";
        $message = $language == "th" ? 'ระบบกำลังส่ง QR Code ให้ท่านทาง SMS เบอร์ '.$phone : 'QR Code will be sent to you by SMS to '.$phone;
        Yii::$app->response->statusCode = 200;
        $response = [
            'status' => true,
            'title' => $title,
            'message' => $message,
            // 'message_sms' => $message_sms
        ];

        $qrcode_model->qrcodekey = $qrcode_info->qrcodeKey;
        $qrcode_model->save();

    }
    else {

        $title = "LET ME IN";
        $message = $language == "th" ? 'ขออภัย Application ยังไม่พร้อมใช้งานในขณะนี้' : "Sorry, this application is not available right now.";

        Yii::$app->response->statusCode = 400;
        $response = [
            'status' => false,
            'title' => $title,
            'message' => $message
        ];
    }

    return $response;

  }

  public function actionUploadimg() {

    date_default_timezone_set("Asia/Bangkok");

    $response = ['message' => "Doesn't have any data."];

    // Yii::warning($_POST, "POST DATA");

    if($_FILES) {

      $img1 = UploadedFile::getInstanceByName('img1');
      $filename1 = 'img1_'.date('Y_m_d_H_i').'_'.mt_rand(100000, 999999).'.'.$img1->extension;
      $img_url1 = Yii::$app->params['url_visitor_img'].$filename1;
      $img1->saveAs(Yii::$app->params['visitor_img_path'].$filename1);

      $img2 = UploadedFile::getInstanceByName('img2');
      $filename2 = 'img2_'.date('Y_m_d_H_i').'_'.mt_rand(100000, 999999).'.'.$img2->extension;
      $img_url2 = Yii::$app->params['url_visitor_img'].$filename2;
      $img2->saveAs(Yii::$app->params['visitor_img_path'].$filename2);

      $response = [
        'img1' => $img_url1,
        'img2' => $img_url2
      ];
    }

    return $response;
  }

  private function Validation($post, $action) {

    if($action == "login"){

      if(empty($post['username'])) return "กรุณากรอกชื่อผู้ใช้งาน";
      if(empty($post['password'])) return "กรุณากรอกรหัสผ่าน";
    }
    else if ($action == "genqr") {

      if(empty($post['identity_type'])) return "กรุณาเลือกระหว่างบัตรประชาชนหรือพาสปอร์ต";

      if($post['identity_type'] == 1) {
        if(empty($post['identity_id'])) return "กรุณากรอกเลขประจำตัวประชาชน";
        if(strlen($post['identity_id']) != 13) return "หมายเลขประจำตัวประชาชนต้องมี 13 หลัก";
      }
      else {
        if(empty($post['identity_id'])) return "กรุณากรอกหมายเลขพาสปอร์ต";
      }

      if(strlen($post['mobile']) != 10) return "หมายเลขโทรศัพท์ต้องมี 10 หลัก";
      if(empty($post['first_name'])) return "กรุณากรอกชื่อ";
      if(empty($post['last_name'])) return "กรุณากรอกนามสกุล";
      if(empty($post['room_number'])) return "กรุณากรอกหมายเลขห้องที่มาติดต่อ";
      // if(empty(UploadedFile::getInstanceByName('visitor_img'))) return "กรุณาอัพโหลดรูปผู้มาติดต่อ";

    }

      return "";
  }

}

?>
