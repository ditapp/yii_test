<?php

namespace api\modules\v2\controllers;

use Yii;
use common\models\Address;
use yii\web\UploadedFile;
use yii\filters\auth\HttpBearerAuth;
use PHPExcel;
use PHPExcel_IOFactory;

class AddressController extends \yii\web\Controller {

  public function beforeAction($action)
  {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
  }

  public function behaviors()
  {
      $behaviors = parent::behaviors();
      $behaviors['authenticator'] = [
          'class' => HttpBearerAuth::className(),
          'only' => ['import-excel'],
      ];
      return $behaviors;

  }

  public function actionImportExcel() {

    date_default_timezone_set("Asia/Bangkok");

    if($_FILES) {

      $file_excel = UploadedFile::getInstanceByName('excelfile');
      $file_name = $file_excel->getBaseName().'_'.date('YmdHi').'.'.$file_excel->extension;
      $file_excel->saveAs(Yii::$app->params['file_path'].$file_name);

      $objPHPExcel = new PHPExcel();

      $inputFileType = PHPExcel_IOFactory::identify(Yii::$app->params['file_path'].$file_name);
      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load(Yii::$app->params['file_path'].$file_name);

      $sheet = $objPHPExcel->getSheet(6);
      $highestRow = $sheet->getHighestRow();
      $highestColumn = $sheet->getHighestColumn();

      // Yii::warning($highestRow, "ROW LOG");
      // Yii::warning($highestColumn, "Column LOG");


      // $rowData = $sheet ->rangeToArray('A6'. ':' .$highestColumn.'6', NULL, TRUE, FALSE);
      //
      // Yii::warning($rowData, "DATA LOG");

      $project_id = (string)Yii::$app->user->identity->property_project_id;

      for ($row = 6; $row <= 466; ++$row) {

        $rowData = $sheet ->rangeToArray('A'.$row. ':' .$highestColumn.$row, NULL, TRUE, FALSE);

        $building = $rowData[0][1];
        $floor = $rowData[0][2];
        $room_no = $rowData[0][7];
        $unit_id = $rowData[0][8];

        $address_model = new Address();
        $address_model->unit_code = $unit_id;
        $address_model->project_id = $project_id;
        $address_model->room_number = $room_no;
        $address_model->floor = number_format($floor,0,'.','');
        $address_model->building = $building;
        $address_model->save();

      }

      return ["message" => "Import Data Success!!", "Row" => $row];

    }
  }

}
