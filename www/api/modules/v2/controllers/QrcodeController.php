<?php

namespace api\modules\v2\controllers;

use Yii;
use yii\filters\auth\HttpBearerAuth;
use common\models\Qrcode;
use common\models\Device;
use common\helpers\thsms;


class QrcodeController extends \yii\web\Controller {

    public function beforeAction($action)
    {
      $this->enableCsrfValidation = false;
      return parent::beforeAction($action);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [''],
        ];
        return $behaviors;

    }

    public function actionGenqr() {

      date_default_timezone_set("Asia/Bangkok");

      $sms = new thsms();
      $qrcode_model = new Qrcode();
      $device_model = new Device();

      $message_error = "";
      $message_error = $this->Validation($_POST, "genqr");
      if($message_error != "") return ["status" => 0, "message" => $message_error];

      $current_time = date("Y-m-d H:i:s");

      $options = !empty($_POST['option']) ? $_POST['option'] : 1;
      $visit_time = !empty($_POST['visit_time']) ? $_POST['visit_time'] : date("Y-m-d H:i:s");
      $language = !empty($_POST['language']) ? $_POST['language'] : "th";
      $identity_type = !empty($_POST['identity_type']) ? $_POST['identity_type'] : 1;

      $first_name = preg_replace('/\s+/', '', $_POST['first_name']);
      $last_name = preg_replace('/\s+/', '', $_POST['last_name']);

      $device_info = $device_model->getDeviceIdGroup($options, $_POST['room_number'], $_POST['channel']);

      if(empty($device_info)){
        Yii::$app->response->statusCode = 400;
        return ["status" => false, "title" => "LET ME IN", "message" => 'กรุณาตรวจสอบหมายเลขห้องที่มาติดต่อ'];
      }

      //save value to qrcode table
      if($identity_type == 1) {
          $qrcode_model->identity_id = $_POST['identity_id'];
      }
      else {
          $qrcode_model->passport_no = $_POST['identity_id'];
      }
      $qrcode_model->channel = $_POST['channel'];
      $qrcode_model->mobile = $_POST['mobile'];
      $qrcode_model->room_number = $_POST['room_number'];

      $qrcode_model->options = $options;
      $qrcode_model->visit_time = $visit_time;
      $qrcode_model->created_tm = $current_time;
      $qrcode_model->identity_type = $identity_type;
      $qrcode_model->first_name = $first_name;
      $qrcode_model->last_name = $last_name;
      $qrcode_model->language = $language;

      $qrcode_model->uuid = !empty($_POST['uuid']) ? $_POST['uuid'] : "";
      $qrcode_model->project_code = $_POST['channel'] == "Kiosk" ? $_POST['project_cod'] : $_POST['project_code'];

      $effective_time = 1;
      $time_expire = 1439;

      $start_time = strtotime(date('Y-m-d', strtotime($visit_time))) * 1000;

      $qrcode_info = $qrcode_model->getQrcodeKey($time_expire, $effective_time, $start_time, $device_info);
      Yii::warning($qrcode_info->qrcodeKey, "qrkey_home");

      Yii::$app->response->statusCode = 200;
      $response = [
          'status' => 'success',
          'message' => 'Generated Complete!',
          'qrcode_key' => $qrcode_info->qrcodeKey
      ];

      $qrcode_model->effective_time = $effective_time;
      $qrcode_model->qrcodekey = $qrcode_info->qrcodeKey;

      $qrcode_model->save();

      return $response;

    }

    public function actionAddaccesscard() {

        $device_model = new Device();

        $post_url = Yii::$app->params['addCard'].Yii::$app->params['openTokenV2'];

        // return['uId' => $_POST['cardUids'], 'deviceId' => $_POST['deviceIds']];

        $uIds = [$_POST['cardUids']];
        $device_group = explode(',', $_POST['deviceIds']);

        $post_data = [
          'cardUids' => $uIds,  //Card Uid specify by HEX 8 bit.
          'deviceIds' => $device_group,  //Group of devices.
          'endTime' => 65535,   //End time of card. (Maxdays: 65535)
          'cardType' => 2, //type 2 is QR Code.
        ];

        $result = $device_model->manageDevice($post_url, $post_data, 2);

        if($result->statusCode == "1") {

            Yii::$app->response->statusCode = 200;
            return ['status' => $result->statusCode, 'message' => $result->responseResult];
        }
        else {
            Yii::warning($result, 'RESULT ADD CARD');
        }
    }

    public function actionAddcardlift() {

        $device_model = new Device();

        $post_url = Yii::$app->params['addCardforLift'].Yii::$app->params['openTokenV2'];

        $uIds = [$_POST['cardUids']];
        $floor = [$_POST['floor']];

        $post_data = [
          'cardUids' => $uIds,  //Card Uid specify by HEX 8 bit.
          'autoFloorId' => 0,
          'baseFloorId' => 1,
          'userType' => 4, //type 4 is QR Code.
          'floorIds' => $floor
        ];

        $result = $device_model->manageDevice($post_url, $post_data, 2);

        if($result->statusCode == "1") {

            Yii::$app->response->statusCode = 200;
            return ['status' => $result->statusCode, 'message' => $result->responseResult];
        }
        else {
            Yii::warning($result, 'RESULT UPDATE CARD');
        }
    }

    public function actionGenqrcode() {

      date_default_timezone_set("Asia/Bangkok");

      $device_model = new Device();

      $post_url = Yii::$app->params['addQrcode'].Yii::$app->params['openTokenV2'];

      $visit_time = date("Y-m-d H:i:s");

      $effective_time = 1;
      $start_date = strtotime(date('Y-m-d', strtotime("2019-08-03 18:15:00")));
      $time_expire = strtotime('+1439 minutes', $start_date) * 1000;
      $time_start = $start_date * 1000;

      // return['start_time' => $time_start, 'end_time' => $time_expire, 'date_start' => date("Y-m-d H:i:s", $time_start/1000), 'date_end' => date("Y-m-d H:i:s", $time_expire/1000)];

      $post_data = [
        'funcFlag' => 1,  //Function identification: standard QR code. 0 is offline, 1 is online
        'userFlag' => $_POST['cardUids'], //Use Card Uid.
        'userType' => 1, //0 is Home User, 1 is APP visitor, 2 is Kiosk visitor
        'startTime' => $time_start,
        'endTime' => $time_expire,
        'effecNumber' => $effective_time,
        'strKey' => Yii::$app->params['strKeyV2']
      ];

      $result = $device_model->manageDevice($post_url, $post_data, 2);

      if($result->statusCode == "1") {

          Yii::$app->response->statusCode = 200;
          return [
            'status' => $result->statusCode,
            'message' => 'Generated Complete!',
            'qrcode_key' => $result->responseResult->qrcodeKey
          ];
      }
      else {
          Yii::warning($result, 'RESULT GEN QRCODE');
          return [
            'status' => $result->statusCode,
            'message' => $result->responseResult
          ];
      }
    }

    private function Validation($post, $action) {

      if($action == "login"){

        if(empty($post['username']) || empty($post['password'])) return "Username or Password is empty!";
      }
      else if ($action == "genqr") {

        if(empty($post['channel'])) return "Missing 'channel' parameter!";

        $language = !empty($post['language']) ? $post['language'] : "th";

        if($language == "th") {

          if(empty($post['identity_id'])) return "กรุณากรอกเลขประจำตัวประชาชนหรือหมายเลขพาสปอร์ต";
          if(strlen($post['mobile']) != 10) return "หมายเลขโทรศัพท์ต้องมี 10 หลัก";
          if(empty($post['first_name'])) return "กรุณากรอกชื่อ";
          if(empty($post['last_name'])) return "กรุณากรอกนามสกุล";
          if(empty($post['room_number'])) return "กรุณากรอกหมายเลขห้องที่มาติดต่อ";

        }
        else {

          if(empty($post['identity_id'])) return "Please fill in your ID number or Passport number.";
          if(strlen($post['mobile']) != 10) return "Mobile phone numner have to be 10 digit.";
          if(empty($post['first_name'])) return "Please fill in your first name.";
          if(empty($post['last_name'])) return "Please fill in your last name.";
          if(empty($post['room_number'])) return "Please fill in room number.";

        }

      }
        return "";
    }
}
