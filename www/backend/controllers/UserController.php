<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\Project;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\UserSearch;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
          'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                  [
                      'actions' => ['index','view','create','update','delete','validation'],
                      'allow' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == 1,
                      'roles' => ['@'],
                  ],
              ],
          ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionValidation(){

      $model = new User;

      if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
          Yii::$app->response->format ='json';
          return ActiveForm::validate($model);
      }

    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new UserSearch();
        $projectcheck = Yii::$app->user->identity->property_project_id;

        if ($projectcheck != ""){
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $dataProvider->query->andWhere('property_project_id='.Yii::$app->user->identity->property_project_id);
        }else{
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $model = $this->findModel($id);

      if($model->gender==1){
        $model->gender="ชาย";
      }
      else {
        $model->gender ="หญิง";
      }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $project_model = new Project();

        $project_info = $project_model->getAll();
        $project_list = ArrayHelper::map($project_info,'id','name');

        if ($model->load(Yii::$app->request->post())) {

          $model->setPassword($model->password_hash);
          $model->generateAuthKey();
          $model->generatePasswordResetToken();
          $model->created_tm = date('Y-m-d H:i:s');
          $model->updated_tm = date('Y-m-d H:i:s');
          $model->created_by = Yii::$app->user->id;
          $model->updated_by = Yii::$app->user->id;

          $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'project_list' => $project_list
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->gender==1){
          $model->gender="ชาย";
        }
        else {
          $model->gender ="หญิง";
        }

        if($model->property_project_id != NULL) {
          $model->project_name = [$model->property_project_id => $model->project->name];
        }
        else {
          $model->project_name = [];
        }

        if ($model->load(Yii::$app->request->post())) {

            $model->updated_tm = date('Y-m-d H:i:s');
            $model->updated_by = Yii::$app->user->id;

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
