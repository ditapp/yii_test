<?php

namespace backend\controllers;

use Yii;
use common\models\Owner;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\OwnerSearch;
use backend\models\Ownerform;
use yii\widgets\ActiveForm;
/**
 * OwnerController implements the CRUD actions for Owner model.
 */
class OwnerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
          'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                  [
                      'actions' => ['index','view','create','update','delete','validation'],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
                  [
                      'actions' =>[]
                  ],
              ],
          ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionValidation(){

      $model = new Ownerform;

      if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
          Yii::$app->response->format ='json';
          return ActiveForm::validate($model);
      }

    }
    /**
     * Lists all Owner models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new OwnerSearch();
        $projectcheck = Yii::$app->user->identity->property_project_id;

        if ($projectcheck != ""){
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $dataProvider->query->andWhere('property_project_id='.Yii::$app->user->identity->property_project_id);
        }else{
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Owner model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $model = $this->findModel($id);

      if($model->gender==1){
        $model->gender="ชาย";
      }
      else {
        $model->gender ="หญิง";
      }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Owner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ownerform();

        if ($model->load(Yii::$app->request->post())) {

            $model->created_tm = date('Y-m-d H:i:s');
            $model->updated_tm = date('Y-m-d H:i:s');
            $model->created_by = Yii::$app->user->id;
            $model->updated_by = Yii::$app->user->id;
            $model->property_project_id = Yii::$app->user->identity->property_project_id; //get project

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Owner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->gender==1){
          $model->gender="ชาย";
        }
        else {
          $model->gender ="หญิง";
        }

        if ($model->load(Yii::$app->request->post())) {

          $model->updated_tm = date('Y-m-d H:i:s');
          $model->updated_by = Yii::$app->user->id;

          $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Owner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Owner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Owner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Owner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
