<?php

namespace backend\controllers;

use Yii;
use common\models\Device;
use common\models\Owner;
use common\models\Project;
use backend\models\DeviceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * DeviceController implements the CRUD actions for Device model.
 */
class DeviceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
          'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                  [
                      'actions' => ['index','view','create','update','delete','validation'],
                      'allow' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == 1,
                      'roles' => ['@'],
                  ],
              ],
          ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionValidation(){

      $model = new Device;

      if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
          Yii::$app->response->format ='json';
          return ActiveForm::validate($model);
      }

    }

    /**
     * Lists all Device models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeviceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Device model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Device model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Device();
        $project_model = new Project();

        $project_info = $project_model->getAll();
        $project_list = ArrayHelper::map($project_info,'id','name');

        if ($model->load(Yii::$app->request->post())) {

          $model->created_tm = date('Y-m-d H:i:s');
          $model->updated_tm = date('Y-m-d H:i:s');
          $model->created_by = Yii::$app->user->id;
          $model->updated_by = Yii::$app->user->id;

          if($model->serial_number != "" || $model->serial_number != null) {

            if($model->version == 1) {

              $post_url = Yii::$app->params['addDevice'].Yii::$app->params['openToken'];
              $post_data = array(
                   'deviceName' => $model->name,
                   'deviceCode' => $model->serial_number
              );
            }
            else if($model->version == 2){

              $post_url = Yii::$app->params['addDevice'].Yii::$app->params['openTokenV2'];
              $post_data = array(
                   'deviceName' => $model->name,
                   'deviceCode' => $model->serial_number,
                   'strKey' => Yii::$app->params['strKeyV2']
              );
            }

            $result = $model->manageDevice($post_url, $post_data, $model->version);

            if($result->statusCode == "1") {

              $model->device_id = strval($result->responseResult->deviceId);

              if($model->device_id != "" || $model->device_id != null) {

                $device_id = $result->responseResult->deviceId;

                if($model->version == 1) {
                  $post_url = Yii::$app->params['makeSdkkey'].Yii::$app->params['openToken'];
                }
                else if($model->version == 2){
                  $post_url = Yii::$app->params['makeSdkkey'].Yii::$app->params['openTokenV2'];
                }

                $post_data = array(
                    'deviceIds' => array($device_id)
                );

                $sdk_key = $model->manageDevice($post_url, $post_data, $model->version);

                if($sdk_key->statusCode == "1") {
                  $model->sdk_key = $sdk_key->responseResult->$device_id;
                }
              }
            }
            else if($result->statusCode == "30001") {
              Yii::warning("มีการเพิ่มอุปกรณ์รหัส ".$model->serial_number." ในระบบแล้ว", "Add Device Error");
            }
          }

          // Yii::warning($model, "Model");

          if($model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
          }
        }

        return $this->render('create', [
            'model' => $model,
            'project_list' => $project_list
        ]);
    }

    /**
     * Updates an existing Device model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->version == 1) {
          $model->version = 'Version1';
          $strKey = Yii::$app->params['strKey'];
          $version = 1;
        }
        else if($model->version == 2){
          $model->version = 'Version2';
          $strKey = Yii::$app->params['strKeyV2'];
          $version = 2;
        }

        $model->device_project = [$model->project_id => $model->project->name];

        $device_id = $model->device_id;

        if ($model->load(Yii::$app->request->post())) {

          if($model->serial_number != "" || $model->serial_number != null) {

             $post_url = Yii::$app->params['updateDevice'].Yii::$app->params['openToken'];

             $post_data = array(
                'deviceId' => $device_id,
                'newDeviceName' => $model->name,
                'newDeviceCode' => $model->serial_number,
                'strKey' => $strKey
             );

             $result = $model->manageDevice($post_url, $post_data, $version);

             if($result->statusCode == "1" && $model->save()) {
               return $this->redirect(['view', 'id' => $model->id]);
             }
             else {
               Yii::warning($result, "Update Device Error");
             }
          }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Device model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Device model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Device the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Device::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
