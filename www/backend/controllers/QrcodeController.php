<?php

namespace backend\controllers;

use Yii;
use backend\models\Qrcodeback;
use backend\models\QrcodeSearch;
use backend\models\QrcodelogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * QrcodeController implements the CRUD actions for Qrcode model.
 */
class QrcodeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
          'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                  [
                      'actions' => ['logout', 'index','view'],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Qrcode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QrcodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Qrcode model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $searchModel = new QrcodelogSearch();
        $searchModel->qrcodekey = $model->qrcodekey;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        if($model->qrcodelog != "" || $model->qrcodelog != null){
          $model->qrcodestatus = 'ถูกใช้งานแล้ว';
        } else{
          $model->qrcodestatus = 'ยังไม่ถูกใช้งาน';
        }

        if($model->options == 1){
          $model->options="Lobby";
        }
        elseif ($model->options == 2) {
          $model->options ="Lobby,Lift";
        }
        // elseif ($model->options == 3) {
        //   $model->options ="All";
        // }

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Qrcode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Qrcode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Qrcodeback::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
