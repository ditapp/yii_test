<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VisitorCard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="visitor-card-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'card_no')->textInput() ?>

    <?= $form->field($model, 'qrcode_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project_id')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'gen_time')->textInput() ?>

    <?= $form->field($model, 'used_time')->textInput() ?>

    <?= $form->field($model, 'usage')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
