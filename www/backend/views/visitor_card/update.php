<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VisitorCard */

$this->title = 'Update Visitor Card: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Visitor Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="visitor-card-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
