<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VisitorCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Visitor Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visitor-card-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Visitor Card', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'card_no',
            'qrcode_key',
            'project_id',
            'created_by',
            //'updated_by',
            //'gen_time',
            //'used_time',
            //'usage',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
