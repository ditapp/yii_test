<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CarOut */

$this->title = 'Update Car Out: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Car Outs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="car-out-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
