<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CarOut */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-out-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parking_time')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'license_plate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parking_time_cal')->textInput() ?>

    <?= $form->field($model, 'net_parking_fee')->textInput() ?>

    <?= $form->field($model, 'get_money')->textInput() ?>

    <?= $form->field($model, 'change')->textInput() ?>

    <?= $form->field($model, 'car_out_image1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car_out_image2')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
