<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CarOutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Car Outs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-out-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Car Out', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'parking_time',
            'created_by',
            'license_plate',
            'parking_time_cal:datetime',
            //'net_parking_fee',
            //'get_money',
            //'change',
            //'car_out_image1',
            //'car_out_image2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
