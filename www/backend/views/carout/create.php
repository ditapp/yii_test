<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CarOut */

$this->title = 'Create Car Out';
$this->params['breadcrumbs'][] = ['label' => 'Car Outs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-out-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
