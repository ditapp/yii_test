<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Project;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายชื่อโครงการ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">
  <div class="table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มโครงการ', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'value' => function($model){
                  if($model->name != "" || $model->name != NULL) {
                    return $model->name;
                  }
                  else {
                    return "";
                  }
                },
                'filter' => Html::activeDropDownList(
                  $searchModel, 'id',
                  ArrayHelper::map(Project::find()->all(),'id','name'),
                  ['class'=>'form-control','prompt' => '']
                ),
                'headerOptions' => ['style'=>'width:20%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'description',
                'value' => function($model){
                  if($model->description != "" || $model->description != NULL) {
                    return $model->description;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:50%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'project_code',
                'value' => function($model){
                    return $model->project_code;
                },
                'headerOptions' => ['style'=>'width:10%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'parking_fee',
                'value' => function($model){
                    return $model->parking_fee;
                },
                'headerOptions' => ['style'=>'width:13%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
