<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="container">
      <div class ="row">
        <div class="col-sm-4">
          <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>
      </div>
  </div>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
