<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CarInSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-in-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?= $form->field($model, 'license_plate') ?>

    <?= $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'created_tm') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'mobile_phone') ?>

    <?php // echo $form->field($model, 'visit_to') ?>

    <?php // echo $form->field($model, 'car_in_image1') ?>

    <?php // echo $form->field($model, 'car_in_image2') ?>

    <?php // echo $form->field($model, 'card_image') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
