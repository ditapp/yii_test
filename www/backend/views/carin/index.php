<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CarInSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Car Ins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-in-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Car In', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',
            'license_plate',
            'address',
            //'created_tm',
            //'created_by',
            //'mobile_phone',
            //'visit_to',
            //'car_in_image1',
            //'car_in_image2',
            //'card_image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
