<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="/imgs/icon.png" sizes="16x16" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title='LET ME IN') ?></title>
    <?php $this->head() ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125657575-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-125657575-1');
    </script>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="/imgs/logo.png">',//logoบน navbar
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => ' navbar-inverse ',
        ],
    ]);

    //เมนูบน navbar
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'เข้าระบบ', 'url' => ['/site/login']];
    }
    else {
        //เมนูสำหรับผู้ดูแลระบบ
        $menuItems[] = ['label' => 'ประวัติการขอ QR', 'url' => ['/qrcode/index']];
        $menuItems[] = ['label' => 'ประวัติการใช้งาน QR', 'url' => ['/qrcodelog/index']];
        //เมนูสำหรับแอดมิน
        if(Yii::$app->user->identity->role == 1) {
          $menuItems[] = ['label' => 'ประวัติรถเข้า','url' => ['/car-in/index']];
          $menuItems[] = ['label' => 'ประวัติรถออก','url' => ['/car-out/index']];
          $menuItems[] = ['label' => 'ข้อมูลลูกบ้าน', 'url' => ['/owner/index']];
          $menuItems[] = [
            'label' => 'เพิ่มเติม',
            'url' => '#',
            'items' => [
    		 			['label' => 'โครงการ', 'url' => ['/project/index']],
              ['label' => 'ที่อยู่อาศัย','url' => ['/address/index']],
              ['label' => 'อุปกรณ์', 'url' => ['/device/index']],
              ['label' => 'ผู้ดูแลระบบ', 'url' => ['/user/index']],
              ['label' => 'ตั้งค่า', 'url' => ['/settings/index']],
    		 		],
          ];
        }

        $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Logout (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
      <?= Breadcrumbs::widget([
        'homeLink' => [
              'label' => Yii::t('yii', 'หน้าแรก'),
              'url' => Yii::$app->homeUrl,
          ],
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
      ]) ?>
      <?= Alert::widget() ?>
      <?= $content ?>
    </div>
</div>

<footer class="footer"></footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
