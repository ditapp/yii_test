<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CarOutSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-out-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parking_time') ?>

    <?= $form->field($model, 'created_by') ?>

    <?= $form->field($model, 'license_plate') ?>

    <?= $form->field($model, 'parking_time_cal') ?>

    <?php // echo $form->field($model, 'net_parking_fee') ?>

    <?php // echo $form->field($model, 'get_money') ?>

    <?php // echo $form->field($model, 'change') ?>

    <?php // echo $form->field($model, 'car_out_image1') ?>

    <?php // echo $form->field($model, 'car_out_image2') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
