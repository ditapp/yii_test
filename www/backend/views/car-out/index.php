<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CarOutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Car Outs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-out-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            [
              'attribute' => 'id',
              'value' => function($model){
                return $model->id;
              },
                'headerOptions' => ['style'=>'width:5%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'parking_time',
              'value' => function($model){
                return $model->parking_time;
              },
                'headerOptions' => ['style'=>'width:25%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'license_plate',
              'value' => function($model){
                return $model->license_plate;
              },
                'headerOptions' => ['style'=>'width:20%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'parking_time_cal',
              'value' => function($model){
                return $model->parking_time_cal;
              },
                'headerOptions' => ['style'=>'width:20%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'net_parking_fee',
              'value' => function($model){
                return $model->net_parking_fee;
              },
                'headerOptions' => ['style'=>'width:20%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            //'net_parking_fee',
            //'get_money',
            //'change',
            //'car_out_image1',
            //'car_out_image2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
