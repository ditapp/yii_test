<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\QrcodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประวัติการขอ QR Code';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qrcode-index">
  <div class="table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
              'attribute' => 'id',
              'value' => function($model){
                return $model->id;
              },
                'headerOptions' => ['style'=>'width:8%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'photo',
                'format' => 'raw',
                'value' => function ($data) {

                    if($data['visitor_img_url'] !="" || $data['visitor_img_url'] != null){
                        return Html::img($data['visitor_img_url'],
                        ['class' => "myImg_index",'width' => '80px','height' => '95px','onClick'=> 'showImg(this.src);']);
                    }
                    else if ($data['camera_img_url'] !="" || $data['camera_img_url'] != null){
                            return Html::img($data['camera_img_url'],
                            ['class' => "myImg_index",'width' => '80px','height' => '95px','onClick'=> 'showImg(this.src);']);
                    }
                    else {
                        return Html::img(Yii::$app->params['default_img'], //default
                        ['width' => '80px','height' => '95px']);
                    }
                },
            ],
            [
                'attribute' => 'channel',
                'filter' => array("Home Service" => "MobileApp" ,"Kiosk" => "Kiosk"),
                'format' => 'raw',
                'value' => function($model){
                  if($model->channel == "Home Service" ) {
                    return Html::img('/imgs/m.png');
                  }
                  elseif ($model->channel == "Kiosk" ){
                    return Html::img('/imgs/k.png');
                  }
                  // elseif ($model->channel == "Line@") {
                  //   return Html::img('/imgs/@.png');
                  // }
                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'fullname',
                'value' => function($model){
                  if($model->first_name && $model->last_name != ""|| $model->first_name && $model->last_name != NULL) {
                    return $model->fullname = $model->first_name." ".$model->last_name;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:15%','color:#337ab7'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'room_number',
                'value' => function($model){
                  if($model->room_number != "" || $model->room_number != NULL) {
                    return $model->room_number;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:10%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'created_tm',
                'value' => function($model){
                    return date('d-m-Y | H:i น.', strtotime($model->created_tm));
                  },
                'format' => 'raw',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'language' => 'th',
                    'attribute' => 'created_tm',
                    'clientOptions' => [
                      'keyboardNavigation' => false,
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                  ]),
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'visit_time',
                'value' => function($model){
                    return date('d-m-Y | H:i น.', strtotime($model->visit_time));
                  },
                'format' =>'raw',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'language' => 'th',
                    'attribute' => 'visit_time',
                    'clientOptions' => [
                        'keyboardNavigation' => false,
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                  ]),
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            //'effective_time:datetime',
            [
                'attribute' => 'options',
                'filter' => array("1" => "Lobby","2" => "Lobby,Lift"),
                'value' => function($model){
                  if($model->options == 1 ) {
                    return "Lobby";
                  }
                  elseif ($model->options == 2 ){
                    return "Lobby,Lift";
                  }
                  // elseif ($model->options == 3) {
                  //   return "All";
                  // }

                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'mobile',
                'value' => function($model){
                  if($model->mobile != "" || $model->mobile != NULL) {
                    return $model->mobile;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:10%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            //'qrcodekey',
            //'uuid',
            //'project_code',
            //language,

            [
              'class' => 'yii\grid\ActionColumn',
              'template'=>'{view}',
            ],
        ],
    ]); ?>
  </div>

  <div id="myModal_index" class="modal">

      <!-- The Close Button -->
      <span class="close">&times;</span>

      <!-- Modal Content (The Image) -->
      <img class="modal-content" id="img">

      <!-- Modal Caption (Image Text) -->
      <div id="caption"></div>
  </div>
</div>
