<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Qrcode */

$this->title = 'รายละเอียด QR code';
$this->params['breadcrumbs'][] = ['label' => 'Qrcodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qrcode-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          [
              'attribute' => 'visitor_img_url',
              'format' => 'raw',
              'value' => function ($data) {
                if($data['visitor_img_url'] !="" || $data['visitor_img_url'] != null){
                    // $img = $data['visitor_img_url'];
                    return Html::img($data['visitor_img_url'],
                    ['class' => "myImg",'width' => '160px','height' => '190px','onClick'=> 'showImg(this.src);']);
                }
                  else{
                    return Html::img(Yii::$app->params['default_img'], //default
                    ['width' => '160px','height' => '190px']);
                  }
                 },
          ],
          [
              'attribute' => 'camera_img_url',
              'format' => 'raw',
              'value' => function ($data) {
                if($data['camera_img_url'] !="" || $data['camera_img_url'] != null){
                    return Html::img($data['camera_img_url'],
                    ['class' => "myImg",'width' => '160px','height' => '190px','onClick'=> 'showImg(this.src);']);
                }
                  else{
                    return Html::img(Yii::$app->params['default_img'], //default
                    ['width' => '160px','height' => '190px']);
                  }
                 },
          ],
            'id',
            [
                'attribute' => 'room_number',
                'value' => function($model){
                  if($model->room_number != "" || $model->room_number != NULL) {
                    return $model->room_number;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'channel',
                'value' => function($model){
                  if($model->channel != "" || $model->channel != NULL) {
                    return $model->channel;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'fullname',
                'value' => function($model){
                  if($model->first_name != "" || $model->first_name != NULL) {
                    return $model->fullname = $model->first_name." ".$model->last_name;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'identity_id',
                'value' => function($model){
                  if($model->identity_id != "" || $model->identity_id != NULL) {
                    return $model->identity_id;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            'effective_time',
            [
                'attribute' => 'created_tm',
                'value' => function($model){
                  if($model->created_tm != "" || $model->created_tm != NULL) {
                    return $model->created_tm=Yii::$app->formatter->asDate($model->created_tm,"d-MM-Y | H:i น.");
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'visit_time',
                'value' => function($model){
                  if($model->visit_time != "" || $model->visit_time != NULL) {
                    return $model->visit_time=Yii::$app->formatter->asDate($model->visit_time,"d-MM-Y | H:i น.");
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'options',
                'value' => function($model){
                  if($model->options != "" || $model->options != NULL) {
                    return $model->options;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'mobile',
                'value' => function($model){
                  if($model->mobile != "" || $model->mobile != NULL) {
                    return $model->mobile;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            'qrcodestatus',
        ],
    ]) ?>

    <div class="table-responsive">
        <p><br></p>
        <h1>รายละเอียดการใช้งาน</h1>

        <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],

              [
                  'attribute' => 'channel',
                  'filter' => array("Home Service" => "MobileApp","Kiosk" => "Kiosk","Line@" => "Line@"),
                  'format' => 'raw',
                  'value' => function($model){
                    if($model->qrcodeinfo->channel == "Home Service" ) {
                      return Html::img('/imgs/m.png');
                    }
                    elseif ($model->qrcodeinfo->channel == "Kiosk" ){
                      return Html::img('/imgs/k.png');
                    }
                    elseif ($model->qrcodeinfo->channel == "Line@") {
                      return Html::img('/imgs/@.png');
                    }
                  },
                  'headerOptions' => ['style'=>'width:15%'],
                  'contentOptions' => ['style' => 'color:#4d4d4d']
              ],
              [
                  'attribute' => 'fullname',
                  'value' => function($model){
                    if($model->qrcodeinfo != "" || $model->qrcodeinfo != NULL) {
                      return $model->fullname = $model->qrcodeinfo->first_name." ".$model->qrcodeinfo->last_name;
                    }
                    else {
                      return "";
                    }
                  },
                  'headerOptions' => ['style'=>'width:17%'],
                  'contentOptions' => ['style' => 'color:#4d4d4d']
              ],
              [
                  'attribute' => 'room_number',
                  'value' => function($model){
                    if($model->qrcodeinfo != "" || $model->qrcodeinfo != NULL) {
                      return $model->qrcodeinfo->room_number ;
                    }
                    else {
                      return "";
                    }
                  },
                  'headerOptions' => ['style'=>'width:17%'],
                  'contentOptions' => ['style' => 'color:#4d4d4d']
              ],
              [
                  'attribute' => 'mobile',
                  'value' => function($model){
                    if($model->qrcodeinfo != "" || $model->qrcodeinfo != NULL) {
                      return $model->qrcodeinfo->mobile ;
                    }
                    else {
                      return "";
                    }
                  },
                  'headerOptions' => ['style'=>'width:17%'],
                  'contentOptions' => ['style' => 'color:#4d4d4d']
              ],
              [
                  'attribute' => 'device_id',
                  'value' => function($model){
                    if(($model->device_id != "" || $model->device_id != NULL) && $model->device) {
                      return $model->device->name;
                    }
                    else {
                      return "";
                    }
                  },
                  'headerOptions' => ['style'=>'width:17%'],
                  'contentOptions' => ['style' => 'color:#4d4d4d']
              ],
              [
                'attribute' => 'opentime',
                'value' => function($model){
                    return date('d-m-Y | H:i น.', strtotime($model->opentime));
                  },
                'format' => 'raw',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'language' => 'th',
                    'attribute' => 'opentime',
                    'clientOptions' => [
                        'keyboardNavigation' => false,
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                  ]),
                  'headerOptions' => ['style'=>'width:17%'],
                  'contentOptions' => ['style' => 'color:#4d4d4d']
              ],
          ],
      ]); ?>
  </div>
  <div id="myModal_index" class="modal">

      <!-- The Close Button -->
      <span class="close">&times;</span>

      <!-- Modal Content (The Image) -->
      <img class="modal-content" id="img">

      <!-- Modal Caption (Image Text) -->
      <div id="caption"></div>
  </div>
</div>
