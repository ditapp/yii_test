<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Project;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Address_Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ที่อยู่อาศัย';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-index">
  <div class="table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มที่อยูอาศัย', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
              'header' => 'ลำดับ',
              'class' => 'yii\grid\SerialColumn'
            ],
            [
              'attribute' => 'project_id',
              'value' => function($model) {

                return $model->project->name;
              },
              'filter' => Html::activeDropDownList(
                $searchModel, 'project_id',
                ArrayHelper::map(Project::find()->all(),'id','name'),
                ['class'=>'form-control','prompt' => '']
              ),
                'headerOptions' => ['style'=>'width:10%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],

            // 'project.name',
            [
              'attribute' => 'floor',
              'value' => function($model) {
                return $model->floor;
              },
                'headerOptions' => ['style'=>'width:10%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'unit_code',
              'value' => function($model) {
                return $model->unit_code;
              },
                'headerOptions' => ['style'=>'width:30%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'room_number',
              'value' => function($model) {
                return $model->room_number;
              },
                'headerOptions' => ['style'=>'width:30%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
  </div>
</div>
