<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Address */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
      <div class="col-sm-3">
        <?= $form->field($model, 'project_id')->textInput(['maxlength' => true]) ?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <?= $form->field($model, 'unit_code')->textInput(['maxlength' => true]) ?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <?= $form->field($model, 'room_number')->textInput(['maxlength' => true]) ?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <?= $form->field($model, 'floor')->textInput(['maxlength' => true]) ?>
      </div>
    </div>

    <div class="form-group">
          <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
