<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="settings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไขข้อมูล', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจที่จะลบข้อมูลนี้ใช่ไหม',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'key',
            'value',
            'active',
            [
                'attribute' => 'project_id',
                'value' => $model->project->name,
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'created_by',
                'value' => $model->usercreate->first_name." ".$model->usercreate->last_name,
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'updated_by',
                'value' => $model->userupdate->first_name." ".$model->userupdate->last_name,
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            'created_tm',
            'updated_tm',
        ],
    ]) ?>

</div>
