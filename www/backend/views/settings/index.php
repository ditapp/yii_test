<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Project;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการตั้งค่า';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มการตั้งค่า', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'value' => function($model){
                    return $model->name;
                },
                'headerOptions' => ['style'=>'width:35%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'value',
                'value' => function($model){
                    return $model->value;
                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'project_id',
                'value' => function($model){
                  if($model->project_id != "" || $model->project_id != NULL) {
                    return $model->project->name;
                  }
                  else {
                    return "";
                  }
                },
                'filter' => Html::activeDropDownList(
                  $searchModel, 'project_id',
                  ArrayHelper::map(Project::find()->all(),'id','name'),
                  ['class'=>'form-control','prompt' => '']
                ),
                'headerOptions' => ['style'=>'width:20%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'active',
                'value' => function($model){
                  if($model->active == 1) {
                    return "ใช้งาน";
                  }
                  else {
                    return "ไม่ใช้งาน";
                  }
                },
                'filter'=>array(1=>"ใช้งาน",0=>"ไม่ใช้งาน"),
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
