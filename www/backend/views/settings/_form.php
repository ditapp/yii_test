<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
//$this->context->action->id
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($this->context->action->id == 'create') {?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'style' =>'width:30%']) ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true,'style' =>'width:30%']) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true,'style' =>'width:30%']) ?>

    <?= $form->field($model, 'active')->dropDownList([1 => "ใช้งาน", 0 => "ไม่ใช้งาน"],['style'=>'width:30%']) ?>

    <?= $form->field($model, 'project_id')->dropDownList($list, ['prompt'=>'เลือกโครงการ...', 'style'=>'width:30%']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

  <?php } else if ($this->context->action->id == 'update') {?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'style' =>'width:30%']) ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true, 'readonly'=>true, 'style' =>'width:30%']) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true,'style' =>'width:30%']) ?>

    <?= $form->field($model, 'active')->dropDownList([1 => "ใช้งาน", 0 => "ไม่ใช้งาน"],['style'=>'width:30%']) ?>

    <?= $form->field($model, 'project_id')->dropDownList($model->project_name, ['maxlength' => true,'style' =>'width:30%'])  ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
        <?= Html::a('ยกเลิก', ['index'], ['class'=>'btn btn-primary']) ?>
    </div>


  <?php }?>

    <?php ActiveForm::end(); ?>

</div>
