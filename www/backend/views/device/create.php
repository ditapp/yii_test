<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Device */

$this->title = 'เพิ่มอุปกรณ์';
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลอุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'project_list' => $project_list
    ]) ?>

</div>
