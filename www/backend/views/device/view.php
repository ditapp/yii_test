<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Device */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลอุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไขข้อมูล', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            [
              'attribute' => 'project_id',
              'value' => function($model){
                if($model->project_id != "" || $model->project_id != NULL) {
                  return $model->project->name;
                }
                else {
                  return "";
                }
              },
            ],
            'serial_number',
            [
              'attribute' => 'created_by',
              'value' => function($model){
                if($model->created_by != "" || $model->created_by != NULL) {
                  return $model->usercreated->username;
                }
                else {
                  return "";
                }
              },
            ],
            [
              'attribute' => 'updated_by',
              'value' => function($model){
                if($model->updated_by != "" || $model->updated_by != NULL) {
                  return $model->userupdated->username;
                }
                else {
                  return "";
                }
              },
            ],
            [
            'attribute' => 'created_tm',
            'value' => function($model){
                return date('d-m-Y | H:i น.', strtotime($model->created_tm));
              }
            ],
            [
            'attribute' => 'updated_tm',
            'value' => function($model){
                return date('d-m-Y | H:i น.', strtotime($model->updated_tm));
              }
            ],
            // 'device_id',
        ],
    ]) ?>

</div>
