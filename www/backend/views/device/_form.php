<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Device */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-form">

    <?php $form = ActiveForm::begin([
            'enableAjaxValidation'=>true,
            'validationUrl'=>Url::toRoute('device/validation')
    ]); ?>

    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'style' =>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'serial_number')->textInput(['maxlength' => true, 'style' =>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'project_id')->dropDownList($project_list, ['prompt'=>'เลือกโครงการ...', 'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'version')->dropDownList([1 => 'Version1', 2 => 'Version2'], ['prompt'=>'เลือกเวอร์ชั่น...', 'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
      </div>

      <div class="form-group">
          <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
