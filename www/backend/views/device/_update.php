<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Device */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deviceupdate-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'serial_number')->textInput(['maxlength' => true]) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'project_id')->dropDownList($model->device_project, ['readonly'=>true,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'version')->dropDownList([$model->version], ['readonly'=>true,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
      </div>
    </div>

      <div class="form-group">
          <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
