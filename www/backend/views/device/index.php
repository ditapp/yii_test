<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Project;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\DeviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลอุปกรณ์';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-index">
  <div class="table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มอุปกรณ์', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
              'attribute' => 'name',
              'value' => 'name',
              'headerOptions' => ['style'=>'width:25%'],
              'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'project_id',
                'value' => function($model){
                  if($model->project_id != "" || $model->project_id != NULL) {
                    return $model->project->name;
                  }
                  else {
                    return "";
                  }
                },
                'filter' => Html::activeDropDownList(
                  $searchModel, 'project_id',
                  ArrayHelper::map(Project::find()->all(),'id','name'),
                  ['class'=>'form-control','prompt' => '']
                ),
                'headerOptions' => ['style'=>'width:25%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'description',
              'value' => 'description',
              'headerOptions' => ['style'=>'width:35%'],
              'contentOptions' => ['style' => 'color:#4d4d4d']
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
  </div>
</div>
