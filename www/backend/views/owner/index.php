<?php

use yii\helpers\Html;
use yii\grid\GridView;
// use yii2mod\editable\EditableColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลลูกบ้าน';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="owner-index">
  <div class="table-responsive">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('เพิ่มลูกบ้าน', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' =>$searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'first_name',
                'value' => function($model){
                  if($model->first_name != "" || $model->first_name != NULL) {
                    return $model->first_name;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'last_name',
                'value' => function($model){
                  if($model->last_name != "" || $model->last_name != NULL) {
                    return $model->last_name;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'identity_id',
                'value' => function($model){
                  if($model->identity_id != "" || $model->identity_id != NULL) {

                    return $model->identity_id;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'phone',
                'value' => function($model){
                  if($model->phone != "" || $model->phone != NULL) {
                    return $model->phone;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:12%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'email',
                'value' => function($model){
                  if($model->email != "" || $model->email != NULL) {
                    return $model->email;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'room_no',
                'value' => function($model){
                  if($model->room_no != "" || $model->room_no != NULL) {
                    return $model->room_no;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:10%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'line_id',
                'value' => function($model){
                  if($model->line_id != "" || $model->line_id != NULL) {
                    return "ยืนยันตัวแล้ว";
                  }
                  else {
                    return "ไม่ได้ยืนยันตัว";
                  }
                },
                'filter' => array(1=>"ยืนยันตัวแล้ว", 2=>"ไม่ได้ยืนยันตัว"),
                'headerOptions' => ['style'=>'width:12%'],
                'contentOptions' => function ($model) {
                    return ['style' => 'color:'.(($model->line_id != "" || $model->line_id != NULL) ? '#4d4d4d' : '#f26464')];
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
  </div>
</div>
