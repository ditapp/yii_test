<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Owner */

$this->title = 'เพิ่มลูกบ้าน';
$this->params['breadcrumbs'][] = ['label' => 'Owners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="owner-create">
  
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
