<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Owner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="owner-form">

    <?php $form = ActiveForm::begin([
            'enableAjaxValidation'=>true,
            'validationUrl'=>Url::toRoute('owner/validation')
    ]); ?>

    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'first_name')->textInput(['maxlength' => 50,'style'=>'width:100%']) ?>
        </div>
          <div class="col-sm-3">
          <?= $form->field($model, 'last_name')->textInput(['maxlength' => 50,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'identity_id')->textInput(['placeholder' => "เลขบัตรประชาชน 13 หลัก",'maxlength' => 13,'style'=>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'gender')->dropdownlist(['1'=>'ชาย']+['2'=>'หญิง'],['maxlength' => 50,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class ="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'date_of_birth')->widget(
              DatePicker::className(), [
                  // inline too, not bad
                   'inline' => false,
                   // modify template for custom rendering
                  'template' => '{addon}{input}',
                  'language' => 'th',
                  'clientOptions' => [
                      'autoclose' => true,
                      'format' => 'yyyy-mm-dd'
                  ]
          ]);?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'room_no')->textInput(['maxlength' => 10,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class ="row">
      </div>
      <div class ="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'phone')->textInput(['maxlength' => 10,'style'=>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'email')->textInput(['maxlength' => 50,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class ="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'address')->textarea(['rows'=>4,'style'=>'width:100%' ])?>
        </div>
      </div>

      <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
      </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
