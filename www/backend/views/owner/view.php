<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Owner */

$this->title = 'ข้อมูลลูกบ้าน : '.$model->first_name.'  '.$model->last_name;;
$this->params['breadcrumbs'][] = ['label' => 'Owners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="owner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไขข้อมูล', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'line_id',
                'value' => function($model){
                  if($model->line_id != "" || $model->line_id != NULL) {

                    return "ยืนยันตัวแล้ว";

                  }
                  else {

                    return "ไม่ได้ยืนยันตัว";

                  }
                },
            ],
            'first_name',
            'last_name',
            'gender',
            // 'date_of_birth:date,',
            [
                'attribute' => 'date_of_birth',
                'value' => function($model){
                  if($model->date_of_birth != "" || $model->date_of_birth != NULL) {
                    return $model->date_of_birth=Yii::$app->formatter->asDate($model->date_of_birth,"d-MM-Y");
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'identity_id',
                'value' => function($model){
                  if($model->identity_id != "" || $model->identity_id != NULL) {
                    return $model->identity_id;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'room_no',
                'value' => function($model){
                  if($model->room_no != "" || $model->room_no != NULL) {
                    return $model->room_no;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'phone',
                'value' => function($model){
                  if($model->phone != "" || $model->phone != NULL) {
                    return $model->phone;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'email',
                'value' => function($model){
                  if($model->email != "" || $model->email != NULL) {
                    return $model->email;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'address',
                'value' => function($model){
                  if($model->address != "" || $model->address != NULL) {
                    return $model->address;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
        ],
    ]) ?>

</div>
