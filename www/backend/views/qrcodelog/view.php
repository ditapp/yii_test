<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Qrcodelog */

$this->title = "QR Code ID : ".$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qrcodelogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qrcodelog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Html::a('ผู้ขอ QR code', ['linkinfo','qrcodekey' => $model->qrcodekey], ['class' => 'btn btn-primary']) ?></p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'qrcodekey',
                'value' => function($model){
                  if($model->qrcodekey != "" || $model->qrcodekey != NULL) {
                    return $model->qrcodekey;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'device_id',
                'value' => function($model){
                  if($model->device_id != "" || $model->device_id != NULL) {
                    return $model->device->name;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'opentime',
                'value' => function($model){
                  if($model->opentime != "" || $model->opentime != NULL) {
                    return $model->opentime=Yii::$app->formatter->asDate($model->opentime,"d-MM-Y | H:i น.");
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'created_tm',
                'value' => function($model){
                  if($model->created_tm != "" || $model->created_tm != NULL) {
                    return $model->created_tm=Yii::$app->formatter->asDate($model->created_tm,"d-MM-Y | H:i น.");
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
        ],
    ]) ?>

</div>
