<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\QrcodelogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประวัติการแสกน QR code';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qrcodelog-index">
  <div class="table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
              'attribute' => 'id',
              'value' => function($model){
                return $model->id;
              },
                'headerOptions' => ['style'=>'width:8%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'channel',
                'filter' => array("Home Service" => "MobileApp","Kiosk" => "Kiosk"),
                'format' => 'raw',
                'value' => function($model){
                  if($model->qrcodeinfo) {
                    if($model->qrcodeinfo->channel == "Home Service" ) {
                      return Html::img('/imgs/m.png');
                    }
                    elseif ($model->qrcodeinfo->channel == "Kiosk" ){
                      return Html::img('/imgs/k.png');
                    }
                    // elseif ($model->qrcodeinfo->channel == "Line@") {
                    //   return Html::img('/imgs/@.png');
                    // }
                  }
                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'fullname',
                'value' => function($model){
                  if($model->qrcodeinfo != "" || $model->qrcodeinfo != NULL) {
                    return $model->fullname = $model->qrcodeinfo->first_name." ".$model->qrcodeinfo->last_name;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:17%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'room_number',
                'value' => function($model){
                  if($model->qrcodeinfo != "" || $model->qrcodeinfo != NULL) {
                    return $model->qrcodeinfo->room_number ;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:17%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'mobile',
                'value' => function($model){
                  if($model->qrcodeinfo != "" || $model->qrcodeinfo != NULL) {
                    return $model->qrcodeinfo->mobile ;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:17%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            // [
            //     'attribute' => 'created_tm2',
            //     'value' => function($model){
            //         return date('d-m-Y | H:i น.', strtotime($model->qrcodeinfo->created_tm));
            //       },
            //     'format' => 'raw',
            //     'filter' => DatePicker::widget([
            //         'model' => $searchModel,
            //         'language' => 'th',
            //         'clientOptions' => [
            //             'keyboardNavigation' => false,
            //             'autoclose' => true,
            //             'format' => 'yyyy-mm-dd'
            //         ]
            //       ]),
            //     'headerOptions' => ['style'=>'width:10%'],
            //     'contentOptions' => ['style' => 'color:#4d4d4d']
            // ],
            [
                'attribute' => 'device_id',
                'value' => function($model){
                  if(($model->device_id != "" || $model->device_id != NULL) && $model->device) {
                    return $model->device->name;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:17%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'opentime',
              'value' => function($model){
                  return date('d-m-Y | H:i น.', strtotime($model->opentime));
                },
              'format' => 'raw',
              'filter' => DatePicker::widget([
                  'model' => $searchModel,
                  'language' => 'th',
                  'attribute' => 'opentime',
                  'clientOptions' => [
                      'keyboardNavigation' => false,
                      'autoclose' => true,
                      'format' => 'yyyy-mm-dd'
                  ]
                ]),
                'headerOptions' => ['style'=>'width:17%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
        ],
    ]); ?>
  </div>
</div>
