<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'แก้ไขข้อมูลผู้ดูแลระบบ : '.$model->first_name.'  '.$model->last_name;;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลผู้ดูแลระบบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไขข้อมูลผู้ดูแลระบบ';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>

</div>
