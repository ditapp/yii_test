<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
            'enableAjaxValidation'=>true,
            'validationUrl'=>Url::toRoute('user/validation')
    ]); ?>

    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'username')->textInput(['maxlength' => 50,'style' =>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'password_hash')->textInput(['maxlength' => 50,'style' =>'width:100%'])->passwordinput() ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'first_name')->textInput(['maxlength' => 50,'style'=>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'last_name')->textInput(['maxlength' => 50,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'identity_id')->textInput(['placeholder' => "เลขบัตรประชาชน 13 หลัก",'maxlength'=>13,'style'=>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'email')->textInput(['style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'gender')->dropdownlist(['1'=>'ชาย']+['2'=>'หญิง'],['style'=>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'phone')->textInput(['maxlength'=>10,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'property_project_id')->dropDownList($project_list, ['prompt'=>'เลือกโครงการ...', 'style'=>'width:100%'])  ?>
        </div>
      </div>

      <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
