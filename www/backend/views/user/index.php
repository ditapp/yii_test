<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Project;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ผู้ดูแลระบบ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มผู้ดูแลระบบ', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' =>$searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'username',
                'value' => function($model){
                  if($model->username != "" || $model->username != NULL) {
                    return $model->username;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'first_name',
                'value' => function($model){
                  if(($model->first_name != "" || $model->first_name != NULL) && ($model->last_name != "" || $model->last_name != NULL)) {
                    return $model->first_name." ".$model->last_name;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:20%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'phone',
                'value' => function($model){
                  if($model->phone != "" || $model->phone != NULL) {
                    return $model->phone;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'email',
                'value' => function($model){
                  if($model->email != "" || $model->email != NULL) {
                    return $model->email;
                  }
                  else {
                    return "";
                  }
                },
                'headerOptions' => ['style'=>'width:20%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'property_project_id',
                'value' => function($model){
                  if($model->property_project_id != "" || $model->property_project_id != NULL) {
                    return $model->project->name;
                  }
                  else {
                    return "";
                  }
                },
                'filter' => Html::activeDropDownList(
                  $searchModel, 'property_project_id',
                  ArrayHelper::map(Project::find()->all(),'id','name'),
                  ['class'=>'form-control','prompt' => '']
                ),
                'headerOptions' => ['style'=>'width:20%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                  if($model->status == 10 ) {
                    return "ทำงาน";
                  }
                  else {
                    return "ลาออก";
                  }
                },
                'headerOptions' => ['style'=>'width:12%'],
                'contentOptions' => function ($model) {
                    return ['style' => 'color:'.(($model->status == 10) ? '#4d4d4d' : '#f26464')];
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
