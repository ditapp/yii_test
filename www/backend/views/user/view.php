<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'ข้อมูลนิติบุคคล : '.$model->first_name.'  '.$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไขข้อมูล', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            [
                'attribute' => 'first_name',
                'value' => function($model){
                  if($model->first_name != "" || $model->first_name != NULL) {
                    return $model->first_name;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'last_name',
                'value' => function($model){
                  if($model->last_name != "" || $model->last_name != NULL) {
                    return $model->last_name;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'gender',
                'value' => function($model){
                  if($model->gender != "" || $model->gender != NULL) {
                    return $model->gender;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'phone',
                'value' => function($model){
                  if($model->phone != "" || $model->phone != NULL) {
                    return $model->phone;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'email',
                'value' => function($model){
                  if($model->email != "" || $model->email != NULL) {
                    return $model->email;
                  }
                  else {
                    return "";
                  }
                },
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                  if($model->status == "10" || $model->status != NULL) {

                    return "ทำงาน";

                  }
                  else {

                    return "ไม่ได้ทำงาน";

                  }
                },
                // 'contentOptions' => function ($model) {
                //     return ['style' => 'color:'.(($model->line_id != "" || $model->line_id != NULL) ? '#4d4d4d' : '#f26464')];
                // },
            ],
        ],
    ]) ?>

</div>
