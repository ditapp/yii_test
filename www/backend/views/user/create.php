<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'เพิ่มผู้ดูแลระบบ';
$this->params['breadcrumbs'][] = ['label' => 'ผู้ดูแลระบบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'project_list' => $project_list
    ]) ?>


</div>
