<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form ActiveForm */
?>
<div class="update">

    <?php $form = ActiveForm::begin(); ?>

    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'username')->textInput(['readonly'=>true,'maxlength' => 50,'style' =>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'property_project_id')->dropDownList($model->project_name, ['readonly'=>true, 'style'=>'width:100%'])  ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'first_name')->textInput(['maxlength' => 50,'style'=>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'last_name')->textInput(['maxlength' => 50,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'identity_id')->textInput(['readonly'=>true,'placeholder' => "เลขบัตรประชาชน 13 หลัก",'maxlength'=>13,'style'=>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'email')->textInput(['readonly'=>true,'style'=>'width:100%']) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <?= $form->field($model, 'gender')->dropdownlist([$model->gender],['readonly'=>true,'style'=>'width:100%']) ?>
        </div>
        <div class="col-sm-3">
          <?= $form->field($model, 'phone')->textInput(['maxlength'=>10,'style'=>'width:100%']) ?>
        </div>
      </div>

        <div class="form-group">
          <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
        </div>
      </div>
    <?php ActiveForm::end(); ?>

</div><!-- _update -->
