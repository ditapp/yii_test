<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CarIn */

$this->title = 'Create Car In';
$this->params['breadcrumbs'][] = ['label' => 'Car Ins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-in-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
