<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CarInSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประวัติรถเข้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-in-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
              'attribute' => 'id',
              'value' => function($model){
                return $model->id;
              },
                'headerOptions' => ['style'=>'width:5%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'first_name',
              'value' => function($model){
                return $model->first_name;
              },
                'headerOptions' => ['style'=>'width:25%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'last_name',
              'value' => function($model){
                return $model->last_name;
              },
                'headerOptions' => ['style'=>'width:25%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'license_plate',
              'value' => function($model){
                return $model->license_plate;
              },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            [
              'attribute' => 'visit_to',
              'value' => function($model){
                return $model->visit_to;
              },
                'headerOptions' => ['style'=>'width:15%'],
                'contentOptions' => ['style' => 'color:#4d4d4d']
            ],
            //'created_tm',
            //'created_by',
            //'mobile_phone',
            //'visit_to',
            //'identity_id',
            //'car_in_image1',
            //'car_in_image2',
            //'card_image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
