<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Qrcodelog;
use common\models\Project;

/**
 * QrcodelogSearch represents the model behind the search form of `common\models\Qrcodelog`.
 */
class QrcodelogSearch extends Qrcodelog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['qrcodekey', 'device_id', 'opentime', 'created_tm','channel'
            ,'fullname','first_name','last_name','room_number','mobile','qrcodeinfo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $project_model = new Project();

        if(Yii::$app->user->identity->role == 1) {

          $query = Qrcodelog::find();
          $query->where(['env' => Yii::$app->params['env']]);
          // $query->where(['env' => "Production"]);
          $query->joinWith(['device']);
          // $query->joinWith(['qrcodeinfo']);
        }
        else {

          $project_info = $project_model->getProjectinfoById(Yii::$app->user->identity->property_project_id);
          $query = Qrcodelog::find();
          $query->where(['env' => Yii::$app->params['env']]);
          $query->andwhere(['qrcode_log.project_code' => $project_info->project_code]);
          $query->joinWith(['device']);
          // $query->joinWith(['qrcodeinfo']);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => 'DESC']],
            'pagination' => [ 'pageSize' => 10 ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'opentime' => $this->opentime,
            // 'created_tm' => $this->created_tm,
        ]);

        $query->andFilterWhere(['like', 'qrcode_log.qrcodekey', $this->qrcodekey])
            // ->andFilterWhere(['like', 'device_id', $this->device_id]);
            ->andFilterWhere(['like', 'device.name', $this->device_id])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'concat(first_name, " " , last_name)', $this->fullname])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'room_number', $this->room_number])
            ->andFilterWhere(['like', 'opentime', $this->opentime]);

        return $dataProvider;
    }
}
