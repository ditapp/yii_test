<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CarOut;

/**
 * CarOutSearch represents the model behind the search form of `common\models\CarOut`.
 */
class CarOutSearch extends CarOut
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'parking_time_cal', 'net_parking_fee', 'get_money', 'change'], 'integer'],
            [['parking_time', 'license_plate', 'car_out_image1', 'car_out_image2'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarOut::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parking_time' => $this->parking_time,
            'created_by' => $this->created_by,
            'parking_time_cal' => $this->parking_time_cal,
            'net_parking_fee' => $this->net_parking_fee,
            'get_money' => $this->get_money,
            'change' => $this->change,
        ]);

        $query->andFilterWhere(['like', 'license_plate', $this->license_plate])
            ->andFilterWhere(['like', 'car_out_image1', $this->car_out_image1])
            ->andFilterWhere(['like', 'car_out_image2', $this->car_out_image2]);

        return $dataProvider;
    }
}
