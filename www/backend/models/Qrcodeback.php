<?php

namespace backend\models;

use Yii;
use common\models\Qrcode;
use common\models\Qrcodelog;
use common\models\Owner;



/**
 * This is the model class for table "qrcode".
 *
 * @property int $id
 * @property int $coowner_id
 * @property string $channel
 * @property string $first_name
 * @property string $last_name
 * @property string $identity_id
 * @property string $room_number
 * @property int $effective_time
 * @property string $visit_time
 * @property int $options
 * @property string $mobile
 * @property string $qrcodekey
 * @property string $uuid
 * @property string $project_code
 * @property string $created_tm
 * @property string $language
 */
class Qrcodeback extends Qrcode
{
  public $projectcheck;
  public $qrcodestatus;
  public $ownername;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'qrcode';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
          [['effective_time', 'options'], 'integer'],
          [['mobile', 'language'], 'string', 'max' => 10],
          [['visit_time','created_tm','opentime'], 'safe'],
          [['channel', 'first_name', 'last_name', 'qrcodekey', 'uuid', 'project_code','device_id','qrcodestatus'], 'string', 'max' => 255],
          [['identity_id', 'room_number'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ไอดี',
          'channel' => 'ช่องทาง',
          'first_name' => 'ชื่อ',
          'last_name' => 'นามสกุล',
          'identity_id' => 'เลขบัตรประชาชน',
          'room_number' => 'ห้อง',
          'effective_time' => 'จำนวนครั้งที่ใช้ได้',
          'visit_time' => 'เวลาเข้าใช้งาน',
          'options' => 'สิทธิการเข้าถึง',
          'mobile' => 'เบอร์โทร',
          'qrcodekey' => 'QRcodeKey',
          'uuid' => 'Uuid',
          'project_code' => 'ProjectCode',
          'created_tm' => 'เวลาที่ขอ QR',
          'language' => 'ภาษา',
          'qrcodestatus' => 'สถานะ QR code',
          'ownername' => 'เจ้าของห้อง',
          'fullname' => 'ชื่อ-นามสกุล',
          'visitor_img_url' => 'รูปถ่ายบัตรประชาขน',
          'camera_img_url' => 'รูปถ่ายผู้ขอ QR code',
        ];
    }

    public function getQrcodelog()
    {
        return $this->hasOne(Qrcodelog::className(),['qrcodekey' => 'qrcodekey']);
    }
}
