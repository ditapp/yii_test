<?php

namespace backend\models;

use Yii;
use common\models\Owner;

/**
 * This is the model class for table "owner".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $identity_id
 * @property string $line_id
 * @property string $room_no
 * @property int $gender
 * @property string $date_of_birth
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $created_tm
 * @property string $updated_tm
 * @property int $created_by
 * @property int $updated_by
 * @property int $property_project_id
 *
 * @property User $createdBy
 * @property PropertyProject $propertyProject
 * @property User $updatedBy
 */
class Ownerform extends Owner
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'owner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['first_name','last_name','gender','identity_id','date_of_birth','room_no','phone'],'required','message' => 'กรุณากรอกข้อมูล'],
            [['gender', 'created_by', 'updated_by', 'property_project_id'], 'integer'],
            [['date_of_birth', 'created_tm', 'updated_tm'], 'safe'],

            ['identity_id', 'number', 'min' => 13,'message'=>'กรุณากรอกตัวเลขเท่านั้น'],
            ['phone','number','min'=>10,'message'=>'กรุณากรอกตัวเลขเท่านั้น'],

            ['identity_id','string','min'=>13,'tooShort' =>'กรุณากรอกให้ครบ 13 หลัก'],
            ['phone','string','min'=>10,'tooShort' =>'กรุณากรอกให้ครบ 10 หลัก'],

            ['identity_id','unique','message'=>'เลขบัตรประชาชน ถูกใช้งานแล้ว'],

            ['email','email'],

            [['first_name', 'last_name', 'line_id', 'address'], 'string', 'max' => 255],

       ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ไอดี',
            'first_name' => 'ชื่อ',
            'last_name' => 'นามสกุล',
            'identity_id' => 'เลขบัตรประชาชน',
            //'line_id' => 'Line ID',
            'room_no' => 'ห้อง',
            'gender' => 'เพศ',
            'date_of_birth' => 'วันเกิด',
            'phone' => 'เบอร์โทรติดต่อ',
            'email' => 'อีเมล',
            'address' => 'ที่อยู่ปัจจุบัน',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyProject()
    {
        return $this->hasOne(PropertyProject::className(), ['id' => 'property_project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
