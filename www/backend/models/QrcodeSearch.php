<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Qrcode;
use common\models\Project;


/**
 * QrcodeSearch represents the model behind the search form of `common\models\Qrcode`.
 */
class QrcodeSearch extends Qrcode
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'coowner_id', 'effective_time', 'options', 'mobile'], 'integer'],
            [['channel', 'first_name', 'last_name', 'identity_id', 'room_number', 'visit_time', 'qrcodekey', 'uuid', 'project_code','created_tm','fullname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $project_model = new Project();

        if(Yii::$app->user->identity->role == 1) {

          $query = Qrcode::find();
        }
        else {

          $project_info = $project_model->getProjectinfoById(Yii::$app->user->identity->property_project_id);
          $query = Qrcode::find()->where(['project_code' => $project_info->project_code]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => 'DESC']],
            'pagination' => [ 'pageSize' => 10 ],


        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'coowner_id' => $this->coowner_id,
            'effective_time' => $this->effective_time,
            // 'visit_time' => $this->visit_time,
            'options' => $this->options,
            //'mobile' => $this->mobile,
        ]);

        // 'or',['like', 'first_name', $this->fullname],['like', 'last_name'

        $query->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'concat(first_name, " " , last_name)', $this->fullname])
            ->andFilterWhere(['like', 'identity_id', $this->identity_id])
            ->andFilterWhere(['like', 'room_number', $this->room_number])
            ->andFilterWhere(['like', 'qrcodekey', $this->qrcodekey])
            ->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'project_code', $this->project_code])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'created_tm', $this->created_tm])
            ->andFilterWhere(['like', 'visit_time', $this->visit_time]);



        return $dataProvider;
    }
}
