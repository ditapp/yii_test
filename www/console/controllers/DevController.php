<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\User;

class DevController extends Controller {
  
  public function actionAdmin()
	{
		$count = User::find()
			->where(['username' => 'admin'])
			->count();

		if($count >= 1)
		{
			echo "Admin is exists\n";
			return;
		}

		$user = new User([
			'username'	=> 'admin',
			'email'		=> 'admin@smart.local',
		]);
        
    $user->setPassword('admin123');
    $user->generateAuthKey();
		$user->save();

		echo "New user admin/admin123\n";
	}
}

?>
