<?php

use yii\db\Migration;

/**
 * Class m190905_113811_add_col_estimatetime_queue_table
 */
class m190905_113811_add_col_estimatetime_queue_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('queue', 'estimate_time', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('queue', 'estimate_time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190905_113811_add_col_estimatetime_queue_table cannot be reverted.\n";

        return false;
    }
    */
}
