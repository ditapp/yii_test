<?php

use yii\db\Migration;

/**
 * Handles the creation of table `property_project`.
 */
class m180802_094820_create_property_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('property_project', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'description' => $this->string(255),
            'creater_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'created_tm' => $this->datetime(),
            'updated_tm' => $this->datetime()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('property_project');
    }
}
