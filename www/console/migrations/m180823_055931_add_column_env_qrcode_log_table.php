<?php

use yii\db\Migration;

/**
 * Class m180823_055931_add_column_env_qrcode_log_table
 */
class m180823_055931_add_column_env_qrcode_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('qrcode_log', 'env', $this->string(30));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('qrcode_log', 'env');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180823_055931_add_column_env_qrcode_log_table cannot be reverted.\n";

        return false;
    }
    */
}
