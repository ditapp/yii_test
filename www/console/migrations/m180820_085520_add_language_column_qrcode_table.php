<?php

use yii\db\Migration;

/**
 * Class m180820_085520_add_language_column_qrcode_table
 */
class m180820_085520_add_language_column_qrcode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('qrcode', 'language', $this->string(10));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('qrcode', 'language');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180820_085520_add_language_column_qrcode_table cannot be reverted.\n";

        return false;
    }
    */
}
