<?php

use yii\db\Migration;

/**
 * Class m190618_063528_alter_value_in_settings_table
 */
class m190618_063528_alter_value_in_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->alterColumn('settings', 'value', $this->string(255));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190618_063528_alter_value_in_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190618_063528_alter_value_in_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
