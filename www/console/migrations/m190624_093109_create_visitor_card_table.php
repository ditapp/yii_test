<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%visitor_card}}`.
 */
class m190624_093109_create_visitor_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%visitor_card}}', [
            'id' => $this->primaryKey(),
            'card_no' => $this->integer(5),
            'qrcode_key' => $this->string(255),
            'project_id' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'gen_time' => $this->datetime(),
            'used_time' => $this->datetime(),
            'usage' =>  $this->integer(1)
        ]);

        $this->addForeignKey(
            'fk-visitor_card-project_id',
            'visitor_card',
            'project_id',
            'property_project',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-visitor_card-created_by',
            'visitor_card',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-visitor_card-updated_by',
            'visitor_card',
            'updated_by',
            'user',
            'id',
            'CASCADE'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%visitor_card}}');
    }
}
