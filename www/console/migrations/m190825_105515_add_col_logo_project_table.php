<?php

use yii\db\Migration;

/**
 * Class m190825_105515_add_col_logo_project_table
 */
class m190825_105515_add_col_logo_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property_project', 'logo_image', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('property_project', 'logo_image');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190825_105515_add_col_logo_project_table cannot be reverted.\n";

        return false;
    }
    */
}
