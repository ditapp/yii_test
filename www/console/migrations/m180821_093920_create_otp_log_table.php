<?php

use yii\db\Migration;

/**
 * Handles the creation of table `otp_log`.
 */
class m180821_093920_create_otp_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('otp_log', [
            'id' => $this->primaryKey(),
            'otp' => $this->integer(6),
            'created_tm' => $this->timestamp(),
            'otp_expire' => $this->timestamp(),
            'mobile' => $this->string(10),
            'request_id' => $this->integer(11)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('otp_log');
    }
}
