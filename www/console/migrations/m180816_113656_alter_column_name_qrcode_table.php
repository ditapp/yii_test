<?php

use yii\db\Migration;

/**
 * Class m180816_113656_alter_column_name_qrcode_table
 */
class m180816_113656_alter_column_name_qrcode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('qrcode', 'option', 'options');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//         echo "m180816_113656_alter_column_name_qrcode_table cannot be reverted.\n";

//         return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180816_113656_alter_column_name_qrcode_table cannot be reverted.\n";

        return false;
    }
    */
}
