<?php

use yii\db\Migration;

/**
 * Class m180827_110418_rename_column_qrcode_table
 */
class m180827_110418_rename_column_qrcode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('qrcode', 'visitor_img', 'visitor_img_url');
        $this->renameColumn('qrcode', 'camera_img', 'camera_img_url');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // echo "m180827_110418_rename_column_qrcode_table cannot be reverted.\n";
        //
        // return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180827_110418_rename_column_qrcode_table cannot be reverted.\n";

        return false;
    }
    */
}
