<?php

use yii\db\Migration;

/**
 * Class m180904_044832_add_column_options_group_device_table
 */
class m180904_044832_add_column_options_group_device_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('device', 'group', $this->integer(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('device', 'group');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180904_044832_add_column_options_group_device_table cannot be reverted.\n";

        return false;
    }
    */
}
