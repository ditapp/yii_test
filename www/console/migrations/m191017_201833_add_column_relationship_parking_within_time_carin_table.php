<?php

use yii\db\Migration;

/**
 * Class m191017_201833_add_column_relationship_parking_within_time_carin_table
 */
class m191017_201833_add_column_relationship_parking_within_time_carin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('car_in', 'relationship', $this->string(255)->notNull());
         $this->addColumn('car_in', 'parking_within_time', $this->string(255)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('car_in', 'relationship');
       $this->dropColumn('car_in', 'parking_within_time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191017_201833_add_column_relationship_parking_within_time_carin_table cannot be reverted.\n";

        return false;
    }
    */
}
