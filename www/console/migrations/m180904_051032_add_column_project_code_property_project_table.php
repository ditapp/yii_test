<?php

use yii\db\Migration;

/**
 * Class m180904_051032_add_column_project_code_property_project_table
 */
class m180904_051032_add_column_project_code_property_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->addColumn('property_project', 'project_code', $this->string(20));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropColumn('property_project', 'project_code');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180904_051032_add_column_project_code_property_project_table cannot be reverted.\n";

        return false;
    }
    */
}
