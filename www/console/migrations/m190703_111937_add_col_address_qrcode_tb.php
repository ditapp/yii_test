<?php

use yii\db\Migration;

/**
 * Class m190703_111937_add_col_address_qrcode_tb
 */
class m190703_111937_add_col_address_qrcode_tb extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('qrcode', 'address', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('qrcode', 'address');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190703_111937_add_col_address_qrcode_tb cannot be reverted.\n";

        return false;
    }
    */
}
