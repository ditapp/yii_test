<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `otp_log`.
 */
class m180822_061819_drop_otp_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('otp_log');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('otp_log', [
            'id' => $this->primaryKey(),
        ]);
    }
}
