<?php

use yii\db\Migration;

/**
 * Class m180820_085327_alter_changename_column_property_table
 */
class m180820_085327_alter_changename_column_property_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('property_project', 'creater_by', 'created_by');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//         echo "m180820_085327_alter_changename_column_property_table cannot be reverted.\n";

//         return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180820_085327_alter_changename_column_property_table cannot be reverted.\n";

        return false;
    }
    */
}
