<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%setting}}`.
 */
class m190613_062501_create_setting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'created_by' => $this->integer(11)->defaultValue(null),
            'updated_by' => $this->integer(11)->defaultValue(null),
            'updated_tm' => $this->timestamp()->defaultValue(null),
            'created_tm' => $this->timestamp()->defaultValue(null),
            'key' => $this->string(50),
            'value' => $this->string(50),
            'active' => $this->integer(1)->defaultValue(1),
            'project_id' => $this->integer(11)
        ]);

        $this->addForeignKey(
            'fk-settings-project_id',
            'settings',
            'project_id',
            'property_project',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
