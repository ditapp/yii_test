<?php

use yii\db\Migration;

/**
 * Class m190910_165729_add_col_project_id_lift_and_queue_tb
 */
class m190910_165729_add_col_project_id_lift_and_queue_tb extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->addColumn('queue', 'project_id', $this->integer(11));
      $this->addColumn('lift_status', 'project_id', $this->integer(11));

      $this->addForeignKey(
          'fk-queue-project_id',
          'queue',
          'project_id',
          'property_project',
          'id',
          'CASCADE'
      );

      $this->addForeignKey(
          'fk-lift_status-project_id',
          'lift_status',
          'project_id',
          'property_project',
          'id',
          'CASCADE'
      );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropColumn('queue', 'project_id');
      $this->dropColumn('lift_status', 'project_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190910_165729_add_col_project_id_lift_and_queue_tb cannot be reverted.\n";

        return false;
    }
    */
}
