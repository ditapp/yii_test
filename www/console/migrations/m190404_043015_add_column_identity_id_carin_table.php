<?php

use yii\db\Migration;

/**
 * Class m190404_043015_add_column_identity_id_carin_table
 */
class m190404_043015_add_column_identity_id_carin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_in', 'identity_id', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('car_in', 'identity_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190404_043015_add_column_identity_id_carin_table cannot be reverted.\n";

        return false;
    }
    */
}
