<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car_in`.
 */
class m190326_062742_create_car_in_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car_in', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'license_plate' => $this->string(50)->notNull(),
            'address' => $this->string(255)->defaultValue(null),
            'created_tm' => $this->timestamp(),
            'created_by' => $this->integer(2)->defaultValue(null),
            'mobile_phone' => $this->string(20)->defaultValue(null),
            'visit_to' => $this->string(50)->notNull(),
            'car_in_image1' => $this->string(50)->defaultValue(null),
            'car_in_image2' => $this->string(50)->defaultValue(null),
            'card_image' => $this->string(50)->defaultValue(null)

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('car_in');
    }
}
