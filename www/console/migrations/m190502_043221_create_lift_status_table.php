<?php

use yii\db\Migration;

/**
 * Handles the creation of table `life_status`.
 */
class m190502_043221_create_lift_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lift_status', [
            'id' => $this->primaryKey(),
            'lift_no' => $this->string(255),
            'lift_status' => $this->string(255)->defaultValue('MANUAL'),
            'card_no' => $this->string(20)->defaultValue(''),
            'arriving_time' => $this->string(255)->defaultValue('00'),
            'vehicle_status' => $this->string(255)->defaultValue('')
        ]);

        $this->insert('lift_status',
        [
          'lift_no' => 'LIFT 1'
        ]);

        $this->insert('lift_status',
        [
          'lift_no' => 'LIFT 2'
        ]);

        $this->insert('lift_status',
        [
          'lift_no' => 'LIFT 3'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lift_status');
    }
}
