<?php

use yii\db\Migration;

/**
 * Class m180802_094923_alter_column_user_table
 */
class m180802_094923_alter_column_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user', 'created_at');
        $this->dropColumn('user', 'updated_at');

        $this->addColumn('user', 'first_name', $this->string(255));
        $this->addColumn('user', 'last_name', $this->string(255));
        $this->addColumn('user', 'gender', $this->integer(1));
        $this->addColumn('user', 'identity_id', $this->string(255));
        $this->addColumn('user', 'phone', $this->string(10));
        $this->addColumn('user', 'created_tm', $this->timestamp());
        $this->addColumn('user', 'updated_tm', $this->timestamp());
        $this->addColumn('user', 'created_by', $this->integer(11));
        $this->addColumn('user', 'updated_by', $this->integer(11));
        $this->addColumn('user', 'property_project_id', $this->integer(11));

        $this->addForeignKey(
            'fk-user-property_project_id',
            'user',
            'property_project_id',
            'property_project',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {

        $this->addColumn('user', 'created_at', $this->timestamp());
        $this->addColumn('user', 'updated_at', $this->timestamp());
        $this->dropColumn('user', 'created_tm');
        $this->dropColumn('user', 'updated_tm');
        $this->dropColumn('user', 'created_by');
        $this->dropColumn('user', 'updated_by');
        $this->dropColumn('user', 'first_name');
        $this->dropColumn('user', 'last_name');
        $this->dropColumn('user', 'gender');
        $this->dropColumn('user', 'identity_id');
        $this->dropColumn('user', 'phone');
        $this->dropColumn('user', 'property_project_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180802_094923_alter_column_user_table cannot be reverted.\n";

        return false;
    }
    */
}
