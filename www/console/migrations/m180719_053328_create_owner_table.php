<?php

use yii\db\Migration;

/**
 * Handles the creation of table `owner`.
 */
class m180719_053328_create_owner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('owner', [
            'id' => $this->primaryKey(11),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'identity_id' => $this->string(20),
            'line_id' => $this->string(255),
            'room_no' => $this->string(20),
            'qrcode' => $this->string(255),
            'qrcode_expire' => $this->datetime(),
            'otp' => $this->integer(6),
            'otp_expire' => $this->datetime(),
            'validation' => $this->boolean()->defaultValue(false)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('owner');
    }
}
