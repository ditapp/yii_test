<?php

use yii\db\Migration;

/**
 * Handles the creation of table `qrcode`.
 */
class m180815_062800_create_qrcode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('qrcode', [
            'id' => $this->primaryKey(),
            'coowner_id' => $this->integer(11),
            'channel' => $this->string(255),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'identity_id' => $this->string(20),
            'room_number' => $this->string(20),
            'effective_time' => $this->integer(3),
            'visit_time' => $this->dateTime(),
            'option' => $this->integer(1),
            'mobile' => $this->integer(10),
            'qrcodekey' => $this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('qrcode');
    }
}
