<?php

use yii\db\Migration;

/**
 * Class m191017_221611_add_column_stamp_carin_table
 */
class m191017_221611_add_column_stamp_carin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_in', 'stamp_to_out', $this->integer(1)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191017_221611_add_column_stamp_carin_table cannot be reverted.\n";

        return false;
    }
    */
}
