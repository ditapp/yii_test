<?php

use yii\db\Migration;

/**
 * Handles the creation of table `options`.
 */
class m180820_090733_create_options_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('options', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)
        ]);
        
        $this->insert('options', ['name' => 'Lobby']);
        $this->insert('options', ['name' => 'Lobby and Lift']);
        $this->insert('options', ['name' => 'Lobby, Lift and Room']);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('options');
    }
}
