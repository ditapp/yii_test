<?php

use yii\db\Migration;

/**
 * Class m190326_063656_add_parking_fee_column_property_project_table
 */
class m190326_063656_add_parking_fee_column_property_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property_project', 'parking_fee', $this->integer(4));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('property_project', 'parking_fee');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190326_063656_add_parking_fee_column_property_project_table cannot be reverted.\n";

        return false;
    }
    */
}
