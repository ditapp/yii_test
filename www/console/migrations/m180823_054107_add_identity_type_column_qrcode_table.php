<?php

use yii\db\Migration;

/**
 * Class m180823_054107_add_identity_type_column_qrcode_table
 */
class m180823_054107_add_identity_type_column_qrcode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('qrcode', 'identity_type', $this->integer(1));
        $this->addColumn('qrcode', 'passport_no', $this->string(20));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('qrcode', 'identity_type');
        $this->dropColumn('qrcode', 'passport_no');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180823_054107_add_identity_type_column_qrcode_table cannot be reverted.\n";

        return false;
    }
    */
}
