<?php

use yii\db\Migration;

/**
 * Class m190605_041752_add_column_projectid_device_table
 */
class m190605_041752_add_column_projectid_device_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('device', 'project_id', $this->integer(11));

        $this->addForeignKey(
            'fk-device-project_id',
            'device',
            'project_id',
            'property_project',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('device', 'project_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190605_041752_add_column_projectid_device_table cannot be reverted.\n";

        return false;
    }
    */
}
