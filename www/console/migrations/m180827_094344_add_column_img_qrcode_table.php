<?php

use yii\db\Migration;

/**
 * Class m180827_094344_add_column_img_qrcode_table
 */
class m180827_094344_add_column_img_qrcode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('qrcode', 'visitor_img', $this->string(255));
        $this->addColumn('qrcode', 'camera_img', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('qrcode', 'visitor_img');
        $this->dropColumn('qrcode', 'camera_img');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180827_094344_add_column_img_qrcode_table cannot be reverted.\n";

        return false;
    }
    */
}
