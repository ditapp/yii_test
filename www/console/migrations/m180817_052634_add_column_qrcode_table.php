<?php

use yii\db\Migration;

/**
 * Class m180817_052634_add_column_qrcode_table
 */
class m180817_052634_add_column_qrcode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('qrcode', 'created_tm', $this->dateTime());
        $this->addColumn('qrcode_log', 'created_tm', $this->dateTime());
        $this->alterColumn('qrcode', 'mobile', $this->string(10));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('qrcode', 'created_tms');
        $this->dropColumn('qrcode_log', 'created_tms');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180817_052634_add_column_qrcode_table cannot be reverted.\n";

        return false;
    }
    */
}
