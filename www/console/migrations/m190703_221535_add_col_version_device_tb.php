<?php

use yii\db\Migration;

/**
 * Class m190703_221535_add_col_version_device_tb
 */
class m190703_221535_add_col_version_device_tb extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->addColumn('device', 'version', $this->integer(2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropColumn('device', 'version');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190703_221535_add_col_version_device_tb cannot be reverted.\n";

        return false;
    }
    */
}
