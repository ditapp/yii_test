<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hive_log`.
 */
class m181221_062106_create_hive_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('qrcode_member_log', [
            'id' => $this->primaryKey(),
            'qrcodekey' => $this->string(255),
            'device_id' => $this->string(255),
            'opentime' => $this->timestamp(),
            'created_tm' => $this->timestamp(),
            'channel' => $this->string(255),
            'access' => $this->boolean()->defaultValue(false),
            'email' => $this->string(255),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('hive_log');
    }
}
