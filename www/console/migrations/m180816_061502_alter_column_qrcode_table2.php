<?php

use yii\db\Migration;

/**
 * Class m180816_061502_alter_column_qrcode_table2
 */
class m180816_061502_alter_column_qrcode_table2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('qrcode', 'project_code', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('qrcode', 'project_code');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180816_061502_alter_column_qrcode_table2 cannot be reverted.\n";

        return false;
    }
    */
}
