<?php

use yii\db\Migration;

/**
 * Handles the creation of table `qrcode_log`.
 */
class m180815_073344_create_qrcode_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('qrcode_log', [
            'id' => $this->primaryKey(),
            'qrcodekey' => $this->string(255),
            'device_id' => $this->string(255),
            'opentime' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('qrcode_log');
    }
}
