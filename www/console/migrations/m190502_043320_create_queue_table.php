<?php

use yii\db\Migration;

/**
 * Handles the creation of table `queue`.
 */
class m190502_043320_create_queue_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('queue', [
            'id' => $this->primaryKey(),
            'queue_no' => $this->integer(2),
            'card_no' => $this->string(20)->defaultValue('')
        ]);

        $this->insert('queue', ['queue_no' => 1]);
        $this->insert('queue', ['queue_no' => 2]);
        $this->insert('queue', ['queue_no' => 3]);
        $this->insert('queue', ['queue_no' => 4]);
        $this->insert('queue', ['queue_no' => 5]);
        $this->insert('queue', ['queue_no' => 6]);
        $this->insert('queue', ['queue_no' => 7]);
        $this->insert('queue', ['queue_no' => 8]);
        $this->insert('queue', ['queue_no' => 9]);
        $this->insert('queue', ['queue_no' => 10]);
        $this->insert('queue', ['queue_no' => 11]);
        $this->insert('queue', ['queue_no' => 12]);
        $this->insert('queue', ['queue_no' => 13]);
        $this->insert('queue', ['queue_no' => 14]);
        $this->insert('queue', ['queue_no' => 15]);
        $this->insert('queue', ['queue_no' => 16]);
        $this->insert('queue', ['queue_no' => 17]);
        $this->insert('queue', ['queue_no' => 18]);
        $this->insert('queue', ['queue_no' => 19]);
        $this->insert('queue', ['queue_no' => 20]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('queue');
    }
}
