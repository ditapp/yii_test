<?php

use yii\db\Migration;

/**
 * Class m180830_070156_add_column_device_table
 */
class m180830_070156_add_column_device_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('device', 'sdk_key', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('device', 'sdk_key');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180830_070156_add_column_device_table cannot be reverted.\n";

        return false;
    }
    */
}
