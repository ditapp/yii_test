<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%iot_log}}`.
 */
class m190911_104503_create_iot_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%iot_log}}', [
            'id' => $this->primaryKey(),
            'locations' => $this->string(100),
            'registries' => $this->string(100),
            'devices' => $this->string(100),
            'project' => $this->string(100),
            'key_file' => $this->string(100),
            'project_id' => $this->integer(11),
            'description' => $this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%iot_log}}');
    }
}
