<?php

use yii\db\Migration;

/**
 * Class m180831_095913_add_column_user_table
 */
class m180831_095913_add_column_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'role', $this->integer(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'role');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180831_095913_add_column_user_table cannot be reverted.\n";

        return false;
    }
    */
}
