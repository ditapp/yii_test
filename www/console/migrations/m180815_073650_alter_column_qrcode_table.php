<?php

use yii\db\Migration;

/**
 * Class m180815_073650_alter_column_qrcode_table
 */
class m180815_073650_alter_column_qrcode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('qrcode', 'uuid', $this->string(255));
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('qrcode', 'uuid');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180815_073650_alter_column_qrcode_table cannot be reverted.\n";

        return false;
    }
    */
}
