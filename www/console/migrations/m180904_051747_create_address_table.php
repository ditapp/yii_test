<?php

use yii\db\Migration;

/**
 * Handles the creation of table `address`.
 */
class m180904_051747_create_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('address', [
            'id' => $this->primaryKey(),
            'unit_code' => $this->string(20),
            'project_id' => $this->string(20),
            'room_number' => $this->string(20),
            'floor' => $this->string(20),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('address');
    }
}
