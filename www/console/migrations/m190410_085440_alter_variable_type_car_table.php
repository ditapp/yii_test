<?php

use yii\db\Migration;

/**
 * Class m190410_085440_alter_variable_type_car_table
 */
class m190410_085440_alter_variable_type_car_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

      $this->dropColumn('car_in', 'car_in_image1');
      $this->dropColumn('car_in', 'car_in_image2');
      $this->dropColumn('car_in', 'card_image');

      $this->dropColumn('car_out', 'car_out_image1');
      $this->dropColumn('car_out', 'car_out_image2');

      $this->addColumn('car_in', 'car_in_image1', $this->string(255));
      $this->addColumn('car_in', 'car_in_image2', $this->string(255));
      $this->addColumn('car_in', 'card_image', $this->string(255));

      $this->addColumn('car_out', 'car_out_image1', $this->string(255));
      $this->addColumn('car_out', 'car_out_image2', $this->string(255));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

      $this->dropColumn('car_in', 'car_in_image1');
      $this->dropColumn('car_in', 'car_in_image2');
      $this->dropColumn('car_in', 'card_image');

      $this->dropColumn('car_out', 'car_out_image1');
      $this->dropColumn('car_out', 'car_out_image2');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_085440_alter_variable_type_car_table cannot be reverted.\n";

        return false;
    }
    */
}
