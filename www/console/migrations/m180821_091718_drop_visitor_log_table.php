<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `visitor_log`.
 */
class m180821_091718_drop_visitor_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('visitor_log');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('visitor_log', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'identity_id' => $this->string(255),
            'room_no' => $this->string(20),
            'visitor_phone' => $this->string(10),
            'visitor_img' => $this->string(255),
            'cheated_at' => $this->datetime(),
            'qrcode' => $this->string(255),
            'qrcode_expire' => $this->datetime()
        ]);
    }
}
