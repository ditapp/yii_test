<?php

use yii\db\Migration;

/**
 * Class m190625_172815_add_col_project_code_qrcodelog_table
 */
class m190625_172815_add_col_project_code_qrcodelog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('qrcode_log', 'project_code', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('qrcode_log', 'project_code');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190625_172815_add_col_project_code_qrcodelog_table cannot be reverted.\n";

        return false;
    }
    */
}
