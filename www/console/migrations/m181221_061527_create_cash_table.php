<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cash`.
 */
class m181221_061527_create_cash_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cash', [
            'id' => $this->primaryKey(),
            'created_tm' => $this->timestamp(),
            'amount' => $this->integer(6)->defaultValue(0),
            'door_id' => $this->integer(2),
            'email' => $this->string(255),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'login' => $this->boolean()->defaultValue(false)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cash');
    }
}
