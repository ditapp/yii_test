<?php

use yii\db\Migration;

/**
 * Class m180905_053258_alter_column_device_table
 */
class m180905_053258_alter_column_device_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('device', 'group', $this->string(5));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180905_053258_alter_column_device_table cannot be reverted.\n";

        return false;
    }
    */
}
