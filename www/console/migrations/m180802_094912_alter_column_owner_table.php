<?php

use yii\db\Migration;

/**
 * Class m180802_094912_alter_column_owner_table
 */
class m180802_094912_alter_column_owner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('owner', 'qrcode');
        $this->dropColumn('owner', 'qrcode_expire');
        $this->dropColumn('owner', 'otp');
        $this->dropColumn('owner', 'otp_expire');
        $this->dropColumn('owner', 'validation');

        $this->addColumn('owner', 'gender', $this->integer(1));
        $this->addColumn('owner', 'date_of_birth', $this->date());
        $this->addColumn('owner', 'phone', $this->string(10));
        $this->addColumn('owner', 'email', $this->string(255));
        $this->addColumn('owner', 'address', $this->string(255));
        $this->addColumn('owner', 'created_tm', $this->timestamp());
        $this->addColumn('owner', 'updated_tm', $this->timestamp());
        $this->addColumn('owner', 'created_by', $this->integer(11));
        $this->addColumn('owner', 'updated_by', $this->integer(11));
        $this->addColumn('owner', 'property_project_id', $this->integer(11));

        $this->addForeignKey(
            'fk-owner-property_project_id',
            'owner',
            'property_project_id',
            'property_project',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-owner-created_by',
            'owner',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-owner-updated_by',
            'owner',
            'updated_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'gender');
        $this->dropColumn('user', 'date_of_birth');
        $this->dropColumn('user', 'phone');
        $this->dropColumn('user', 'email');
        $this->dropColumn('user', 'address');
        $this->dropColumn('user', 'property_project_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180802_094912_alter_column_owner_table cannot be reverted.\n";

        return false;
    }
    */
}
