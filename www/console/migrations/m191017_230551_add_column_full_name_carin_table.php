<?php

use yii\db\Migration;

/**
 * Class m191017_230551_add_column_full_name_carin_table
 */
class m191017_230551_add_column_full_name_carin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_in', 'full_name', $this->string(255)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       /* echo "m191017_230551_add_column_full_name_carin_table cannot be reverted.\n";

        return false;*/
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191017_230551_add_column_full_name_carin_table cannot be reverted.\n";

        return false;
    }
    */
}
