<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car_out`.
 */
class m190326_062749_create_car_out_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car_out', [
            'id' => $this->primaryKey(),
            'parking_time' => $this->timestamp()->notNull(),
            'created_by' => $this->integer(2)->notNull(),
            'license_plate' => $this->string(50),
            'parking_time_cal' => $this->integer(5)->defaultValue(0),
            'net_parking_fee' => $this->integer(5)->defaultValue(0),
            'get_money' => $this->integer(5)->defaultValue(0),
            'change' => $this->integer(5)->defaultValue(0),
            'car_out_image1' => $this->string(50)->defaultValue(null),
            'car_out_image2' => $this->string(50)->defaultValue(null)

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('car_out');
    }
}
