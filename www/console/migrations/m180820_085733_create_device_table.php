<?php

use yii\db\Migration;

/**
 * Handles the creation of table `device`.
 */
class m180820_085733_create_device_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('device', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'description' => $this->text(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'created_tm' => $this->datetime(),
            'updated_tm' => $this->datetime(),
            'serial_number' => $this->string(100),
            'device_id' => $this->string(100)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('device');
    }
}
