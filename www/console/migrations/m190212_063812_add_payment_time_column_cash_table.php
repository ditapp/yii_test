<?php

use yii\db\Migration;

/**
 * Class m190212_063812_add_payment_time_column_cash_table
 */
class m190212_063812_add_payment_time_column_cash_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cash', 'payment_time', $this->timestamp());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('cash', 'payment_time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190212_063812_add_payment_time_column_cash_table cannot be reverted.\n";

        return false;
    }
    */
}
