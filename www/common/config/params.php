<?php

Yii::setAlias('logo_path', '../../frontend/web/imgs/');

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => '‪3600000',
    'linglingId' => '06e98322d7',
    'strKey' => '4C6A05EF',
    'token' => '1533192747475',
    'signature' => '681352d8-5460-481e-8f96-7bda626a0c17',
    'openToken' => 'F7B8BF0B61BA16F3A096A0D1F6CC6033',
    'strKeyV2' => '0CF5670E',
    'tokenV2' => '1566442491644',
    'signatureV2' => '5afcbf57-7cbd-481d-9372-bc4bff5f1128',
    'openTokenV2' => '54B7284610485D2F208B8EE72C6DDAC4',
    'accessApiUrl' => 'http://api.linglingkaimen.net:8889/',
    'QrcodeUrl' => 'http://api.linglingkaimen.net:8889/cgi-bin/qrcode/addVisitorQrCode/',
    'OpenDoor' => 'http://api.linglingkaimen.net:8889/cgi-bin/key/remoteOpenDoor/',
    'LiftQrCode' => 'http://api.linglingkaimen.net:8889/cgi-bin/qrcode/addLiftQrCode/',
    'addDevice' => 'http://api.linglingkaimen.net:8889/cgi-bin/device/addDevice/',
    'updateDevice' => 'http://api.linglingkaimen.net:8889/cgi-bin/device/updateDevice/',
    'makeSdkkey' => 'http://api.linglingkaimen.net:8889/cgi-bin/key/makeSdkKey/',
    'addCard' => 'http://api.linglingkaimen.net:8889/cgi-bin/openDoorCard/addOpenDoorCard/',
    'updateCard' => 'http://api.linglingkaimen.net:8889/cgi-bin/openDoorCard/updateOpenDoorCard/',
    'addCardforLift' => 'http://api.linglingkaimen.net:8889/cgi-bin/openDoorCard/addOrUpdateCardLiftInfo/',
    'addQrcode' => 'http://api.linglingkaimen.net:8889/cgi-bin/qrcode/addQrcodeCard/',
    'default_img' => 'https://api.letmein.asia/img/default.png',
    'url_shortlink' => 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyB5nl-a6pNKqVmlwMHj0cVaCf5QuA7gGaE',
    'noti_apikey' => 'xQD42ar34JaYmxFNSgHkua8AHsperdcE7bnwkasr',
    'noti_url' => 'https://549fk4iae1.execute-api.ap-southeast-1.amazonaws.com/v1',
    'sendlog_url' => 'http://10.130.68.247:3333/api/log/getlog'
];
