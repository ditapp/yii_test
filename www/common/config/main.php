<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'broadcastDriver' => [
            'class' => \common\helpers\socketio\drivers\RedisDriver::class,
            'hostname' => 'localhost',
            'port' => 6379,
        ],
        'broadcastEvents' => [
            'class' => \common\helpers\socketio\EventManager::class,
            // 'nsp' => 'octagon',
            // Namespaces with events folders
            'namespaces' => [
                'common\helpers\socketio',
            ]
        ],
        'log' => [
      		'traceLevel' => YII_DEBUG ? 3 : 0,
      		'targets' => [
      			[
      				'class' => 'yii\log\FileTarget',
      				'levels' => ['error', 'warning'],
      				'logFile' => '@app/runtime/logs/error.log',
      			],
      			[
      				'class' => 'yii\log\FileTarget',
      				'levels' => ['info'],
      				'logFile' => '@app/runtime/logs/info.log',
      			],
      		],
      	],
    ],
];
