<?php

namespace common\helpers;

use Endroid\QrCode\QrCode;
use Yii;
use common\models\Project;

class qrcode_gen {

  function renderQrcode($data, $project_id = null, $source = null, $timestamp = null) {

    $project_model = new Project();

    date_default_timezone_set("Asia/Bangkok");

    $qrCode = new QrCode();

    $foreground = Yii::$app->params['foreground'];
    $size_logo = 108;
    $logo_path = $source == "LINE@" ? Yii::getAlias('@logo_path').'/'.'letmein.png' : Yii::getAlias('@logo_path').'/'.'lineasok.png';

    if($project_id != null){

      $project_info = $project_model->find()->where(['id' => $project_id])->one();

      if($project_info) {
        $logo_path = Yii::getAlias('@logo_path').'/'.$project_info->logo_image;
      }
      else {
        $logo_path = Yii::getAlias('@logo_path').'/'.'letmein.png';
      }
    }

    $qrCode->setText($data)
    ->setSize(310)
    ->setPadding(10) //10
    ->setErrorCorrection(QrCode::LEVEL_HIGH)
    ->setForegroundColor($foreground)
    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
    // Path to your logo with transparency
    ->setLogo($logo_path)
    // Set the size of your logo, default is 48
    ->setLogoSize($size_logo)
    ->setImageType(QrCode::IMAGE_TYPE_PNG);

    if($source == "LINE@") {

      $dirname = date('Y-m-d');
      $filename = Yii::$app->params['qrcode_path'].$dirname."/";

      if (!file_exists($filename)) {

          $oldmask = umask(0);
          mkdir(Yii::$app->params['qrcode_path'].$dirname, 0777);
          umask($oldmask);
          $qrCode->save($filename.$source."_".$timestamp.'.png');
      }
      else {

          $qrCode->save($filename.$source."_".$timestamp.'.png');
      }
    }
    else {
      // Send output of the QRCode directly
      header('Content-Type: '.$qrCode->getContentType());
      $qrCode->render();
    }

  }

}
 ?>
