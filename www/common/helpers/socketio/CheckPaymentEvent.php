<?php

namespace common\helpers\socketio;

use common\helpers\socketio\events\EventInterface;
use common\helpers\socketio\events\EventPolicyInterface;
use common\helpers\socketio\events\EventSubInterface;
use common\helpers\socketio\Broadcast;
use Yii;
use api\controllers\OwnerController;

class CheckPaymentEvent implements EventInterface, EventPolicyInterface, EventSubInterface {

    /**
    * Changel name. For client side this is nsp.
    */
   public static function broadcastOn(): array
   {
       return ['letmein'];
   }

   /**
    * Event name
    */
   public static function name(): string
   {
       return 'checkpayment';
   }

   public function can($data): bool
   {
       // Check data from client
       return true;
   }

   /**
    * Emit client event
    * @param array $data
    * @return array
    */
   public function handle(array $data)
   {
      // Yii::info($data['door_id'], "DATA");
       $status = OwnerController::checkPayment($data['door_id']);

       $data = ['status' => $status];

       Broadcast::emit('resultpayment', $data);
   }
}

?>
