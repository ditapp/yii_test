<?php

namespace common\helpers\socketio;

use common\helpers\socketio\events\EventInterface;
use common\helpers\socketio\events\EventPubInterface;
use Yii;

class ResultLogoutEvent implements EventInterface, EventPubInterface{

    /**
     * Changel name. For client side this is nsp.
     */
    public static function broadcastOn(): array
    {
        return ['letmein'];
    }

    /**
     * Event name
     */
    public static function name(): string
    {
        return 'resultlogout';
    }

    /**
     * Emit client event
     * @param array $data
     * @return array
     */
    public function fire(array $data): array
    {
        return $data;
    }
}

?>
