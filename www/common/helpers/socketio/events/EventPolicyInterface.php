<?php

namespace common\helpers\socketio\events;

interface EventPolicyInterface
{
    public function can($data): bool;
}
