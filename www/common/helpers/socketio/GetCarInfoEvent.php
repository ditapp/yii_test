<?php

namespace common\helpers\socketio;

use common\helpers\socketio\events\EventInterface;
use common\helpers\socketio\events\EventPolicyInterface;
use common\helpers\socketio\events\EventSubInterface;
use common\helpers\socketio\Broadcast;
use Yii;
use api\controllers\CarController;

class GetCarInfoEvent implements EventInterface, EventPolicyInterface, EventSubInterface {

    /**
    * Changel name. For client side this is nsp.
    */
   public static function broadcastOn(): array
   {
       return ['letmein'];
   }

   /**
    * Event name
    */
   public static function name(): string
   {
       return 'getcarinfo';
   }

   public function can($data): bool
   {
       // Check data from client
       return true;
   }

   /**
    * Emit client event
    * @param array $data
    * @return array
    */
   public function handle(array $data)
   {
       // Yii::info($data['door_id'], "DATA");
       $info = [];
       $info = CarController::getCarinfo($data['license_plate'], $data['token']);
       // $data = ['status' => true];
       Broadcast::emit('resultcarinfo', $info);
   }
}

?>
