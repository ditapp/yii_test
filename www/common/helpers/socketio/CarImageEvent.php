<?php

namespace common\helpers\socketio;

use common\helpers\socketio\events\EventInterface;
use common\helpers\socketio\events\EventPubInterface;
use common\helpers\socketio\events\EventRoomInterface;
use Yii;

class CarImageEvent implements EventInterface, EventPubInterface, EventRoomInterface {

    /**
     * User id
     * @var int
     */
    protected $userId;

    /**
     * Changel name. For client side this is nsp.
     */
    public static function broadcastOn(): array
    {
        return ['letmein'];
    }

    /**
     * Event name
     */
    public static function name(): string
    {
        return 'carimage';
    }

    /**
     * Socket.io room
     * @return string
     */
    public function room(): string
    {
        return 'letmein_' . $this->userId;
    }

    /**
     * Emit client event
     * @param array $data
     * @return array
     */
    public function fire(array $data): array
    {
       $this->userId = $data['userId'];
       return $data;
    }
}

?>
