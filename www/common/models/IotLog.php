<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "iot_log".
 *
 * @property int $id
 * @property string $locations
 * @property string $registries
 * @property string $devices
 * @property string $project
 * @property string $key_file
 * @property int $project_id
 * @property string $description
 */
class IotLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'iot_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id'], 'integer'],
            [['locations', 'registries', 'devices', 'project', 'key_file'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'locations' => 'Locations',
            'registries' => 'Registries',
            'devices' => 'Devices',
            'project' => 'Project',
            'key_file' => 'Key File',
            'project_id' => 'Project ID',
            'description' => 'Description',
        ];
    }
}
