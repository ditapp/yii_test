<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $name
 * @property int $created_by
 * @property int $updated_by
 * @property string $updated_tm
 * @property string $created_tm
 * @property string $key
 * @property string $value
 * @property int $active
 * @property int $project_id
 *
 * @property PropertyProject $project
 */
class Settings extends \yii\db\ActiveRecord
{

  const ACTIVE_STATUS = 1;
  const INACTIVE_STATUS = 0;

  public $project_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_by', 'updated_by', 'active', 'project_id'], 'integer'],
            [['updated_tm', 'created_tm'], 'safe'],
            [['name', 'value'], 'string', 'max' => 255],
            ['key', 'string', 'max' => 50],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ชื่อ',
            'created_by' => 'ผู้ที่สร้าง',
            'updated_by' => 'ผู้ที่อัพเดท',
            'updated_tm' => 'เวลาอัพเดท',
            'created_tm' => 'เวลาสร้าง',
            'key' => 'คีย์',
            'value' => 'ค่า',
            'active' => 'สถานะ',
            'project_id' => 'โครงการ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public function getUsercreate()
    {
      return $this->hasOne(User::className(),['id' => 'created_by']);
    }

    public function getUserupdate()
    {
      return $this->hasOne(User::className(),['id' => 'updated_by']);
    }

    public function getValuebyKey($key, $project_id) {
      $info = static::findOne(['key' => $key, 'project_id' =>  $project_id, 'active' => self::ACTIVE_STATUS]);
      return $info->value;
    }
}
