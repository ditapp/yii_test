<?php

namespace common\models;

use Yii;
use \LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use \LINE\LINEBot\MessageBuilder\ImageMessageBuilder;
use \LINE\LINEBot\MessageBuilder\MultiMessageBuilder;
use \LINE\LINEBot\HTTPClient\CurlHTTPClient;
use \LINE\LINEBot;
use common\models\Device;

/**
 * This is the model class for table "owner".
 *
 * @property int $id
 * @property string $identity_id
 * @property int $otp
 * @property string $otp_expire
 * @property string $line_id
  * @property string $qrcode
  * @property string $qrcode_expire
  * @property string $first_name
  * @property string $last_name
 */
class Owner extends \yii\db\ActiveRecord
{

  public $floor;
  public $qrcode_time;
  public $name;
  public $otp;
  private $time_expire;
  private $effective_time;
  public $projectcheck;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'owner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gender','created_by','updated_by','property_project_id'], 'integer'],
            [['created_tm', 'updated_tm	','date_of_birth', 'first_name','last_name'], 'safe'],

            [['identity_id','room_no'], 'string', 'max' => 20],
            [['phone'],'string','max' =>10],
            [['line_id','email','address'], 'string', 'max' => 255],

            [['identity_id'], 'unique', 'targetAttribute' => ['identity_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ไอดี',
            'identity_id' => 'เลขบัตรประชาชน',
            'line_id' => 'การยืนยันตัวตน',
            'first_name' => 'ชื่อ',
            'last_name' => 'นามสกุล',
            'room_no' => 'ห้อง',
            'phone' => 'เบอร์โทรติดต่อ',
            'email' => 'อีเมล',
            'gender' => 'เพศ',
            'date_of_birth' => 'วัน/เดือน/ปี เกิด',
            'address' => 'ที่อยู่',
            'name' => 'ชื่อ-สกุล',
            'floor' => 'ชั้น'
        ];
    }

    public function getQrCodeKey($time_expire, $effective_time, $start_time, $sdknfloor) {

        $post_url = Yii::$app->params['LiftQrCode'].Yii::$app->params['openToken'];

        Yii::warning($sdknfloor, "SDK Key and Floor");

        $post_data = array(
                        'lingLingId' =>  Yii::$app->params['linglingId'],
                        'sdkKeys' =>  $sdknfloor['sdkKey'],
                        'startTime' => $start_time,
                        'endTime' => $time_expire,
                        'effecNumber' => $effective_time,
                        'autoFloorId' => 0,
                        'baseFloorId' => 1,
                        'userType' => 0,
                        'floorIds' => $sdknfloor['floor'],
                        'strKey' =>  Yii::$app->params['strKey']
                    );

        $post_data = $this->makeMessage($post_data);
        $post_data = array("MESSAGE" => json_encode($post_data));

        Yii::warning($post_url, "POST_URL");
        Yii::warning($post_data, "POST_DATA");

        $result = $this->curl($post_url, $post_data);
//         Yii::warning($result->responseResult->qrcodeKey, "result");
        return $result->responseResult;
    }

    public function manageDevice($post_url, $post_data = array()) {

        $post_data = $this->makeMessage($post_data);
        $post_data = array("MESSAGE" => json_encode($post_data));

        $result = $this->curl($post_url, $post_data);
        Yii::warning($result, "manage device");
        return $result->responseResult;
    }

    public function makeMessage($requestParam=array()) {

        $header = array('signature' => Yii::$app->params['signature'], 'token' => Yii::$app->params['token']);
        $message = array('requestParam' => $requestParam, 'header' => $header);
        return $message;
    }

    public function curl($url, $params=array())
    {

//         Yii::warning($params, "param");

        $headers = array();
        $headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "http.protocol.cookie-policy=compatibility");
        curl_setopt($ch, CURLOPT_URL, $url);
//         curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response  = curl_exec($ch);
//         $lastError = curl_error($ch);
//         $lastReq = curl_getinfo($ch);

        curl_close($ch);

//         Yii::warning($response, "curl_response");
//         Yii::warning($lastError, "lastError");
//         Yii::warning($lastReq, "lastReq");

        $responseData = json_decode($response);
        return $responseData;
    }

    public function replyMessage($line_id, $message, $image_url = null) {

        $channel_token = Yii::$app->params['ch_token'];

        $httpClient = new CurlHTTPClient($channel_token);
        $bot = new LINEBot($httpClient, ['channelSecret' => Yii::$app->params['ch_secret']]);

        $textMessageBuilder = new TextMessageBuilder($message);

        if($image_url != null) {

            $multipleMessageBuilder = new MultiMessageBuilder();
            $imageMessageBuilder = new ImageMessageBuilder($image_url, $image_url);

            $multipleMessageBuilder->add($imageMessageBuilder)->add($textMessageBuilder);

            $response = $bot->pushMessage($line_id, $multipleMessageBuilder);
        }
        else {

            $response = $bot->pushMessage($line_id, $textMessageBuilder);
        }

        if ($response->isSucceeded()) {
            return true;
        }

    }

    public function sendNotification($url, $params = array())
    {

        $headers = array(
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "x-api-key: ".Yii::$app->params['noti_apikey']
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_COOKIE, "http.protocol.cookie-policy=compatibility");
        curl_setopt($ch, CURLOPT_URL, $url);
//         curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response  = curl_exec($ch);

        curl_close($ch);

        // Yii::warning(json_encode($params), "params noti");
        // Yii::warning($response, "noti_response");

        $responseData = json_decode($response);
        return $responseData;
    }

    public function sendLog($data = array()) {

      $url = Yii::$app->params['sendlog_url'];
      // $url = "http://192.168.100.10:3333/api/log/getlog";
      $headers = array();
      $headers[] = 'Content-type: application/json;charset=UTF-8';

      $payload = json_encode($data);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_URL, $url);
//         curl_setopt($ch, CURLOPT_HEADER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $response  = curl_exec($ch);

      curl_close($ch);

        Yii::warning($response, "curl_response");
//         Yii::warning($lastError, "lastError");
//         Yii::warning($lastReq, "lastReq");

      $responseData = json_decode($response);
      return $responseData;

    }
}
