<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "car_out".
 *
 * @property int $id
 * @property string $parking_time
 * @property int $created_by
 * @property string $license_plate
 * @property int $parking_time_cal
 * @property int $net_parking_fee
 * @property int $get_money
 * @property int $change
 * @property string $car_out_image1
 * @property string $car_out_image2
 */
class CarOut extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_out';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parking_time'], 'safe'],
            [['created_by'], 'required'],
            [['created_by', 'parking_time_cal', 'net_parking_fee', 'get_money', 'change'], 'integer'],
            [['license_plate'], 'string', 'max' => 50],
            [['car_out_image1', 'car_out_image2'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ลำดับ',
            'parking_time' => 'เวลาเข้าจอด',
            'created_by' => 'ผู้สร้าง',
            'license_plate' => 'ทะเบียนรถ',
            'parking_time_cal' => 'เวลาที่จอด (ชั่วโมง)',
            'net_parking_fee' => 'ค่าจอดรถสุทธิ',
            'get_money' => 'เงินที่รับมา',
            'change' => 'เงินทอน',
            'car_out_image1' => 'รูปรถ รูปที่1',
            'car_out_image2' => 'รูปรถ รูปที่2',
        ];
    }
}
