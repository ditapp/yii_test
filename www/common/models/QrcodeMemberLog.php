<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "qrcode_member_log".
 *
 * @property int $id
 * @property string $qrcodekey
 * @property string $device_id
 * @property string $opentime
 * @property string $created_tm
 * @property string $channel
 * @property int $access
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 */
class QrcodeMemberLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'qrcode_member_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['opentime', 'created_tm'], 'safe'],
            [['access'], 'integer'],
            [['qrcodekey', 'device_id', 'channel', 'email', 'first_name', 'last_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'qrcodekey' => 'Qrcodekey',
            'device_id' => 'Device ID',
            'opentime' => 'Opentime',
            'created_tm' => 'Created Tm',
            'channel' => 'Channel',
            'access' => 'Access',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
        ];
    }
}
