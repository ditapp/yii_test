<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "qrcode_log".
 *
 * @property int $id
 * @property string $qrcodekey
 * @property string $device_id
 * @property string $opentime
 */
class Qrcodelog extends \yii\db\ActiveRecord
{
  public $fullname;
  public $channel;
  public $room_number;
  public $mobile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'qrcode_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['opentime', 'created_tm',], 'safe'],
            [['qrcodekey', 'device_id','channel','fullname','room_number','mobile'], 'string', 'max' => 255],
            ['env', 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ลำดับ',
            'qrcodekey' => 'QRcode Key',
            'device_id' => 'ชื่ออุปกรณ์',
            'opentime' => 'เวลาที่ใช้งาน',
            'created_tm' => 'วันที่สร้าง',
            'fullname' => 'ชื่อ-นามสกุล',
            'channel' => 'ช่องทาง',
            'room_number'=> 'ห้อง',
            'mobile' => 'เบอร์โทร',
        ];
    }

    public function getQrcodeinfo()
    {
      return $this->hasOne(Qrcode::className(),['qrcodekey' => 'qrcodekey']);
    }

    public function getDevice()
    {
      return $this->hasOne(Device::className(),['id' => 'device_id']);
    }
}
