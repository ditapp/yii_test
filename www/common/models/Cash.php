<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cash".
 *
 * @property int $id
 * @property string $created_tm
 * @property int $amount
 * @property int $door_id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property int $login
 */
class Cash extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_tm'], 'safe'],
            [['amount', 'door_id', 'login'], 'integer'],
            [['email', 'first_name', 'last_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_tm' => 'Created Tm',
            'amount' => 'Amount',
            'door_id' => 'Door ID',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'login' => 'Login',
        ];
    }
}
