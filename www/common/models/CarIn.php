<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "car_in".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $license_plate
 * @property string $address
 * @property string $created_tm
 * @property int $created_by
 * @property string $mobile_phone
 * @property string $visit_to
 * @property string $identity_id
 * @property string $car_in_image1
 * @property string $car_in_image2
 * @property string $card_image
 * @property string $relationship
 * @property string $parking_within_time
 * @property int $stamp_to_out
 * @property string $full_name
 */
class CarIn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_in';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['last_name', 'license_plate', 'visit_to', 'relationship', 'parking_within_time', 'stamp_to_out', 'full_name'], 'required'],
            [['created_tm'], 'safe'],
            [['created_by', 'stamp_to_out'], 'integer'],
            [['first_name', 'last_name', 'address', 'identity_id', 'car_in_image1', 'car_in_image2', 'card_image', 'relationship', 'parking_within_time', 'full_name'], 'string', 'max' => 255],
            [['license_plate', 'visit_to'], 'string', 'max' => 50],
            [['mobile_phone'], 'string', 'max' => 20],
            [['stamp_to_out'], 'integer' , 'max' => 1],
            [['first_name'],'required','message' => 'กรุณากรอกชื่อ'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ลำดับ',
            'first_name' => 'ชื่อ',
            'last_name' => 'นามสกุล',
            'license_plate' => 'ทะเบียนรถ',
            'address' => 'ที่อยู่',
            'created_tm' => 'วันที่/เวลา',
            'created_by' => 'ผู้สร้าง',
            'mobile_phone' => 'เบอร์ติดต่อ',
            'visit_to' => 'เลขที่บ้านที่มาติดต่อ',
            'identity_id' => 'เลขบัตรประชาชน',
            'car_in_image1' => 'รูปรถ รูปที่1',
            'car_in_image2' => 'รูปรถ รูปที่2',
            'card_image' => 'รูปบัตรประชาชน',
            'relationship' => 'ความสัมพันธ์',
            'parking_within_time' => 'ชั่วโมงในการจอดรถ',
            'full_name' => 'ชื่อ-นามสกุล',
            'stamp_to_out' => 'แสตมป์'
        ];
    }
}
