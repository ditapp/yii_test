<?php

namespace common\models;

use Yii;
use common\models\Address;
use common\models\Owner;

/**
 * This is the model class for table "device".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $created_by
 * @property int $updated_by
 * @property string $created_tm
 * @property string $updated_tm
 * @property string $serial_number
 * @property string $device_id
 */
class Device extends \yii\db\ActiveRecord
{

  public $device_project;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'device_id', 'sdk_key'], 'string'],
            [['name', 'serial_number'], 'string', 'max' => 100],
            [['created_by', 'updated_by', 'version', 'project_id'], 'integer'],
            [['created_tm', 'updated_tm'], 'safe'],
            ['serial_number','unique','message'=>'มี Serial Number นี้ในระบบ'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ไอดี',
            'name' => 'ชื่ออุปกรณ์',
            'description' => 'คำอธิบาย',
            'created_by' => 'ผู้สร้าง',
            'updated_by' => 'ผู้แก้ไข',
            'created_tm' => 'วันที่สร้าง',
            'updated_tm' => 'วันที่แก้ไข',
            'serial_number' => 'Serial Number',
            'device_id' => 'ไอดีอุปกรณ์',
            'project_id' => 'โครงการ',
            'version' => 'เวอร์ชั่นอุปกรณ์'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public function getUsercreated()
    {
      return $this->hasOne(User::className(),['id' => 'created_by']);
    }

    public function getUserupdated()
    {
      return $this->hasOne(User::className(),['id' => 'updated_by']);
    }

    public static function getInfoByDeviceId($device_id) {

        $value = static::findOne(['device_id' => $device_id]);

        if($value) {
          return $value;
        }
        else {
          return [];
        }
    }

    public static function getSdkKeyByDeviceid($device_id) {

        $value_add = static::findOne(['device_id' => $device_id]);

        if($value_add) {
          return $value_add->sdk_key;
        }
        else {
          return 1;
        }
    }

    public static function getSdkKeyGroup($option, $room_no, $channel) {

      if($channel == "Kiosk"){

        $address_info = Address::findOne(['room_number' => $room_no]);
      }
      else {

        $address_info = Address::findOne(['unit_code' => $room_no]);
      }

      $result = [];

      if(!empty($address_info)) {

        Yii::warning($address_info->project_id, "Project ID");

        $sdkKey = array();

        if($address_info->building != NULL) {

          if($option == 1) {

            if($address_info->building == "S") {
              $group = ["A", "B"];
            }
            else {
              $group = $address_info->building;
            }
          }
          else if($option == 2) {

            if($address_info->building == "S") {
              $group = ["A", "LA", "B", "LB"];
            }
            else {
              if($address_info->building == "A") {
                $group = ["A", "LA"];
              }
              else if($address_info->building == "B"){
                $group = ["B", "LB"];
              }
            }
          }
        }
        else {

          if($option == 1) {

            $group = "1";
          }
          else if($option == 2 || $option == 3) {

            $group = ["1", "2"];

            if($address_info->floor == "2" ) {
              array_push($group, "02");
            }
            else if($address_info->floor == "24" ) {
              array_push($group, "24");
            }
            else if($address_info->floor == "25" ) {
              array_push($group, "25");
            }
            else if($address_info->floor == "38" ) {
              array_push($group, "38");
            }
          }

        }

        $device_val = static::find()->where(['project_id' => $address_info->project_id, 'group' => $group])->all();

        foreach ($device_val as $device_info) {
          array_push($sdkKey, $device_info->sdk_key);
        }

        $floor_arr = [(int)$address_info->floor];

        $result = [
          "sdkKey" => $sdkKey,
          "floor" => $floor_arr
        ];
      }

      // Yii::warning($result, "Result");
      return $result;
    }

    public static function getDeviceIdGroup($option, $room_no, $channel) {

      if($channel == "Kiosk"){

        $address_info = Address::findOne(['room_number' => $room_no]);
      }
      else {

        $address_info = Address::findOne(['unit_code' => $room_no]);
      }

      $result = [];

      if(!empty($address_info)) {

        Yii::warning($address_info->project_id, "Project ID");

        $deviceId = array();

        if($address_info->building != NULL) {

          $group = $address_info->building;

        }
        else {

          if($option == 1) {

            $group = "1";
          }
          else if($option == 2 || $option == 3) {

            $group = ["1", "2"];

            if($address_info->floor == "2" ) {
              array_push($group, "02");
            }
            else if($address_info->floor == "24" ) {
              array_push($group, "24");
            }
            else if($address_info->floor == "25" ) {
              array_push($group, "25");
            }
            else if($address_info->floor == "38" ) {
              array_push($group, "38");
            }
          }

        }

        $device_val = static::find()->where(['project_id' => $address_info->project_id, 'group' => $group])->all();

        foreach ($device_val as $device_info) {
          array_push($deviceId, $device_info->device_id);
        }

        $floor_arr = [(int)$address_info->floor];

        $result = [
          "deviceId" => $deviceId,
          "floor" => $floor_arr
        ];
      }

      // Yii::warning($result, "Result");
      return $result;
    }

    public function manageDevice($post_url, $post_data = array(), $version) {

        $owner_model = new Owner();

        if($version == 1) {
          $token = Yii::$app->params['token'];
          $signature = Yii::$app->params['signature'];
        }
        else {
          $token = Yii::$app->params['tokenV2'];
          $signature = Yii::$app->params['signatureV2'];
        }

        $post_data = $this->makeMessage($post_data, $token, $signature);
        $post_data = array("MESSAGE" => json_encode($post_data));

        $result = $owner_model->curl($post_url, $post_data);
        Yii::warning($result, "manage device");
        return $result;
    }

    public function makeMessage($requestParam=array(), $token, $signature) {

        $header = array('signature' => $signature, 'token' => $token);
        $message = array('requestParam' => $requestParam, 'header' => $header);
        return $message;
    }
}
