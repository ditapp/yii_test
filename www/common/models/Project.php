<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "property_project".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $creater_by
 * @property int $updated_by
 * @property string $created_tm
 * @property string $updated_tm
 *
 * @property Owner[] $owners
 * @property User[] $users
 */
class Project extends \yii\db\ActiveRecord
{
  public $projectcheck;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_by', 'updated_by'], 'integer'],
            ['logo_image', 'string', 'max' => 100],
            [['created_tm', 'updated_tm'], 'safe'],
            [['name', 'description','users','owners'], 'string', 'max' => 255],

            [['name'],'required','message' => 'กรุณากรอกชื่อโครงการ']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ไอดี',
            'name' => 'ชื่อโครงการ',
            'description' => 'คำอธิบาย',
            'created_by' => 'ผู้สร้าง',
            'updated_by' => 'ผู้แก้ไข',
            'created_tm' => 'วันที่สร้าง',
            'updated_tm' => 'วันที่แก้ไข',
            'project_code' => 'รหัสโครงการ',
            'parking_fee' => 'ค่าจอดรถ/ชั่วโมง',
            'logo_image' => 'QR Code Logo'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwners()
    {
        return $this->hasMany(Owner::className(), ['property_project_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['property_project_id' => 'id']);
    }

    public function getUsercreate()
    {
      return $this->hasOne(User::className(),['id' => 'created_by']);
    }

    public function getUserupdate()
    {
      return $this->hasOne(User::className(),['id' => 'updated_by']);
    }

    public function getAll()
    {
      return $this->find()->all();
    }

    public function getProjectinfoById($id)
    {
      return $this->find()->where(['id' => $id])->one();
    }
}
