<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "queue".
 *
 * @property int $id
 * @property int $queue_no
 * @property string $card_no
 */
class Queue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['queue_no'], 'integer'],
            [['card_no'], 'string', 'max' => 20],
            ['estimate_time', 'string', 'max' => 100]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'queue_no' => 'Queue No',
            'card_no' => 'Card No',
            'estimate_time' => 'Estimate Time'
        ];
    }
}
