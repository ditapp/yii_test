<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property string $unit_code
 * @property string $project_id
 * @property string $room_number
 * @property string $floor
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit_code', 'project_id', 'room_number', 'floor'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unit_code' => 'ยูนิตโค้ด',
            'project_id' => 'โครงการ',
            'room_number' => 'เลขห้อง',
            'floor' => 'ชั้น',
        ];
    }

    public static function getFloorByUnitcode($unit_code) {

        $value_add = static::findOne(['unit_code' => $unit_code]);

        if($value_add) {
          return $value_add->floor;
        }
        else {
          return "1";
        }

    }

    public static function getUnitcodeByRoom($room_number) {

        $value_add = static::findOne(['room_number' => $room_number]);

        if($value_add) {
          return $value_add->unit_code;
        }
        else {
          return "";
        }
    }

    public function getProject() {
      return $this->hasOne(Project::className(),['id' => 'project_id']);
    }

    public function getProjectbyCode() {
      return $this->hasOne(Project::className(),['project_code' => 'project_id']);
    }
}
