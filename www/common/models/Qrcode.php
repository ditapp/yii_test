<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use common\models\Device;

/**
 * This is the model class for table "qrcode".
 *
 * @property int $id
 * @property int $coowner_id
 * @property string $channel
 * @property string $first_name
 * @property string $last_name
 * @property string $identity_id
 * @property string $room_number
 * @property int $effective_time
 * @property string $visit_time
 * @property int $options
 * @property int $mobile
 * @property string $qrcodekey
 * @property string $uuid
 * @property string $project_code
 * @property string $created_tm
 */
class Qrcode extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName() {

        return 'qrcode';
    }

    public $fullname;
    public $visitor_img;
    public $camera_img;
    public $photo;

    /**
     * {@inheritdoc}
     */
    public function rules() {

        return [
            [['coowner_id', 'effective_time', 'options', 'identity_type'], 'integer'],
            [['mobile', 'language'], 'string', 'max' => 10],
            [['visitor_img', 'camera_img'], 'file'],
            [['visit_time', 'created_tm', 'opentime'], 'safe'],
            [['channel', 'first_name', 'last_name', 'qrcodekey', 'uuid', 'fullname', 'visitor_img_url',
            'camera_img_url','photo'], 'string', 'max' => 255],
            [['identity_id', 'room_number', 'passport_no'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {

        return [
            'id' => 'ลำดับ',
            'coowner_id' => 'เจ้าของห้อง',
            'channel' => 'ช่องทาง',
            'first_name' => 'ชื่อ',
            'last_name' => 'นามสกุล',
            'identity_id' => 'เลขบัตรประชาชน',
            'room_number' => 'ห้อง',
            'effective_time' => 'จำนวนครั้งที่ใช้ได้',
            'visit_time' => 'เวลาเข้าใช้งาน',
            'options' => 'สิทธิการเข้าถึง',
            'mobile' => 'เบอร์โทร',
            'qrcodekey' => 'QRcodeKey',
            'uuid' => 'Uuid',
            'project_code' => 'ProjectCode',
            'created_tm' => 'เวลาที่ขอ QR',
            'language' => 'ภาษา',
            'fullname' => 'ชื่อ-นามสกุล',
            'visitor_img_url' => 'รูปถ่ายบัตรประชาขน',
            'camera_img_url' => 'รูปถ่ายผู้ขอ QR code',
            'photo' => 'รูปภาพ',
        ];
    }

    public static function getOwnerByQrkey($qrkey)
    {
        return static::findOne(['qrcodekey' => $qrkey]);
    }

    public function createShortlink($url)
    {

        $params = array('longDynamicLink' => 'https://qrletmein.page.link/?link='.$url);

        $headers = array();
        $headers[] = 'Content-type: application/json;charset=UTF-8';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "http.protocol.cookie-policy=compatibility");
        curl_setopt($ch, CURLOPT_URL, Yii::$app->params['url_shortlink']);
//         curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response  = curl_exec($ch);

        curl_close($ch);

        // Yii::warning($response, "curl_response");

        $responseData = json_decode($response);
        return $responseData;
    }

    public function addAccesscard() { //Version2

        $device_model = new Device();

        $post_url = Yii::$app->params['addCard'].Yii::$app->params['openToken'];

        $post_data = [
          'cardUids' => [],  //Card Uid specify by HEX 8 bit.
          'deviceIds' => [],  //Group of devices.
          'endTime' => '',   //End time of card. (Maxdays: 65535)
          'cardType' => 2, //type 2 is QR Code.
        ];

        $result = $device_model->manageDevice($post_url, $post_data, 2);

        if($result->statusCode == "1") {

            Yii::$app->response->statusCode = 200;
            return ['status' => $result->statusCode, 'message' => $result->responseResult];
        }
        else {
            Yii::warning($result, 'RESULT ADD CARD');
        }
    }

    public function updateAccesscard() { //Version2

        $device_model = new Device();

        $post_url = Yii::$app->params['updateCard'].Yii::$app->params['openToken'];

        $post_data = [
          'cardUids' => [],  //Card Uid specify by HEX 8 bit.
          'newDeviceIds' => [],  //Group of devices.
          'newEndTime' => '',   //End time of card. (Maxdays: 65535)
          'cardType' => 2, //type 2 is QR Code.
        ];

        $result = $device_model->manageDevice($post_url, $post_data, 2);

        if($result->statusCode == "1") {

            Yii::$app->response->statusCode = 200;
            return ['status' => $result->statusCode, 'message' => $result->responseResult];
        }
        else {
            Yii::warning($result, 'RESULT UPDATE CARD');
        }
    }

    public function addAccesscardlift() { //Version2

        $device_model = new Device();

        $post_url = Yii::$app->params['addCardforLift'].Yii::$app->params['openToken'];

        $post_data = [
          'cardUids' => [],  //Card Uid specify by HEX 8 bit.
          'autoFloorId' => 0,
          'baseFloorId' => 1,
          'userType' => 4, //type 4 is QR Code.
          'floorIds' => []
        ];

        $result = $device_model->manageDevice($post_url, $post_data, 2);

        if($result->statusCode == "1") {

            Yii::$app->response->statusCode = 200;
            return ['status' => $result->statusCode, 'message' => $result->responseResult];
        }
        else {
            Yii::warning($result, 'RESULT UPDATE CARD');
        }
    }

    public function getQrcodeKey($time_expire, $effective_time, $start_time, $device_info) { //Version2

      $device_model = new Device();

      $post_url = Yii::$app->params['addQrcode'].Yii::$app->params['openToken'];

      $post_data = [
        'funcFlag' => 1,  //Function identification: standard QR code. 0 is offline, 1 is online
        'userFlag' => '', //Use Card Uid.
        'userType' => 1, //0 is Home User, 1 is APP visitor, 2 is Kiosk visitor
        'startTime' => $start_time,
        'endTime' => $time_expire,
        'effecNumber' => $effective_time,
        'strkey' => Yii::$app->params['strKeyV2']
      ];

      $result = $device_model->manageDevice($post_url, $post_data, 2);

      if($result->statusCode == "1") {

          Yii::$app->response->statusCode = 200;
          return [
            'status' => $result->statusCode,
            'message' => 'Generated Complete!',
            'qrcode_key' => $result->responseResult->qrcodeKey
          ];
      }
      else {
          Yii::warning($result, 'RESULT UPDATE CARD');
      }
    }

}
