<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lift_status".
 *
 * @property int $id
 * @property string $lift_no
 * @property string $lift_status
 * @property string $card_no
 * @property string $arriving_time
 * @property string $vehicle_status
 */
class LiftStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lift_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lift_no', 'lift_status', 'arriving_time', 'vehicle_status'], 'string', 'max' => 255],
            [['card_no'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lift_no' => 'Lift No',
            'lift_status' => 'Lift Status',
            'card_no' => 'Card No',
            'arriving_time' => 'Arriving Time',
            'vehicle_status' => 'Vehicle Status',
        ];
    }
}
