<?php
return [
    'ch_secret' => 'f4bc61bac29b6bbc745a573abc0532c5',
    'ch_token' => '6VbQOynb5YpPmisWEZrr7KcDozxyDAJ0Pgbw6/Vop0gSkpITKhwdqFQOYP69nL0dMoAMvs1qYpZJNhvmWTeAvjfqWBH0LbjfIAasEKrC/2yXr3ujh0Tng7UPtih3mLp0UpdKZReu5fL4ycddQDTvawdB04t89/1O/w1cDnyilFU=',
    'env' => 'Production',
    'url_visitor_img' => 'https://api.letmein.asia/img/visitors/',
    'url_carin_img' => 'https://api.letmein.asia/img/carin/',
    'url_carout_img' => 'https://api.letmein.asia/img/carout/',
    'url_card_img' => 'https://api.dev.letmein.asia/img/cardid/',
    'file_path' => '../../api/file/upload/',
    'visitor_img_path' => '../../api/img/visitors/',
    'carin_img_path' => '../../api/img/carin/',
    'carout_img_path' => '../../api/img/carout/',
    'card_img_path' => '../../api/img/cardid/',
    'url_camera_img' => 'https://api.letmein.asia/img/cameras/',
    'camera_img_path' => '../../api/img/cameras/',
    'genqr_url' => 'https://www.letmein.asia/index.php?r=qrcode/genqr',
    'apigenqr' => 'https://api.letmein.asia/gen_qrcode.php?w=',
    'qrcode_path' => '../../api/img/qrcode/',
    'qrcode_url' => 'https://api.letmein.asia/img/qrcode/',
    'foreground' => array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0),
    'logo_default' => '../../frontend/web/imgs/letmein.png',
    'logo_taka' => '../../frontend/web/imgs/taka.png',
    'logo_path' => '../../frontend/web/imgs/lineasok.png',
    'foreground_line' => array('r' => 63, 'g' => 72, 'b' => 85, 'a' => 0),
    'logo_path_line' => '../../frontend/web/imgs/letmein.png'
];
